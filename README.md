# 맛딱드림 프로젝트
##### Matddak Drim Project

2024.03.04 ~ 2024.05.07


![logo](./assets/matddak_thumb.jpg)
한국의 배달 플랫폼 시장이 계속해서 성장하는 가운데, 우리는 음식을 주문하는 고객의 입장이 아닌 **배달 라이더 분들의 관점에서 출발**하여 프로젝트를 시작했습니다.

이미 다양한 배달 라이더 플랫폼이 존재하지만, 이번 맛딱드림 프로젝트에서는 **사용자 경험을 개선하기 위해 더 적은 탭 수, 직관적인 화면 및 시스템을 구현하는 것을 목표**로 삼았습니다. 
GPS 기술의 적용과 함께 **라이더 플랫폼의 시스템 흐름을 파악하는 것이 이번 프로젝트의 핵심**입니다.

우리는 맛딱드림 프로젝트를 통해 라이더 분들의 작업 효율성을 향상시키는 방법과 더 나은 서비스를 제공할 수 있는 방안을 모색할 것이며 이를 통해
**라이더 플랫폼의 시스템 흐름도를 파악할 수 있는 유익한 경험이 될 것으로 기대**됩니다.
***
## 😄 개발자 소개

- **GmJu** (PM) : 백엔드 [[ 😺GitLap ]](https://gitlab.com/myungdon)
- **McKang** (PL) : 앱 프론트엔드 [[ 😺GitLap ]](https://gitlab.com/kmjkali)
- **IuPark** : 웹 프론트엔드 [[ 😺GitLap ]](https://gitlab.com/meimeipark)
- **WbBae** : 웹 프론트엔드[[ 😺GitLap ]](https://gitlab.com/babasw135)
- **RgKim** : 백엔드 [[ 😺GitLap ]](https://gitlab.com/namho0172)
- **FxNull** : 앱 프론트엔드 [[ 😺GitLap ]](https://gitlab.com/hamsubin0628)
***
## 📱 프로젝트 주요 기능

1. 한번의 탭으로 **요청, 배차, 출발, 완료 상태변경**
2. 라이더 **실시간 현재 위치** 확인
3. 앱에서 바로 가능한 **페이 출금** 기능
***
## 🛠️ 기술 스택

![dart](https://img.shields.io/badge/Dart-0175C2?style=for-the-badge&logo=dart&logoColor=white)
![flutter](https://img.shields.io/badge/Flutter-02569B?style=for-the-badge&logo=flutter&logoColor=white)
***
## 📁️ 사용 패키지
### 🛵 라이더 실시간 위치 확인하기
- **google_maps_flutter** : ^2.5.3 [[ 🌐pub.dev 바로 가기 ]](https://pub.dev/packages/google_maps_flutter)
- **geolocator** : ^11.0.0 [[ 🌐pub.dev 바로 가기 ]](https://pub.dev/packages/geolocator)


### 🌀 애니메이션이 적용된 로딩 표시기 모음
- **flutter_spinkit** : ^5.1.0 [[ 🌐pub.dev 바로 가기 ]](https://pub.dev/packages/flutter_spinkit)

### 📱 스플래시 화면
- **flutter_native_splash** : ^2.3.10 [[ 🌐pub.dev 바로 가기 ]](https://pub.dev/packages/flutter_native_splash)

### 🔵 쿠퍼티노 아이콘
- **cupertino_icons** : ^1.0.2 [[ 🌐pub.dev 바로 가기 ]](https://pub.dev/packages/cupertino_icons)

### 📞 디오
- **dio** : ^5.4.0 [[ 🌐pub.dev 바로 가기 ]](https://pub.dev/packages/dio)

### ⚙ 공유 환경 설정 플러그인
- **shared_preferences** : ^2.2.2 [[ 🌐pub.dev 바로 가기 ]](https://pub.dev/packages/shared_preferences)

### 📑 폼 빌더
- **flutter_form_builder** : ^9.2.1 [[ 🌐pub.dev 바로 가기 ]](https://pub.dev/packages/flutter_form_builder)

### 🔍 폼 빌더 유효성 검사
- **form_builder_validators** : ^9.1.0 [[ 🌐pub.dev 바로 가기 ]](https://pub.dev/packages/form_builder_validators)

### 🌎 국제화 메커니즘
- **intl** : ^0.18.1 [[ 🌐pub.dev 바로 가기 ]](https://pub.dev/packages/intl)

### 🗓 달력 위젯
- **table_calendar** : ^3.0.9 [[ 🌐pub.dev 바로 가기 ]](https://pub.dev/packages/table_calendar)

### 🖼 폼 빌더 이미지 선택기
- **form_builder_image_picker** : ^4.0.0 [[ 🌐pub.dev 바로 가기 ]](https://pub.dev/packages/form_builder_image_picker)

### 📂 파일 MIME 유형 결정
- **mime** : ^1.0.5 [[ 🌐pub.dev 바로 가기 ]](https://pub.dev/packages/mime)
***
## 🔗 프로젝트 아키텍처
![architecture](./assets/matddak_architecture.jpg)
***
## ✅ 릴리즈 노트

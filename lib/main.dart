
import 'package:flutter/material.dart';
import 'package:matddak_v1/pages/join/page_join_membership.dart';
import 'package:matddak_v1/pages/join/page_safety_check.dart';

import 'package:matddak_v1/pages/order/page_order_fin_detail.dart';
import 'package:matddak_v1/pages/order/page_request_build.dart';
import 'package:matddak_v1/pages/order/page_rider_main.dart';
import 'package:matddak_v1/pages/page_change_my_info.dart';
import 'package:matddak_v1/pages/page_login.dart';
import 'package:matddak_v1/pages/page_modify_my_info.dart';
import 'package:matddak_v1/pages/page_order_history.dart';
import 'package:matddak_v1/pages/page_splash.dart';
import 'package:matddak_v1/pages/payment/page_delivery_fee.dart';
import 'package:matddak_v1/pages/payment/page_income_declatarion.dart';
import 'package:matddak_v1/pages/payment/page_notice_list.dart';
import 'package:matddak_v1/pages/payment/page_out_money.dart';
import 'package:matddak_v1/pages/payment/page_out_money_detail.dart';
import 'package:matddak_v1/pages/payment/page_pay_details.dart';
import 'package:matddak_v1/pages/payment/page_pay_main.dart';
import 'package:matddak_v1/pages/test.dart';
import 'package:matddak_v1/test/pay_test.dart';

import 'map_sample.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});



  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      //initialRoute:"/page_login",
      initialRoute:"/page_login",
      routes: { /** 페이지 주소 경로
                  * 사용할 페이지 생성 후 주석 풀어서 사용
                 **/
        "/filetest" : (context) => FileTestApp(),
        "/page_rider_main":(context) => PageRiderMain(),
        "/page_splash":(context) => PageSplash(),
        "/page_login":(context) => PageLogin(),
        "/page_join_membership":(context) => PageJoinMembership(),
        // "/page_device":(context) => PageDevice(),
        // "/page_account_info":(context) => PageAccountInfo(),
        // "/page_delivery_info":(context) => PageDeliveryInfo(),
        "/page_safety_check":(context) => PageSafetyCheck(),
        // "/page_order_request":(context) => PageOrderRequest(),
        "/page_order_fin_detail":(context) => PageOrderFinDetail(deliveryId: 1),
        "/page_change_my_info":(context) => PageChangeMyInfo(),
        "/page_modify_my_info":(context) => PageModifyMyInfo(),
        "/page_pay_main":(context) => PagePayMain(),
        "/page_pay_details":(context) => PagePayDetails(),
        "/page_delivery_fee":(context) => PageDeliveryFee(),
        "/page_out_money":(context) => PageOutMoney(),
        "/page_out_money_detail":(context) => PageOutMoneyDetail(),
        "/page_income_declatarion":(context) => PageIncomeDeclatarion(),
        "/page_notice_list":(context) => PageNoticeList(),
        "/page_order_history":(context) => PageOrderHistory(),
        //"/page_notice_detail":(context) => PageNoticeDetail(id: 2,),
        "/map_sample":(context) => MapSample(),
        "/page_request_build":(context) => PageRequestBuild(),
        "/pay_test":(context) => PayTest(),
      },
      title: 'Flutter Demo',
      darkTheme: ThemeData.dark(useMaterial3: true),
      theme: ThemeData(
        scaffoldBackgroundColor: Color.fromRGBO(48, 48, 48, 1),
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.orangeAccent),
        useMaterial3: true,
      ),
      // home:PageAccountInfo()
    );
  }
}

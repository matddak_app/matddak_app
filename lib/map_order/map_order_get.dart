import 'dart:async';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:matddak_v1/remote_data_source/google_map_data_source.dart';

class MapOrderGet extends StatefulWidget {
  const MapOrderGet({super.key});

  @override
  State<MapOrderGet> createState() => _MapOrderGetState();
}

class _MapOrderGetState extends State<MapOrderGet> {
  Position? currentPosition;

  // OrderGetResponse? _orderContents;

  // Future<void> _loadOrderContents() async {
  //   await GoogleMapDataSource().getList()
  //       .then((res) => {
  //     setState(() {
  //       _orderContents = res.data;
  //     })
  //   });
  // }

  /**
   * 기기 현재 위치 확인
   *
   * 위치 서비스가 활성화 되지 않았거나 사용 권한이 있는 경우
   * '미래'에 오류가 반환되는 것을 거부한다.
   */
  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    // 위치 서비스 활성화 테스트
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      // 위치서비스 비활성화 => 계속 하지 않음
      // 위체이 액세스 하고 사용자에게 요청
      // 앱 위치 서비스 활성화
      return Future.error('위치 서비스가 비활성화 되었습니다.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        // 권한이 거부되었습니다 => 다음에 시도할 수 있습니다.
        // 권한 재요청 ( this is also where )
        // Android's는 RequestPermissionRationale를 표시해야 합니다
        // 사실로 돌아갔습니다. 안드로이드 가이드라인에 따르면
        // 지금 앱에 설명 UI가 표시되어야 합니다.
        return Future.error('위치 서비스가 거부되었습니다.');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // 권한은 영원히 거부되며, 적절히 처리됩니다.
      return Future.error(
          '위치 권한이 영구적으로 거부되었습니다, 권한을 요청할 수 없습니다.');
    }

    // 우리가 여기에 도착하면, 허가가 주어지고 우리는 할 수 있습니다
    // 장치의 위치에 계속 액세스합니다.
    return await Geolocator.getCurrentPosition();
  }

  final Completer<GoogleMapController> _controller =
  Completer<GoogleMapController>();

  final List<Marker> _markers = [];

  @override
  void initState() {
    super.initState();
    _determinePosition(); // 실행만 한다고 currentPosition에 들어가지 않는다
    // _getMapList();
    // _loadOrderContents(); // 가게 마커 찍기
    _determinePosition().then((value) => { // async니까 promise(약속) 쓸 수 있음
      setState((){ // setState 해야하는 이유? => 원웨이 바인딩이니까 바꼈어! 해야 디자인 요소에서 다시 가져감
        currentPosition = value;
      })
    });
  }

  // 미래에 포지션 정보를 주는 애니까.. 처음에는 없다
  // 그럼 null 허용
  // 디자인 요소에서 써야하니까.. 어디다가 담아둬야한다?
  // 주니까 받아서 보관하고 그 보관한걸 디자인에서 가져다가 써야지



  // Future<void> _getMapList() async {
  //   await GoogleMapDataSource().getList().then((res) => {
  //     res.forEach((e) {
  //       setState(() {
  //         _markers.add(
  //             Marker(
  //                 markerId: MarkerId(e.storeName.toString()),
  //                 position: LatLng(126.832982, 37.3182998),
  //                 infoWindow: InfoWindow(title: e.priceFood.toString()))
  //         );
  //       });
  //     })
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GoogleMap(
        mapType: MapType.normal,
        markers: {
          Marker(
              markerId: MarkerId('빈체로'),
              position: LatLng(37.3182998, 126.832982)),
        },
        initialCameraPosition: CameraPosition(
          target: LatLng(currentPosition!.latitude, currentPosition!.longitude),
          zoom: 14.4746,
        ),
        onMapCreated: (GoogleMapController controller) {
          _controller.complete(controller);
        },
        myLocationEnabled: true,
        myLocationButtonEnabled: false,
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:matddak_v1/functions/token_lib.dart';
import 'package:matddak_v1/pages/order/page_rider_main.dart';
import 'package:matddak_v1/pages/page_login.dart';

class MiddlewareLoginCheck {
  void check(BuildContext context) async {
    String? riderToken = await TokenLib.getRiderToken();
    if (riderToken == null) {
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) => const PageLogin()),
          (route) => false);
    } else {
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) => const PageRiderMain()),
          (route) => false);
    }
  }
}

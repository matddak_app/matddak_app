


import 'package:dio/dio.dart';
import 'package:matddak_v1/config/config_api.dart';
import 'package:matddak_v1/functions/token_lib.dart';
import 'package:matddak_v1/model/common_result.dart';
import 'package:matddak_v1/model/payment/list_result_income_statistics_item.dart';
import 'package:matddak_v1/model/payment/list_result_money_history_item.dart';
import 'package:matddak_v1/model/payment/money_history_create_request.dart';
import 'package:matddak_v1/model/payment/single_result_income_statistics_response.dart';
import 'package:matddak_v1/model/payment/single_result_pay_response.dart';


class RepoPayment {


  //현재 보유 페이 불러오기

  Future<SingleResultPayResponse> getCurrentPay() async {
    String? token = await TokenLib.getRiderToken();
    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    String _baseUrl = '$apiUrl/rider-pay/detail';

    final response = await dio.get(
        _baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return SingleResultPayResponse.fromJson(response.data);
  }

  // 일간 수익  단수

  Future<SingleResultIncomeStatisticsResponse> getDateFee() async {
    String? token = await TokenLib.getRiderToken();
    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';


    String _baseUrl = '$apiUrl/income/date/rider';

    final response = await dio.get(
        _baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    print(response);
    return SingleResultIncomeStatisticsResponse.fromJson(response.data);
  }

  // 일간 수익 복수

  Future<ListResultIncomeStatisticsItem> getDateBundleFee() async {
    String? token = await TokenLib.getRiderToken();
    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';


    String _baseUrl = '$apiUrl/income/all/date/rider';

    final response = await dio.get(
        _baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    print(response);
    return ListResultIncomeStatisticsItem.fromJson(response.data);
  }


  // 주간 수익  단수 (주별 총수익)

  Future<SingleResultIncomeStatisticsResponse> getWeekFee() async {
    String? token = await TokenLib.getRiderToken();
    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';


    String _baseUrl = '$apiUrl/income/week/rider';

    final response = await dio.get(
        _baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
            return status == 200;
            }
        )
    );
    print(response);
    return SingleResultIncomeStatisticsResponse.fromJson(response.data);
  }



  // 주간 수익 복수

  Future<ListResultIncomeStatisticsItem> getWeekBundleFee() async {
    String? token = await TokenLib.getRiderToken();
    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';


    String _baseUrl = '$apiUrl/income/all/week/rider';

    final response = await dio.get(
        _baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    print(response);
    return ListResultIncomeStatisticsItem.fromJson(response.data);
  }

  // 월간 수익  단수 (한달 총 수익)

  Future<SingleResultIncomeStatisticsResponse> getMonthFee() async {
    String? token = await TokenLib.getRiderToken();
    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    String _baseUrl = '$apiUrl/income/month/rider';

    final response = await dio.get(
        _baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return SingleResultIncomeStatisticsResponse.fromJson(response.data);
  }


  // 월간 수익 복수( 일별 수익 1달치 )

  Future<ListResultIncomeStatisticsItem> getMonthBundleFee() async {
    String? token = await TokenLib.getRiderToken();
    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';


    String _baseUrl = '$apiUrl/income/all/month/rider';

    final response = await dio.get(
        _baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return ListResultIncomeStatisticsItem.fromJson(response.data);
  }




  // 출금 요청 하기

  Future<CommonResult> setOutMoney(MoneyHistoryCreateRequest request) async {

    String? token = await TokenLib.getRiderToken();
    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    String _baseUrl = '$apiUrl/money-history/out';

    final response = await dio.post(
      _baseUrl,
      data: request.toJson(),
      options: Options(
        followRedirects: false,
        validateStatus: (status) {
          return status == 200;
        }
      )
    );

    return CommonResult.fromJson(response.data);

  }

  // 출금요청 내역

  Future<ListResultMoneyHistoryItem> getOutMoney() async {
    String? token = await TokenLib.getRiderToken();
    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    String _baseUrl = '$apiUrl/money-history/all/out';

    final response = await dio.get(
        _baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    print("===========================");
    print(response.data);
    print("===========================");

    return ListResultMoneyHistoryItem.fromJson(response.data);
  }




}
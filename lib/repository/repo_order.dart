import 'package:dio/dio.dart';
import 'package:matddak_v1/config/config_api.dart';
import 'package:matddak_v1/functions/token_lib.dart';
import 'package:matddak_v1/model/common_result.dart';
import 'package:matddak_v1/model/order/ask_post/ask_create_request.dart';
import 'package:matddak_v1/model/order/cancel/order_cancel_list_result.dart';
import 'package:matddak_v1/model/order/fin/order_fin_item_list_result.dart';
import 'package:matddak_v1/model/order/go/order_start_change_request.dart';
import 'package:matddak_v1/model/order/request/order_get_result.dart';
import 'package:matddak_v1/model/order/request/order_request_list_result.dart';
import 'package:matddak_v1/model/order/total_count/single_result_tab_count_response.dart';

/** 배달 요청 복수 R **/
class RepoOrder {
  /** 냄비 뚜껑이 없으면 무조건 줘 **/
  Future<OrderRequestListResult> getList({int page = 1}) async {
    Dio dio = Dio();

    String baseUrl = '$apiUrl/ask/all/request/{pageNum}'.replaceAll('{pageNum}', page.toString());

    final response = await dio.get(
        baseUrl.replaceAll('{pageNum}', page.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return OrderRequestListResult.fromJson(response.data);
  }

  /** 배달 배차 단수 R **/
  Future<OrderGetResult> getOrder() async{
    String? token = await TokenLib.getRiderToken();
    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;

    String _baseUrl = '$apiUrl/delivery/detail/pick';

    final response = await dio.get(
        _baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return OrderGetResult.fromJson(response.data);
  }

  /** 배달 출발 단수 R **/
  Future<OrderGetResult> getStart() async{
    String? token = await TokenLib.getRiderToken();
    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;

    String _baseUrl = '$apiUrl/delivery/detail/go';

    final response = await dio.get(
        _baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return OrderGetResult.fromJson(response.data);
  }

  /** 배달 완료 상세 보기 단수 R **/
  Future<OrderGetResult> getFinDetail(num id) async{
    String? token = await TokenLib.getRiderToken();
    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;

    String _baseUrl = '$apiUrl/delivery/detail/done/{deliveryId}';

    final response = await dio.get(
        _baseUrl.replaceAll('{deliveryId}', id.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return OrderGetResult.fromJson(response.data);
  }

  /** 배달 완료 리스트 보기 복수 R **/
  Future<OrderFinItemListResult> getFins() async{
    String? token = await TokenLib.getRiderToken();
    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;

    String _baseUrl = '$apiUrl/delivery/all/done/app';

    final response = await dio.get(
        _baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return OrderFinItemListResult.fromJson(response.data);
  }

  /** 배달 취소 리스트 보기 복수 R **/
  Future<OrderCancelListResult> getCancels({int page = 1}) async{
    Dio dio = Dio();

    String _baseUrl = '$apiUrl/ask/all/cancel/date';

    final response = await dio.get(
        _baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return OrderCancelListResult.fromJson(response.data);
  }

  /** 배달 카운트 단수 R **/
  Future<SingleResultTabCountResponse> getTabCount() async{

    String? token = await TokenLib.getRiderToken();
    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;

    String _baseUrl = '$apiUrl/delivery/tab';

    final response = await dio.get(
        _baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return SingleResultTabCountResponse.fromJson(response.data);
  }

  /** 배달 요청 -> 배차 신청 C **/
  Future<CommonResult> setPick(num id) async{

    String? token = await TokenLib.getRiderToken();

    String _baseUrl = '$apiUrl/delivery/new/ask-id/{askId}';

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;

    final response = await dio.post(
        _baseUrl.replaceAll('{askId}', id.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return CommonResult.fromJson(response.data);
  }

  /** 배달 배차 -> 출발 수정 U **/
  Future<CommonResult> putStart() async{

    String? token = await TokenLib.getRiderToken();

    String _baseUrl = '$apiUrl/delivery/change/go';

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;

    final response = await dio.put(
        _baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return CommonResult.fromJson(response.data);
  }

  /** 배달 출발 -> 완료 수정 U **/
  Future<CommonResult> putFin() async{

    String? token = await TokenLib.getRiderToken();

    String _baseUrl = '$apiUrl/delivery/change/done';

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;

    final response = await dio.put(
        _baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return CommonResult.fromJson(response.data);
  }
}
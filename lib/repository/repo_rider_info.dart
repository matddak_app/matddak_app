import 'package:dio/dio.dart';
import 'package:matddak_v1/config/config_api.dart';
import 'package:matddak_v1/functions/token_lib.dart';
import 'package:matddak_v1/model/rider/single_result_rider_info_response.dart';

class RepoRiderInfo {
  Future<SingleResultRiderInfoResponse> getRiderInfo() async{

    String _baseUrl = '$apiUrl/rider-info/detail';

    String? token = await TokenLib.getRiderToken();
    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;

    final response = await dio.get(
        _baseUrl.toString(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }));
    return SingleResultRiderInfoResponse.fromJson(response.data);
  }
}
import 'dart:async';

import 'package:dio/dio.dart';
import 'package:matddak_v1/config/config_api.dart';
import 'package:matddak_v1/functions/token_lib.dart';
import 'package:matddak_v1/model/common_result.dart';
import 'package:matddak_v1/model/login/login_request.dart';
import 'package:matddak_v1/model/login/login_result.dart';
import 'package:matddak_v1/model/rider/join_request.dart';
import 'package:matddak_v1/model/rider/my_info_change_request.dart';
import 'package:matddak_v1/model/rider/single_result_rider_response.dart';






class RepoRider {

  /** 라이더 정보 수정 U**/

  Future<CommonResult> putRider(MyInfoChangeRequest changeRequest) async {
    String? token = await TokenLib.getRiderToken();
    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    String _baseUrl = '$apiUrl/rider/change';

    final response = await dio.put(
        _baseUrl.toString(),
        data: changeRequest.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (state) {
              return state == 200;
            }));

    return CommonResult.fromJson(response.data);
  }


  /** 라이더 정보 단수 R **/
  Future<SingleResultRiderResponse> getRiderInfo() async {
    String? token = await TokenLib.getRiderToken();
    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    String _baseUrl = '$apiUrl/rider/detail';

    final response = await dio.get(
        _baseUrl.toString(),
        options: Options(
            followRedirects: false,
            validateStatus: (state) {
              return state == 200;
            }));

    return SingleResultRiderResponse.fromJson(response.data);
  }

  /** 라이더 회원가입 C **/
  Future<CommonResult> setRider(JoinRequest joinRequest) async {
    Dio dio = Dio();

    String _baseUrl = '$apiUrl/rider/join/normal';

    final response = await dio.post(
        _baseUrl,
        data: joinRequest.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (state) {
              return state == 200;
            }));

    return CommonResult.fromJson(response.data);
  }

  /** 라이더 로그인 C **/
  Future<LoginResult> doLogin(LoginRequest loginRequest) async {
    const baseUrl = '$apiUrl/login/app/user';


    Dio dio = Dio();


    final response = await dio.post(
        baseUrl,
        data: loginRequest.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (state) {
              return state == 200;
            }));

    return LoginResult.fromJson(response.data);
  }

  /** 프로필 등록 C **/

  Future<CommonResult> changeAvatar(dynamic formData) async {
    print("프로필 사진을 서버에 업로드 합니다.");
    String? token = await TokenLib.getRiderToken();
    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';
    dio.options.contentType = 'multipart/form-data';
    dio.options.maxRedirects.isFinite;

    const _baseUrl = '$apiUrl/rider/change/img';
    final response = await dio.put(
        _baseUrl,
        data: formData,
        options: Options(
            followRedirects: false,
            validateStatus: (state) {
              return state == 200;
            }));
    print('성공적으로 업로드했습니다');

    return CommonResult.fromJson(response.data);
  }


}
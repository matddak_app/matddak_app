

import 'package:dio/dio.dart';
import 'package:matddak_v1/config/config_api.dart';
import 'package:matddak_v1/model/notice/notice_list_result.dart';
import 'package:matddak_v1/model/notice/single_result_notice_response.dart';

class RepoNotice {


  //공지사항  리스트  불러오기

  Future<NoticeListResult> getNoticeList() async {

    Dio dio = Dio();
    String _baseUrl = '$apiUrl/board/all';

    final response = await dio.get(
      _baseUrl,
      options: Options(
        followRedirects: false,
        validateStatus: (status) {
          return status == 200;
        }
      )
    );
    print(response.data.toString());

    return NoticeListResult.fromJson(response.data);
  }

  //공지사항 단수 불러오기

 Future<SingleResultNoticeResponse> getNotice(num id) async{
    Dio dio = Dio();

    String _baseUrl = '$apiUrl/board/detail/{id}';

    final response = await dio.get(
      _baseUrl.replaceAll('{id}', id.toString()),
      options: Options(
        followRedirects: false,
        validateStatus: (status) {
          return status == 200;
        }
      )

    );
    return SingleResultNoticeResponse.fromJson(response.data);
 }


}
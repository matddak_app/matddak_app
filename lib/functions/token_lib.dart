import 'package:flutter/material.dart';
import 'package:matddak_v1/pages/page_login.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TokenLib {
  static Future<String?> getRiderToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('riderToken');
  }
  static Future<String?> getRiderName() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('riderName');
  }
  static void setRiderToken(String riderToken) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('riderToken', riderToken);
  }
  static void setRiderName(String riderName) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('riderName', riderName);
  }

  static void logout(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    prefs.clear();

    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (BuildContext context) => const PageLogin()),
            (route) => false
    );
  }
}

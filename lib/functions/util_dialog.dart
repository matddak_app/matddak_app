import 'package:flutter/material.dart';

import '../config/config_color.dart';
import '../config/config_text_size.dart';

void showAlertDialog(
    BuildContext context, {
      required String title,
      required String content,
      List<Widget> actions = const [],
    }) {
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        surfaceTintColor: Colors.grey,
        backgroundColor: colorButton,
        title: Text(
          title,
          style: TextStyle(
            fontFamily: 'NotoSans_Bold',
            fontSize: fontSizeSuper,
            letterSpacing: -0.5,
            color: colorTextBright,
          ),
        ),
        content: Text(
          content,
          style: TextStyle(
            fontFamily: 'NotoSans_NotoSansKR-Light',
            fontSize: fontSizeMainText,
            letterSpacing: -0.5,
            color: colorTextBright,
          ),
        ),

        actions: actions,
      );
    },
  );
}

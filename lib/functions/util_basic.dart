import 'package:matddak_v1/functions/util_dialog.dart';

class StringUtils {
  /// 주어진 문자열을 반환하거나, 주어진 문자열이 null인 경우 기본 문자열을 반환합니다.
  static String defaultString(String? str, {String defaultStr = ''}) {
    return str ?? defaultStr;
  }
}

class ApiErrorHandler {

  static bool isDebug = true;

  static void handlingError(context, err){
    debugError(err);
    if ( err.response != null ){
      Map<String, dynamic> data = err.response.data;
      if ( data.containsKey("msg") ){
        showAlertDialog(context, title: 'DEBUG:[API 오류]', content: data['msg']);
      }
    }
  }

  static void debugError(err){
    if ( isDebug ){
      print(err);
      if (err.response != null) {
        print(err.response.data);
        print(err.response.headers);
        print(err.response.requestOptions);
      } else {
        print(err.requestOptions);
        print(err.message);
      }
    }
  }

}



import 'package:flutter/material.dart';
import 'package:matddak_v1/component/component_appbar.dart';
import 'package:matddak_v1/component/component_order_history_item.dart';
import 'package:matddak_v1/config/config_color.dart';
import 'package:matddak_v1/config/config_text_size.dart';
import 'package:matddak_v1/model/order/fin/order_fin_item.dart';
import 'package:matddak_v1/repository/repo_order.dart';


class PageOrderHistory extends StatefulWidget {
  const PageOrderHistory({super.key});

  @override
  State<PageOrderHistory> createState() => _PageOrderHistoryState();
}

class _PageOrderHistoryState extends State<PageOrderHistory> {
  DateTime date = DateTime.now(); // 선택한 날짜를 입력받을 변수 선언

  /** 달력 테마 다크로 적용 **/
  @override
  Widget bulid(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(brightness: Brightness.dark),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /** 앱바 **/
      appBar: ComponentAppbar(
        text: '주문 과거 내역',
        page: '/page_rider_main',
      ),
      /** 전체를 감싸는 틀 **/
      body: Container(
        color: colorMainBg,
        child: Column(
          children: [
            /** 캘린더 전체 **/
            // Container(
            //   padding: EdgeInsets.fromLTRB(5, 0, 5, 5),
            //   child: Row(
            //     mainAxisAlignment: MainAxisAlignment.center,
            //     children: [
            //       /** 캘린더 시작 날짜 **/
            //       Container(
            //         width: MediaQuery.of(context).size.width / 1.03,
            //         height: 55,
            //         decoration: BoxDecoration(
            //             color: colorButton,
            //             borderRadius: BorderRadius.circular(5)
            //         ),
            //         child: Container(
            //           alignment: FractionalOffset.center,
            //           margin: EdgeInsets.fromLTRB(10, 5, 0, 10),
            //           child: TextButton(
            //             onPressed: () async{
            //               final selectedDate = await showDatePicker(
            //                 builder: (BuildContext context, Widget ?child) {
            //                   return Theme(data: ThemeData.dark(), child: child ??Text(''));
            //                 },
            //                 context: context, // 팝업으로 띄우기 때문에 context 전달
            //                 initialDate: date, // 달력을 띄웠을 때 선택된 날짜. 위에서 date 변수에 오늘 날짜를 넣었으므로 오늘 날짜가 선택돼서 나옴
            //                 firstDate: DateTime(1980), // 시작 년도
            //                 lastDate: DateTime.now(), // 마지막 년도. 오늘로 지정하면 미래의 날짜 선택불가
            //               );
            //               if (selectedDate != null) {
            //                 setState(() {
            //                   date = selectedDate; // 선택한 날짜는 dart 변수에 저장
            //                 });
            //               }
            //             },
            //             child: Text(
            //               '$date'.substring(0, 10), //출력하면 시간도 같이 출력이 되는데 날짜만 표시하고 싶어서 substring으로 잘라냄
            //               style: TextStyle(
            //                 fontFamily: 'NotoSans_Bold',
            //                 color: colorTextBright,
            //                 fontSize: fontSizeSuper,
            //               ),
            //             ),
            //           ),
            //         ),
            //       ),
            //       // /** 사이 물결 **/
            //       // Container(
            //       //   margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
            //       //   child: Text(
            //       //     '~',
            //       //     style: TextStyle(
            //       //       fontFamily: 'NotoSans_NotoSansKR-SemiBold',
            //       //       color: colorTextBright,
            //       //       fontSize: fontSizeTitle,
            //       //     ),
            //       //   ),
            //       // ),
            //       // /** 캘린더 종료 날짜 **/
            //       // Container(
            //       //   width: MediaQuery.of(context).size.width / 2.38,
            //       //   height: 55,
            //       //   decoration: BoxDecoration(
            //       //       color: colorButton,
            //       //       borderRadius: BorderRadius.circular(8)
            //       //   ),
            //       //   child: Container(
            //       //     alignment: FractionalOffset.center,
            //       //     margin: EdgeInsets.fromLTRB(10, 5, 0, 10),
            //       //     child: TextButton(
            //       //       onPressed: () async{
            //       //         final selectedDate = await showDatePicker(
            //       //           builder: (BuildContext context, Widget ?child) {
            //       //             return Theme(data: ThemeData.dark(), child: child ??Text(''));
            //       //           },
            //       //           context: context, // 팝업으로 띄우기 때문에 context 전달
            //       //           initialDate: date, // 달력을 띄웠을 때 선택된 날짜. 위에서 date 변수에 오늘 날짜를 넣었으므로 오늘 날짜가 선택돼서 나옴
            //       //           firstDate: DateTime(1980), // 시작 년도
            //       //           lastDate: DateTime.now(), // 마지막 년도. 오늘로 지정하면 미래의 날짜 선택불가
            //       //         );
            //       //         if (selectedDate != null) {
            //       //           setState(() {
            //       //             date = selectedDate; // 선택한 날짜는 dart 변수에 저장
            //       //           });
            //       //         }
            //       //       },
            //       //       child: Text(
            //       //         '$date'.substring(0, 10), //출력하면 시간도 같이 출력이 되는데 날짜만 표시하고 싶어서 substring으로 잘라냄
            //       //         style: TextStyle(
            //       //           fontFamily: 'NotoSans_Bold',
            //       //           color: colorTextBright,
            //       //           fontSize: fontSizeSuper,
            //       //         ),
            //       //       ),
            //       //     ),
            //       //   ),
            //       // ),
            //     ],
            //   ),
            // ),
            /** 완료 주문건 띠 **/
            Container(
                padding: EdgeInsets.fromLTRB(16, 6, 0, 10),
                width: MediaQuery.of(context).size.width,
                color: colorBg,
                child: Text(
                  '완료 주문건',
                  style: TextStyle(
                      fontFamily: 'NotoSans_NotoSansKR-Regular',
                      fontSize: fontSizeMainText,
                      color: colorText,
                      letterSpacing: -0.5
                  ),
                )
            ),
            Expanded(
              /** 완료내역 컴포넌트 **/
                child: finList()
            )
          ],
        ),
      )
    );
  }

  /** 완료 리스트 연동 **/
  List<OrderFinItem> _OrderFinItem = [];

  final _scrollController = ScrollController();

  Future<void> _loadFinList() async {
    await RepoOrder().getFins()
        .then((value) => {
      setState((){
        _OrderFinItem = value.list;
      })
    }).catchError((onError)=>{
      debugPrint(onError)
    });
  }

  @override
  void initState() {
    super.initState();
    _loadFinList();
  }

  Widget finList() {
    return Scaffold(
      body: Ink( color: colorMainBg,
        child: ListView.builder(
          controller: _scrollController,
          itemCount: _OrderFinItem.length,
          itemBuilder: (BuildContext ctx, int idx) {
            return Container(
              child: ComponentOrderHistory(
                orderFinItem: _OrderFinItem[idx],
              ),
            );
          },
        ),
      )
    );
  }
}

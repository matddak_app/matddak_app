import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:matddak_v1/config/config_color.dart';
import 'package:matddak_v1/config/config_form_validator.dart';
import 'package:matddak_v1/config/config_img.dart';
import 'package:matddak_v1/config/config_text_size.dart';
import 'package:matddak_v1/functions/token_lib.dart';
import 'package:matddak_v1/middleware/middleware_login_check.dart';
import 'package:matddak_v1/model/login/login_request.dart';
import 'package:matddak_v1/repository/repo_rider.dart';

class PageLoginV2 extends StatefulWidget {
  const PageLoginV2({super.key});

  @override
  State<PageLoginV2> createState() => _PageLoginV2State();
}

class _PageLoginV2State extends State<PageLoginV2> {
  final _formKey = GlobalKey<FormBuilderState>();

  Future<void> _doLogin(LoginRequest loginRequest) async {
    await RepoRider().doLogin(loginRequest).then((res) {
      // api에서 받은 결과값을 token에 넣는다.
      TokenLib.setRiderToken(res.data.token);
      TokenLib.setRiderName(res.data.name);
      // 미들웨어에게 부탁해서 토큰값 여부 검사 후 페이지 이동을 부탁한다.
      MiddlewareLoginCheck().check(context);
    }).catchError((err) {
      debugPrint(err);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true, /** 키보드가 올라왔을 때 입력 창 키보드에 가려지지 않게 함 **/
      body: Container(
        height: MediaQuery.of(context).size.height,
        color: colorBg,
        padding: EdgeInsets.all(30.0),
        child: FormBuilder(
          key: _formKey,
          autovalidateMode: AutovalidateMode.disabled,
          child: ListView(
            children: [
              logoImg(),
              idInput(),
              passwordInput(),
              loginBtn(),
              joinBtn()
            ],
          )
        ),
      ),
    );
  }

  /** 로고 이미지 **/
  Widget logoImg() {
    return Container(
        width: MediaQuery.of(context).size.width / 4,
        child: Image.asset(logoImgUrl)
    );
  }

  /** 아이디 입력 폼**/
  Widget idInput() {
    return Container(
      margin: EdgeInsets.only(top: 6.0),
      height: 45,
      child: FormBuilderTextField(
        name: 'email',
        validator: FormBuilderValidators.compose([
          FormBuilderValidators.required(errorText: formErrorRequired), /** 필수 **/
        ]),
        /** 커서 색상 **/
        cursorColor: colorPoint,
        /** 키보드 바깥 영역 탭할 시 키보드 닫기 **/
        onTapOutside: (event) => FocusManager.instance.primaryFocus?.unfocus(),
        // inputFormatters: <TextInputFormatter>[
        //   FilteringTextInputFormatter.allow(RegExp(r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')), /** 이메일 형식 **/
        // ],
        decoration: InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
          ),
          /** 기본 보더 컬러 **/
          enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: colorHintText)
          ),
          /** 활성화된 보더 컬러 **/
          focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: colorPoint)
          ),
          hintText: '아이디',
          hintStyle: TextStyle(
              fontSize: fontSizeMainText,
              fontFamily: 'NotoSans_NotoSansKR-Regular',
              letterSpacing: -0.5,
              color: colorHintText,
              height: 1
          ),
        ),
        style: TextStyle(
            fontSize: fontSizeMainText,
            fontFamily:'NotoSans_NotoSansKR-Regular',
            color: colorHintText,
            letterSpacing: -0.5
        ),
      ),
    );
  }

  /** 비밀번호 입력 폼**/
  Widget passwordInput() {
    return Container(
      margin: EdgeInsets.only(top: 6.0),
      height: 45,
      child: FormBuilderTextField(
        name: 'password',
        obscureText: true, /** 가리기 **/
        validator: FormBuilderValidators.compose([
          FormBuilderValidators.required(errorText: formErrorRequired), /** 필수 **/
        ]),
        /** 커서 색상 **/
        cursorColor: colorPoint,
        /** 키보드 바깥 영역 탭할 시 키보드 닫기 **/
        onTapOutside: (event) => FocusManager.instance.primaryFocus?.unfocus(),
        // inputFormatters: <TextInputFormatter>[
        //   FilteringTextInputFormatter.allow(RegExp(r'[a-z|A-Z|0-9|ᆢ]')), /** 영문, 숫자만 입력 가능하게 하기 **/
        // ],
        decoration: InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
          ),
          /** 기본 보더 컬러 **/
          enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: colorHintText)
          ),
          /** 활성화된 보더 컬러 **/
          focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: colorPoint)
          ),
          hintText: '비밀번호',
          hintStyle: TextStyle(
              fontSize: fontSizeMainText,
              fontFamily: 'NotoSans_NotoSansKR-Regular',
              letterSpacing: -0.5,
              color: colorHintText,
              height: 1
          ),
        ),
        style: TextStyle(
            fontSize: fontSizeMainText,
            fontFamily:'NotoSans_NotoSansKR-Regular',
            color: colorHintText,
            letterSpacing: -0.5
        ),
      ),
    );
  }

  /** 로그인 버튼**/
  Widget loginBtn() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 45,
      margin: EdgeInsets.only(top: 6.0),
      child:ElevatedButton(
        child: Text(
          '로그인',
          style: TextStyle(
              color: colorTextBright,
              fontFamily: 'NotoSans_NotoSansKR-Medium',
              letterSpacing: -0.5,
              height: 1.5,
              fontSize: fontSizeMainText
          ),
        ),
        onPressed: () {
          if(_formKey.currentState!.saveAndValidate()) {
            LoginRequest loginRequest = LoginRequest(
                _formKey.currentState!.fields['email']!.value,
                _formKey.currentState!.fields['password']!.value
            );
            _doLogin(loginRequest);
          }
        },
        style: ElevatedButton.styleFrom(
          foregroundColor: Colors.grey,
          backgroundColor:  colorButton,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(4)
          ),
        ),
      ),
    );
  }

  /** 회원가입 버튼**/
  Widget joinBtn() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 45,
      margin: EdgeInsets.only(top: 5.0),
      child:ElevatedButton(
        child: Text(
          '회원가입',
          style: TextStyle(
              color: colorTextBright,
              fontFamily: 'NotoSans_NotoSansKR-Medium',
              letterSpacing: -0.5,
              height: 1.5,
              fontSize: fontSizeMainText
          ),
        ),
        onPressed: () {
          Navigator.of(context).pushNamed('/page_join_membership');
        },
        style: ElevatedButton.styleFrom(
          foregroundColor: Colors.grey,
          backgroundColor:  colorJoin,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(4)
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PageSplash extends StatelessWidget {
  const PageSplash({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          color: Color(0xff1A1A1A),

          child: Center(
            child: Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              alignment: Alignment.center,
              child: Image.asset('assets/img.gif')
            ),
          ),
        ),
      ),
    );
  }
}

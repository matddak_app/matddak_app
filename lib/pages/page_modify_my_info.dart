import 'dart:io';
import 'package:cross_file/cross_file.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_image_picker/form_builder_image_picker.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:matddak_v1/component/component_appbar.dart';
import 'package:matddak_v1/config/config_color.dart';
import 'package:matddak_v1/config/config_form_validator.dart';
import 'package:matddak_v1/config/config_text_size.dart';
import 'package:matddak_v1/functions/util_dialog.dart';
import 'package:matddak_v1/model/rider/my_info_change_request.dart';
import 'package:matddak_v1/model/rider/rider_response.dart';
import 'package:matddak_v1/repository/repo_rider.dart';

import '../functions/util_basic.dart';

class PageModifyMyInfo extends StatefulWidget {
  const PageModifyMyInfo({super.key});

  @override
  State<PageModifyMyInfo> createState() => _PageModifyMyInfoState();
}
class ApiImage {
  final String imageUrl;
  final String id;

  ApiImage({
    required this.imageUrl,
    required this.id,
  });
}


class _PageModifyMyInfoState extends State<PageModifyMyInfo> {

  final _formKey = GlobalKey<FormBuilderState>();

  RiderResponse? _response;

  Future<void> _putRider(MyInfoChangeRequest changeRequest) async {
    await RepoRider().putRider(changeRequest)
        .then((res) =>{
      showAlertDialog(context,
          title: "수정이 완료되었습니다. ",
          content: "",
          actions: <Widget>[
            TextButton(onPressed: ()=>Navigator.of(context).popAndPushNamed('/page_rider_main'), child: const Text("확인"))
          ])

    }).catchError((err) => {
      ApiErrorHandler.handlingError(context, err)
    });
  }

  Future<void> _putImage(dynamic formData) async {
    await RepoRider().changeAvatar(formData)
        .then((res) =>{
          print('Profile Image Upload Success!')
    }).catchError((err) => {
      ApiErrorHandler.handlingError(context, err)
    });
  }

  Future<void> _loadDetail() async {
    try {
      await RepoRider().getRiderInfo()
          .then((res) =>{
        setState(() {
          _response = res.data;
          // @Todo api에서 이넘값으로 줘야한다 . 아니면 텍스트랑 코드를 둘다줘야한다. 코드랑 코드명  다줘야함.
          _selectedDropdown = _response!.addressWishValue;
          _selectedKindDropdown = _response!.driveTypeValue;
          _selectedbankname = _response!.bankValue;
        })
      }).catchError((err) => {
        ApiErrorHandler.handlingError(context, err)
      });
    } catch (e, s) {
      print(s);
    }
  }

  @override
  void initState(){
    super.initState();
    _loadDetail();
  }


  List <Map<String,String>> _dropdownList = [
    { 'text': '* 희망하는 배달 지역을 선택해 주세요', 'value' : '' },
    { 'text': '안산시 단원구', 'value' :   'DANWON' },
    { 'text': '안산시 상록구', 'value' :   'SANGROCK' }
  ];

  //기본값 설정
  String? _selectedDropdown = '';

  // /** 배달 수단 드롭 메뉴 전역 변수 선언 **/
  // List<String> _kindList=['* 희망하는 배달 수단을 선택해 주세요','차', '오토바이', '전기 자전거', '자전거' ,'전동 킥보드', '도보'];
  List <Map<String,String>> _kindList = [
    { 'text': '* 희망하는 배달 수단을 선택해 주세요', 'value' : '' },
    { 'text': '자동차', 'value' :   'CAR' },
    { 'text': '오토바이', 'value' :   'MOTORCYCLE' },
    { 'text': '도보', 'value' :   'WALKING' },
    { 'text': '전기 자전거', 'value' :   'ELECTRIC_BICYCLE' },
    { 'text': '자전거', 'value' :   'CYCLE' },
    { 'text': '전동 킥보드', 'value' :   'ELECTRIC_SCOOTER' },
  ];

  //기본값 설정
  String? _selectedKindDropdown = '';

  List<Map<String,String>> _banknameList = [
    { 'text':'* 은행을 선택해 주세요', 'value':'' },
    { 'text': '국민은행', 'value' :   'KUKMIN' },
    { 'text': '하나은행', 'value' :   'HANA' },
    { 'text': '신한은행', 'value' :   'SINHAN' },
    { 'text': '우리은행', 'value' :   'URI' },
    { 'text': 'SC제일은행', 'value' :   'SCJEIL' },
    { 'text': '한국씨티은행', 'value' :   'HANKUKCITY' },
    { 'text': '케이뱅크은행', 'value' :   'KBANK' },
    { 'text': '카카오뱅크은행', 'value' :   'KAKAO' },
    { 'text': '토스뱅크은행', 'value' :   'TOS' },
    { 'text': '기타은행', 'value' :   'ETC' }
  ];
  // 기본값 설정
  String? _selectedbankname = '';

  @override
  Widget build(BuildContext context) {
    return   Scaffold(
      appBar: ComponentAppbar(
        text: '내 정보',
        page: '/page_change_my_info',
      ),
      body: SingleChildScrollView(
        /** 전체를 감싸는 컨테이너 **/
          child: Container(
            padding: EdgeInsets.fromLTRB(25, 75, 25, 25),
            color: colorMainBg,
            child: FormBuilder(
              key: _formKey,
              child:Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  /**프로필 변경 창**/
                  Container(
                    width: MediaQuery
                        .of(context)
                        .size
                        .width * 0.2,
                    height: MediaQuery
                        .of(context)
                        .size
                        .height * 0.2,
                    alignment: Alignment.center,
                    child: Container(
                      width: 80,
                      child: FormBuilderImagePicker(
                        name: 'profileImg',
                        displayCustomType: (obj) =>
                        obj is ApiImage ? obj.imageUrl : obj,
                        decoration: const InputDecoration(
                          labelText: 'Pick Photos',
                        ),
                        availableImageSources: const [ImageSourceOption.gallery],
                        transformImageWidget: (context, displayImage) => Card(
                          shape: CircleBorder(),
                          clipBehavior: Clip.antiAlias,
                          child: SizedBox.expand(
                            child: displayImage,
                          ),
                        ),
                        showDecoration: false,
                        maxImages: 1,
                        previewAutoSizeWidth: false,
                        initialValue: [
                         'https://storage.googleapis.com/md-example-bucket/piak.jpeg'
                        ],
                      ),
                    ),
                    // child: CircleAvatar(
                    //   child: Icon(
                    //     Icons.account_circle_rounded,
                    //     size: 80,
                    //     color: colorText,
                    //   ),
                    // ),
                  ),
                  /**이름 수정창**/
                  Container(
                      child:Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [

                          Text(
                            '이름 입력',
                            style: TextStyle(
                                fontSize: fontSizeSuper,
                                fontFamily: 'NotoSans_Bold',
                                color: colorText,
                                letterSpacing: -0.5
                            ),
                          ),
                          FormBuilderTextField(
                            name: 'name',
                            // @TODO 기본값
                            initialValue: '${_response!.name}',
                            /**커서 색상**/
                            cursorColor:colorPoint,
                            /**키보드 바깥 영역 탭할 시 키보드 닫기**/
                            onTapOutside: (event) =>FocusManager.instance.primaryFocus?.unfocus(),
                            keyboardType: TextInputType.text,/** 키보드 기본 텍스트 키 올라오게 하기 **/
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(/** 기본 텍스트 필드 인풋 라인 색상 **/
                                borderSide: BorderSide(color: colorButton),
                              ),
                              focusedBorder: UnderlineInputBorder(/** 선택됐을 시 텍스트 필드 인풋 라인 색상 **/
                                borderSide: BorderSide(color: Colors.orangeAccent),
                              ),
                            ),
                            style: TextStyle(
                                fontSize: fontSizeMainText,
                                fontFamily:'NotoSans_NotoSansKR-Regular',
                                color: colorHintText,
                                letterSpacing: -0.5
                            ),
                          )
                        ],
                      )
                  ),
                  // Container(
                  //     child:Column(
                  //       crossAxisAlignment: CrossAxisAlignment.start,
                  //       children: [
                  //         Text(
                  //           '비밀번호',
                  //           style: TextStyle(
                  //               fontSize: fontSizeSuper,
                  //               fontFamily: 'NotoSans_Bold',
                  //               color: colorText,
                  //               letterSpacing: -0.5
                  //           ),
                  //         ),
                  //         TextField(
                  //           /**커서 색상**/
                  //           cursorColor:colorPoint,
                  //           /**비밀번호 암호화 서식**/
                  //           obscureText: true,
                  //           /**키보드 바깥 영역 탭할 시 키보드 닫기**/
                  //           onTapOutside: (event) =>FocusManager.instance.primaryFocus?.unfocus(),
                  //           keyboardType: TextInputType.text,/** 키보드 기본 텍스트 키 올라오게 하기 **/
                  //           decoration: InputDecoration(
                  //             enabledBorder: UnderlineInputBorder(/** 기본 텍스트 필드 인풋 라인 색상 **/
                  //               borderSide: BorderSide(color: colorButton),
                  //             ),
                  //             focusedBorder: UnderlineInputBorder(/** 선택됐을 시 텍스트 필드 인풋 라인 색상 **/
                  //               borderSide: BorderSide(color: Colors.orangeAccent),
                  //             ),
                  //           ),
                  //           style: TextStyle(
                  //               fontSize: fontSizeMainText,
                  //               fontFamily:'NotoSans_NotoSansKR-Regular',
                  //               color: colorHintText,
                  //               letterSpacing: -0.5
                  //           ),
                  //         )
                  //
                  //       ],
                  //     )
                  // ),
                  Container(
                    margin: EdgeInsets.only(top: 60.0),
                    child: Row(
                      children: [
                        Text(
                          '배달 희망 지역',
                          style: TextStyle(
                              fontSize: fontSizeSuper,
                              fontFamily: 'NotoSans_Bold',
                              color: colorText,
                              letterSpacing: -0.5
                          ),
                        ),
                      ],
                    ),
                  ),
                  /** 배달 지역 드롭 메뉴 **/
                  Container(
                      width: MediaQuery.of(context).size.width,
                      child: DropdownButton(
                        isExpanded: true,
                        style: TextStyle(
                            fontSize: fontSizeMainText,
                            fontFamily:'NotoSans_NotoSansKR-Regular',
                            color: colorHintText,
                            letterSpacing: -0.5
                        ),
                        value: _selectedDropdown,
                        items: _dropdownList.map((Map<String,dynamic> item) {
                          return DropdownMenuItem<String>(
                            child: Text(item['text']),
                            value: item['value'],
                          );
                        }).toList(),
                        onChanged: (dynamic value) {
                          setState(() {
                            _selectedDropdown = value!;
                          });
                        },
                      )
                  ),
                  /** 배달 수단 선택 타이틀 **/
                  Container(
                      margin: EdgeInsets.only(top: 40.0),
                      child: Row(
                        children: [
                          Text(
                            '배달 수단 선택',
                            style: TextStyle(
                                fontSize: fontSizeSuper,
                                fontFamily: 'NotoSans_Bold',
                                color: colorText,
                                letterSpacing: -0.5
                            ),
                          ),
                        ],
                      )
                  ),
                  /** 배달 수단 선택 **/
                  Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                            width: MediaQuery.of(context).size.width,
                            child: DropdownButton(
                              isExpanded: true,
                              style: TextStyle(
                                  fontSize: fontSizeMainText,
                                  fontFamily:'NotoSans_NotoSansKR-Regular',
                                  color: colorHintText,
                                  letterSpacing: -0.5
                              ),
                              value: _selectedKindDropdown,
                              items: _kindList.map((Map<String,dynamic> item) {
                                return DropdownMenuItem<String>(
                                  child: Text(item['text']),
                                  value: item['value'],
                                );
                              }).toList(),
                              onChanged: (dynamic value) {
                                setState(() {
                                  _selectedKindDropdown = value!;
                                });
                              },
                            )
                        ),

                      ],
                    ),
                  ),
                  Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        /** 배달 수단 정보 타이틀 **/
                        Container(
                            margin: EdgeInsets.only(top: 40.0),
                            child: Text(
                              '배달 수단 정보',
                              style: TextStyle(
                                  fontSize: fontSizeSuper,
                                  fontFamily: 'NotoSans_Bold',
                                  color: colorText,
                                  letterSpacing: -0.5
                              ),
                            )
                        ),
                        /** 차종 및 번호판 입력 텍스트 필드 **/
                        Container(
                          margin: EdgeInsets.only(top: 10.0),
                          padding: EdgeInsets.fromLTRB(15, 5, 15, 0),
                          height: 50,
                          decoration:BoxDecoration(
                              color: colorJoin,
                              borderRadius: BorderRadius.circular(6.0)
                          ),
                          child: FormBuilderTextField(
                            name: 'driveNumber',

                            // @TODO 기본값
                            initialValue: '${_response!.driveNumber}',

                            /** 키보드 바깥 영역 탭할 시 키보드 닫기 **/
                            onTapOutside: (event) => FocusManager.instance.primaryFocus?.unfocus(),
                            keyboardType: TextInputType.text, /** 키보드 기본 텍스트 키 올라오게 하기 **/
                            decoration:InputDecoration(
                              isDense: true, /** 텍스트 필드 패딩 없애기 **/
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide.none, /** 기본 텍스트 필드 인풋 라인 없음 **/
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide.none, /** 선택됐을 시 텍스트 필드 인풋 라인 없음 **/
                              ),
                              hintText: '상세 배달 수단 정보 및 번호판',
                              hintStyle: TextStyle(
                                  fontSize: fontSizeMainText,
                                  fontFamily:'NotoSans_NotoSansKR-Regular',
                                  color: colorHintText,
                                  letterSpacing: -0.5
                              ),
                            ),
                            style: TextStyle(
                                fontSize: fontSizeMainText,
                                fontFamily:'NotoSans_NotoSansKR-Regular',
                                color: colorHintText,
                                letterSpacing: -0.5
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 20),
                  Container(
                    margin: EdgeInsets.only(top: 40.0),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            /** 은행명 **/
                            Text(
                              '은행 선택',
                              style: TextStyle(
                                  fontSize: fontSizeSuper,
                                  fontFamily: 'NotoSans_Bold',
                                  color: colorText,
                                  letterSpacing: -0.5
                              ),
                            ),
                          ],
                        ),
                        /** 은행 드롭 메뉴 **/
                        Container(
                            width: MediaQuery.of(context).size.width,
                            child: DropdownButton(
                              // hint: Text(
                              //   '* 은행을 선택해 주세요'
                              // ),
                              isExpanded: true,
                              style: TextStyle(
                                fontSize: fontSizeMainText,
                                fontFamily: 'NotoSans_NotoSansKR-Regular',
                                color: colorHintText,
                                letterSpacing: -0.5,
                              ),
                              value: _selectedbankname,
                              items: _banknameList.map((Map<String,dynamic> item) {
                                return DropdownMenuItem<String>(
                                  child: Text(item['text']),
                                  value: item['value'],
                                );
                              }).toList(),
                              onChanged: (dynamic value) {
                                setState(() {
                                  _selectedbankname = value!;
                                });
                              },
                            )
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 20),
                  /** 예금주명 입력 폼 전체 **/
                  Container(
                      child:Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            '예금주명 입력',
                            style: TextStyle(
                                fontSize: fontSizeSuper,
                                fontFamily: 'NotoSans_Bold',
                                color: colorText,
                                letterSpacing: -0.5
                            ),
                          ),
                          FormBuilderTextField(
                            name: 'bankOwner',
                            // @TODO 기본값
                            initialValue: '${_response!.bankOwner}',

                            /**커서 색상**/
                            cursorColor:colorPoint,
                            /**키보드 바깥 영역 탭할 시 키보드 닫기**/
                            onTapOutside: (event) =>FocusManager.instance.primaryFocus?.unfocus(),
                            keyboardType: TextInputType.text,/** 키보드 기본 텍스트 키 올라오게 하기 **/
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(/** 기본 텍스트 필드 인풋 라인 색상 **/
                                borderSide: BorderSide(color: colorButton),
                              ),
                              focusedBorder: UnderlineInputBorder(/** 선택됐을 시 텍스트 필드 인풋 라인 색상 **/
                                borderSide: BorderSide(color: Colors.orangeAccent),
                              ),
                            ),
                            style: TextStyle(
                                fontSize: fontSizeMainText,
                                fontFamily:'NotoSans_NotoSansKR-Regular',
                                color: colorHintText,
                                letterSpacing: -0.5
                            ),
                          )
                        ],
                      )
                  ),

                  /** 계좌번호 입력 폼 전체 **/
                  Container(
                    margin: EdgeInsets.only(top: 40.0),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            /** 계좌번호 **/
                            Text(
                              '계좌번호 입력',
                              style: TextStyle(
                                  fontSize: fontSizeSuper,
                                  fontFamily: 'NotoSans_Bold',
                                  color: colorText,
                                  letterSpacing: -0.5
                              ),
                            ),
                          ],
                        ),
                        /** 계좌번호 입력 텍스트 필드 **/
                        FormBuilderTextField(
                          name: 'bankNumber',
                          // @Todo 테스트 값
                          initialValue: '110-545-873215',
                          validator: FormBuilderValidators.compose([
                            FormBuilderValidators.required(errorText: formErrorRequired), /** 필수 **/
                          ]),

                          /** 키보드 바깥 영역 탭할 시 키보드 닫기 **/
                          onTapOutside: (event) => FocusManager.instance.primaryFocus?.unfocus(),
                          keyboardType: TextInputType.number, /** 키보드 기본 숫자키 올라오게 하기 **/
                          inputFormatters: [
                            FilteringTextInputFormatter.digitsOnly, /** 숫자만 입력 받고 싶을 때 **/
                          ],
                          decoration:InputDecoration(
                            enabledBorder: UnderlineInputBorder( /** 기본 텍스트 필드 인풋 라인 색상 **/
                                borderSide: BorderSide(color: colorButton)
                            ),
                            focusedBorder: UnderlineInputBorder( /** 선택됐을 시 텍스트 필드 인풋 라인 색상 **/
                              borderSide: BorderSide(color: Colors.orangeAccent),
                            ),
                            hintText: '* 계좌번호를 입력해 주세요.',
                            hintStyle: TextStyle(
                                fontSize: fontSizeMainText,
                                fontFamily:'NotoSans_NotoSansKR-Regular',
                                color: colorHintText,
                                letterSpacing: -0.5
                            ),
                          ),
                          style: TextStyle(
                              fontSize: fontSizeMainText,
                              fontFamily:'NotoSans_NotoSansKR-Regular',
                              color: colorHintText,
                              letterSpacing: -0.5
                          ),
                        ),
                      ],
                    ),
                  ),

                  SizedBox(height: 20),

                  Container(
                    margin: EdgeInsets.only(top: 80.0),
                    height: 45,
                    width: MediaQuery.of(context).size.width,
                    child: ElevatedButton(
                      child: Text(
                        '수정',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: fontSizeMainText,
                          fontFamily: 'NotoSans_NotoSansKR-Regular',
                          letterSpacing: -0.5,
                        ),
                      ),
                      style: ElevatedButton.styleFrom(
                          backgroundColor: colorPoint,
                          foregroundColor: colorPoint,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(6)
                          ),
                          padding: EdgeInsets.zero,
                          elevation: 0
                      ),
                      onPressed: () async {
                        if( _selectedbankname == '' ){
                          showAlertDialog(context,
                              title: "은행정보가 입력되지않았습니다.",
                              content: "은행을 다시 선택해주세요.",
                              actions: <Widget>[
                                TextButton(onPressed: ()=>Navigator.pop(context), child: const Text("확인"))
                              ]);
                          return;
                        }
                        if( _selectedDropdown == '' ){
                          showAlertDialog(context,
                              title: "희망지역이 입력되지않았습니다.",
                              content: "희망지역을 다시 선택해주세요.",
                              actions: <Widget>[
                                TextButton(onPressed: ()=>Navigator.pop(context), child: const Text("확인"))
                              ]);
                          return;
                        }
                        if( _selectedKindDropdown == '' ){
                          showAlertDialog(context,
                              title: "배달수단이 입력되지않았습니다.",
                              content: "배달수단을 다시 선택해주세요.",
                              actions: <Widget>[
                                TextButton(onPressed: ()=>Navigator.pop(context), child: const Text("확인"))
                              ]);
                          return;
                        }
                        if(_formKey.currentState!.saveAndValidate()) {

                          MyInfoChangeRequest changeRequest = MyInfoChangeRequest(
                            _formKey.currentState!.fields['name']!.value,
                            _selectedDropdown!,
                            _selectedKindDropdown!,
                            _formKey.currentState!.fields['driveNumber']!.value,
                            _formKey.currentState!.fields['bankOwner']!.value,
                            _selectedbankname!,
                            _formKey.currentState!.fields['bankNumber']!.value,
                          );

                          // 1. 사용자 이미지 파일 선택한 경우 API 파일 업로드
                          List<dynamic> imgList = _formKey.currentState!.fields['profileImg']!.value;
                          for ( dynamic img in imgList ){
                            if ( img is XFile ){
                              XFile file = img;
                              dynamic sendData = file.path;
                              var formData = FormData.fromMap({'file': await MultipartFile.fromFile(sendData)});
                              await _putImage(formData);
                              break;
                            }
                          }

                          // 2. 내 정보 수정 요청
                          await _putRider(changeRequest).then((value) => null);

                        }
                      },
                    ),
                  )
                ],
              ) ,
            ),
          )
      ),
    );
  }
}

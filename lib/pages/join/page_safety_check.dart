import 'package:flutter/material.dart';
import 'package:matddak_v1/model/rider/join_request.dart';

import '../../config/config_color.dart';
import '../../config/config_text_size.dart';

class PageSafetyCheck extends StatelessWidget {
  const PageSafetyCheck({super.key});




  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        /** 전체를 감싸는 컨테이너 **/
        child: Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.fromLTRB(25, 70, 25, 25),
          color: Color(0xff1A1A1A),
          child: Column(
            children: [
              /** 1, 2, 3 단계 표시 **/
              Container(
                child: Row(
                  children: [
                    /** 1단계 **/
                    Container(
                        padding: EdgeInsets.only(bottom: 2.0),
                        width: 20,
                        height: 20,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: colorPoint,
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              '1',
                              style: TextStyle(
                                fontSize: fontSizeMicro,
                                fontFamily: 'NotoSans_Bold',
                                color: Colors.white,
                              ),
                            ),
                          ],
                        )
                    ),
                    /** 단계 사이 선 **/
                    Container(
                      height: 1,
                      width: 25,
                      color: colorPoint,
                    ),
                    /** 2단계 **/
                    Container(
                      padding: EdgeInsets.only(bottom: 2.0),
                      width: 20,
                      height: 20,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: colorPoint,
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            '2',
                            style: TextStyle(
                              fontSize: fontSizeMicro,
                              fontFamily: 'NotoSans_NotoSansKR-Regular',
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                    ),
                    /** 단계 사이 선 **/
                    Container(
                      height: 1,
                      width: 25,
                      color: colorPoint,
                    ),
                    /** 3단계 **/
                    Container(
                      padding: EdgeInsets.only(bottom: 2.0),
                      width: 20,
                      height: 20,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: colorPoint,
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            '3',
                            style: TextStyle(
                              fontSize: fontSizeMicro,
                              fontFamily: 'NotoSans_NotoSansKR-Regular',
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              /** 안전 사항 타이틀 텍스트 **/
              Container(
                margin: EdgeInsets.only(top: 20.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      '안전 사항',
                      style: TextStyle(
                          fontSize: fontSizeTitle,
                          fontFamily: 'NotoSans_Bold',
                          letterSpacing:-0.5,
                          color: colorPoint
                      ),
                    ),
                    Container(
                      child: Row(
                        children: [
                          Text(
                              '딱! 5개만',
                            style: TextStyle(
                                fontSize: fontSizeTitle,
                                fontFamily: 'NotoSans_Bold',
                                letterSpacing:-0.5,
                                color: colorPoint
                            ),
                          ),
                          Text(
                            ' 지켜주세요',
                            style: TextStyle(
                                fontSize: fontSizeTitle,
                                fontFamily: 'NotoSans_NotoSansKR-Light',
                                letterSpacing:-0.5,
                                color: colorPoint
                            ),
                          ),
                        ],
                      ),
                    ),
                    /** 안전사항 1 운행 전 **/
                    Container(
                      margin: EdgeInsets.only(top: 60.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Container(
                                padding: EdgeInsets.only(bottom: 2.0),
                                width: 20,
                                height: 20,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: colorTextBright,
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      '1',
                                      style: TextStyle(
                                        fontSize: fontSizeMicro,
                                        fontFamily: 'NotoSans_Bold',
                                        color: colorBox,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.fromLTRB(10, 0, 0, 5),
                                child: Text(
                                  '운행 전',
                                  style: TextStyle(
                                    fontFamily: 'NotoSans_Bold',
                                    color: colorTextBright,
                                    fontSize: fontSizeSuper,
                                    letterSpacing: -0.5
                                  ),
                                ),
                              )
                            ],
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 30.0),
                            child: Text(
                              '헬멧 / 보호대 착용, 운송수단을 점검해 주세요!',
                              style: TextStyle(
                                  fontFamily: 'NotoSans_NotoSansKR-Regular',
                                  color: colorHintText,
                                  fontSize: fontSizeMainText,
                                  letterSpacing: -0.5
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    /** 안전사항 2 주문 확인 시 **/
                    Container(
                      margin: EdgeInsets.only(top: 30.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Container(
                                padding: EdgeInsets.only(bottom: 2.0),
                                width: 20,
                                height: 20,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: colorTextBright,
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      '2',
                                      style: TextStyle(
                                        fontSize: fontSizeMicro,
                                        fontFamily: 'NotoSans_Bold',
                                        color: colorBox,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.fromLTRB(10, 0, 0, 5),
                                child: Text(
                                  '주문 확인 시',
                                  style: TextStyle(
                                      fontFamily: 'NotoSans_Bold',
                                      color: colorTextBright,
                                      fontSize: fontSizeSuper,
                                      letterSpacing: -0.5
                                  ),
                                ),
                              )
                            ],
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 30.0),
                            child: Text(
                              '운행 중 핸드폰 조작 금지! 주정차 후 확인해 주세요!',
                              style: TextStyle(
                                  fontFamily: 'NotoSans_NotoSansKR-Regular',
                                  color: colorHintText,
                                  fontSize: fontSizeMainText,
                                  letterSpacing: -0.5
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    /** 안전사항 3 신호 준수 **/
                    Container(
                      margin: EdgeInsets.only(top: 30.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Container(
                                padding: EdgeInsets.only(bottom: 2.0),
                                width: 20,
                                height: 20,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: colorTextBright,
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      '3',
                                      style: TextStyle(
                                        fontSize: fontSizeMicro,
                                        fontFamily: 'NotoSans_Bold',
                                        color: colorBox,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.fromLTRB(10, 0, 0, 5),
                                child: Text(
                                  '신호 준수',
                                  style: TextStyle(
                                      fontFamily: 'NotoSans_Bold',
                                      color: colorTextBright,
                                      fontSize: fontSizeSuper,
                                      letterSpacing: -0.5
                                  ),
                                ),
                              )
                            ],
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 30.0),
                            child: Text(
                              '꼬리물기 금지! 황색 / 적색 신호를 잘 지켜주세요!',
                              style: TextStyle(
                                  fontFamily: 'NotoSans_NotoSansKR-Regular',
                                  color: colorHintText,
                                  fontSize: fontSizeMainText,
                                  letterSpacing: -0.5
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    /** 안전사항 4 범규 준수 **/
                    Container(
                      margin: EdgeInsets.only(top: 30.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Container(
                                padding: EdgeInsets.only(bottom: 2.0),
                                width: 20,
                                height: 20,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: colorTextBright,
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      '3',
                                      style: TextStyle(
                                        fontSize: fontSizeMicro,
                                        fontFamily: 'NotoSans_Bold',
                                        color: colorBox,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.fromLTRB(10, 0, 0, 5),
                                child: Text(
                                  '법규 준수',
                                  style: TextStyle(
                                      fontFamily: 'NotoSans_Bold',
                                      color: colorTextBright,
                                      fontSize: fontSizeSuper,
                                      letterSpacing: -0.5
                                  ),
                                ),
                              )
                            ],
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 30.0),
                            child: Text(
                              '인도 / 횡단보도 주행 금지! 보행자를 보호해 주세요!',
                              style: TextStyle(
                                  fontFamily: 'NotoSans_NotoSansKR-Regular',
                                  color: colorHintText,
                                  fontSize: fontSizeMainText,
                                  letterSpacing: -0.5
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    /** 안전사항 5 도보 이동 시 **/
                    Container(
                      margin: EdgeInsets.only(top: 30.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Container(
                                padding: EdgeInsets.only(bottom: 2.0),
                                width: 20,
                                height: 20,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: colorTextBright,
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      '5',
                                      style: TextStyle(
                                        fontSize: fontSizeMicro,
                                        fontFamily: 'NotoSans_Bold',
                                        color: colorBox,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.fromLTRB(10, 0, 0, 5),
                                child: Text(
                                  '도보 이동 시',
                                  style: TextStyle(
                                      fontFamily: 'NotoSans_Bold',
                                      color: colorTextBright,
                                      fontSize: fontSizeSuper,
                                      letterSpacing: -0.5
                                  ),
                                ),
                              )
                            ],
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 30.0),
                            child: Text(
                              '픽업자 / 배달지 계단 및 문턱을 조심 하세요!',
                              style: TextStyle(
                                  fontFamily: 'NotoSans_NotoSansKR-Regular',
                                  color: colorHintText,
                                  fontSize: fontSizeMainText,
                                  letterSpacing: -0.5
                              ),
                            ),
                          ),
                          /** 맛딱드림 시작하기 버튼 **/
                          Container(
                            margin: EdgeInsets.only(top: 135.0),
                            height: 45,
                            width: MediaQuery.of(context).size.width,
                            child: ElevatedButton(
                              child: Text(
                                '맛딱드림 시작하기',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: fontSizeMainText,
                                  fontFamily: 'NotoSans_NotoSansKR-Regular',
                                  letterSpacing: -0.5,
                                ),
                              ),
                              style: ElevatedButton.styleFrom(
                                  primary: colorButton,
                                  onPrimary: Colors.grey,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(6)
                                  ),
                                  padding: EdgeInsets.zero,
                                  elevation: 0
                              ),
                              onPressed: () {
                                Navigator.of(context).pushNamed('/page_login');
                              },
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}


import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:matddak_v1/config/config_form_validator.dart';
import 'package:matddak_v1/config/config_text_size.dart';
import 'package:matddak_v1/input_form_hyphen/id_num.dart';
import 'package:matddak_v1/pages/join/page_delivery_info.dart';


import '../../config/config_color.dart';
import '../../functions/util_dialog.dart';
import '../../model/rider/join_request.dart';

class PageAccountInfo extends StatefulWidget {

  const PageAccountInfo({super.key, required this.joinRequest});

  final JoinRequest joinRequest;

  @override
  State<PageAccountInfo> createState() => _PageAccountInfoState();
}

class _PageAccountInfoState extends State<PageAccountInfo> {

  final _formKey = GlobalKey<FormBuilderState>();

  //step1에서 입력받은 데이터를 받아와서 변경하는 부분
  void putData() {
    setState(() {
      //widget.joinRequest;
    });
  }

  @override
  void initState() {
    super.initState();
    putData();
  }

  /*List<String> _dropdownList = [
    '* 은행을 선택해 주세요', '국민은행', '하나은행', '신한은행', '우리은행', '제일은행', '한국은행', '케이뱅크', '카카오뱅크', '토쓰뱅크', '기타'
  ];*/

  List<Map<String,String>> _dropdownList = [
    { 'text':'* 은행을 선택해 주세요', 'value':'' },
    { 'text': '국민은행', 'value' :   'KUKMIN' },
    { 'text': '하나은행', 'value' :   'HANA' },
    { 'text': '신한은행', 'value' :   'SINHAN' },
    { 'text': '우리은행', 'value' :   'URI' },
    { 'text': '제일은행', 'value' :   'SCJEIL' },
    { 'text': '한국씨티은행', 'value' :   'HANKUKCITY' },
    { 'text': '케이뱅크은행', 'value' :   'KBANK' },
    { 'text': '카카오뱅크은행', 'value' :   'KAKAO' },
    { 'text': '토스뱅크은행', 'value' :   'TOS' },
    { 'text': '기타은행', 'value' :   'ETC' }
  ];
  // 기본값 설정
  String? _selectedDropdown = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true, /** 키보드가 올라왔을 때 입력 창 키보드에 가려지지 않게 함 **/
      body: SingleChildScrollView(
        /** 전체 감싸는 컨테이너 **/
        child: Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.fromLTRB(25, 70, 25, 25),
          color: colorBg,
          child: Column(
            children: [
              /** 1, 2, 3 단계 표시 **/
              Container(
                child: Row(
                  children: [
                    /** 1단계 **/
                    Container(
                      padding: EdgeInsets.only(bottom: 2.0),
                      width: 20,
                      height: 20,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                        color: colorPoint,
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            '1',
                            style: TextStyle(
                              fontSize: fontSizeMicro,
                              fontFamily: 'NotoSans_Bold',
                              color: Colors.white,
                            ),
                          ),
                        ],
                      )
                    ),
                    /** 단계 사이 선 **/
                    Container(
                      height: 1,
                      width: 25,
                      color: colorButton,
                    ),
                    /** 2단계 **/
                    Container(
                      padding: EdgeInsets.only(bottom: 2.0),
                      width: 20,
                      height: 20,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: colorButton,
                      ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              '2',
                              style: TextStyle(
                                fontSize: fontSizeMicro,
                                fontFamily: 'NotoSans_NotoSansKR-Regular',
                                color: Color.fromRGBO(33, 33, 33, 1),
                              ),
                            ),
                          ],
                        ),
                    ),
                    /** 단계 사이 선 **/
                    Container(
                      height: 1,
                      width: 25,
                      color: colorButton,
                    ),
                    /** 3단계 **/
                    Container(
                      padding: EdgeInsets.only(bottom: 2.0),
                      width: 20,
                      height: 20,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: colorButton,
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            '3',
                            style: TextStyle(
                              fontSize: fontSizeMicro,
                              fontFamily: 'NotoSans_NotoSansKR-Regular',
                              color: Color.fromRGBO(33, 33, 33, 1),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              /** 계좌 정보 타이틀 텍스트 **/
              Container(
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: 20.0),
                      child: Row(
                        children: [
                          Container(
                            child: Text(
                              '페이',
                              style: TextStyle(
                                  fontSize: fontSizeTitle,
                                  fontFamily: 'NotoSans_Bold',
                                  letterSpacing:-0.5,
                                  color: colorPoint
                              ),
                            ),
                          ),
                          Container(
                            child: Text(
                              '를 받을 계좌번호를',
                              style: TextStyle(
                                  fontSize: fontSizeTitle,
                                  fontFamily: 'NotoSans_NotoSansKR-Light',
                                  letterSpacing:-0.5,
                                  color: colorPoint
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      child: Row(
                        children: [
                          Text(
                            '등록해 주세요',
                            style: TextStyle(
                                fontSize: fontSizeTitle,
                                fontFamily: 'NotoSans_NotoSansKR-Light',
                                letterSpacing:-0.5,
                                color: colorPoint
                            ),
                          ),
                        ],
                      )
                    ),
                  ],
                ),
              ),
              FormBuilder(
                  key: _formKey,
                  child: Column(
                    children: [
                      /** 예금주명 입력 폼 전체 **/
                      Container(
                        margin: EdgeInsets.only(top: 60.0),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                /** 예금주명 입력 **/
                                Text(
                                  '예금주명 입력',
                                  style: TextStyle(
                                      fontSize: fontSizeSuper,
                                      fontFamily: 'NotoSans_Bold',
                                      color: colorText,
                                      letterSpacing: -0.5
                                  ),
                                ),
                              ],
                            ),
                            /** 예금주명 입력 텍스트 필드 **/
                            FormBuilderTextField(
                              name: 'bankOwner',

                              // @TODO 테스트 값
                              initialValue: '홍길동',
                              validator: FormBuilderValidators.compose([
                                FormBuilderValidators.required(errorText: formErrorRequired), /** 필수 **/
                              ]),

                              /** 키보드 바깥 영역 탭할 시 키보드 닫기 **/
                              onTapOutside: (event) => FocusManager.instance.primaryFocus?.unfocus(),
                              keyboardType: TextInputType.text, /** 키보드 기본 텍스트 키 올라오게 하기 **/
                              decoration:InputDecoration(
                                enabledBorder: UnderlineInputBorder( /** 기본 텍스트 필드 인풋 라인 색상 **/
                                  borderSide: BorderSide(color: colorButton),
                                ),
                                focusedBorder: UnderlineInputBorder( /** 선택됐을 시 텍스트 필드 인풋 라인 색상 **/
                                  borderSide: BorderSide(color: Colors.orangeAccent),
                                ),
                                hintText: '* 예금주명을 입력해 주세요.',
                                hintStyle: TextStyle(
                                    fontSize: fontSizeMainText,
                                    fontFamily:'NotoSans_NotoSansKR-Regular',
                                    color: colorHintText,
                                    letterSpacing: -0.5
                                ),
                              ),
                              style: TextStyle(
                                  fontSize: fontSizeMainText,
                                  fontFamily:'NotoSans_NotoSansKR-Regular',
                                  color: colorHintText,
                                  letterSpacing: -0.5
                              ),
                            ),
                          ],
                        ),
                      ),
                      /** 은행명 입력 폼 전체 **/
                      Container(
                        margin: EdgeInsets.only(top: 40.0),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                /** 은행명 **/
                                Text(
                                  '은행 선택',
                                  style: TextStyle(
                                      fontSize: fontSizeSuper,
                                      fontFamily: 'NotoSans_Bold',
                                      color: colorText,
                                      letterSpacing: -0.5
                                  ),
                                ),
                              ],
                            ),
                            /** 은행 드롭 메뉴 **/
                            Container(
                                width: MediaQuery.of(context).size.width,
                                child: DropdownButton(
                                  // hint: Text(
                                  //   '* 은행을 선택해 주세요'
                                  // ),
                                  isExpanded: true,
                                  style: TextStyle(
                                    fontSize: fontSizeMainText,
                                    fontFamily: 'NotoSans_NotoSansKR-Regular',
                                    color: colorHintText,
                                    letterSpacing: -0.5,
                                  ),
                                  value: _selectedDropdown,
                                  items: _dropdownList.map((Map<String,dynamic> item) {
                                    return DropdownMenuItem<String>(
                                      child: Text(item['text']),
                                      value: item['value'],
                                    );
                                  }).toList(),
                                  onChanged: (dynamic value) {
                                    setState(() {
                                      _selectedDropdown = value!;
                                    });
                                  },
                                )
                            ),
                          ],
                        ),
                      ),
                      /** 계좌번호 입력 폼 전체 **/
                      Container(
                        margin: EdgeInsets.only(top: 40.0),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                /** 계좌번호 **/
                                Text(
                                  '계좌번호 입력',
                                  style: TextStyle(
                                      fontSize: fontSizeSuper,
                                      fontFamily: 'NotoSans_Bold',
                                      color: colorText,
                                      letterSpacing: -0.5
                                  ),
                                ),
                              ],
                            ),
                            /** 계좌번호 입력 텍스트 필드 **/
                            FormBuilderTextField(
                              name: 'bankNumber',
                              // @Todo 테스트 값
                              initialValue: '110-545-873215',
                              validator: FormBuilderValidators.compose([
                                FormBuilderValidators.required(errorText: formErrorRequired), /** 필수 **/
                              ]),

                              /** 키보드 바깥 영역 탭할 시 키보드 닫기 **/
                              onTapOutside: (event) => FocusManager.instance.primaryFocus?.unfocus(),
                              keyboardType: TextInputType.number, /** 키보드 기본 숫자키 올라오게 하기 **/
                              inputFormatters: [
                                FilteringTextInputFormatter.digitsOnly, /** 숫자만 입력 받고 싶을 때 **/
                              ],
                              decoration:InputDecoration(
                                enabledBorder: UnderlineInputBorder( /** 기본 텍스트 필드 인풋 라인 색상 **/
                                    borderSide: BorderSide(color: colorButton)
                                ),
                                focusedBorder: UnderlineInputBorder( /** 선택됐을 시 텍스트 필드 인풋 라인 색상 **/
                                  borderSide: BorderSide(color: Colors.orangeAccent),
                                ),
                                hintText: '* 계좌번호를 입력해 주세요.',
                                hintStyle: TextStyle(
                                    fontSize: fontSizeMainText,
                                    fontFamily:'NotoSans_NotoSansKR-Regular',
                                    color: colorHintText,
                                    letterSpacing: -0.5
                                ),
                              ),
                              style: TextStyle(
                                  fontSize: fontSizeMainText,
                                  fontFamily:'NotoSans_NotoSansKR-Regular',
                                  color: colorHintText,
                                  letterSpacing: -0.5
                              ),
                            ),
                          ],
                        ),
                      ),
                      /** 주민등록번호 입력 폼 전체 **/
                      Container(
                        margin: EdgeInsets.only(top: 40.0),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                /** 계좌번호 **/
                                Text(
                                  '주민등록번호 입력',
                                  style: TextStyle(
                                      fontSize: fontSizeSuper,
                                      fontFamily: 'NotoSans_Bold',
                                      color: colorText,
                                      letterSpacing: -0.5
                                  ),
                                ),
                              ],
                            ),
                            /** 주민등록번호 입력 텍스트 필드 **/
                            FormBuilderTextField(
                              name: 'bankIdNum',
                              // @Todo 테스트 값
                              initialValue: '850101-1234567',
                              validator: FormBuilderValidators.compose([
                                FormBuilderValidators.required(errorText: formErrorRequired), /** 필수 **/
                                FormBuilderValidators.minLength(14, errorText: formErrorMinLength(14)), /** 8자 이상 **/
                              ]),
                              /** 커서 색상 **/
                              cursorColor: colorPoint,

                              /** 키보드 바깥 영역 탭할 시 키보드 닫기 **/
                              onTapOutside: (event) => FocusManager.instance.primaryFocus?.unfocus(),
                              keyboardType: TextInputType.number, /** 키보드 기본 숫자키 올라오게 하기 **/
                              inputFormatters: [
                                FilteringTextInputFormatter.digitsOnly, /** 숫자만 입력 받고 싶을 때 **/
                                IdNumFormatter(), /** 자동 하이픈 000000-0000000 **/
                                LengthLimitingTextInputFormatter(14) /** 14자리만 입력 받도록 하이픈 1개+숫자 13개 **/
                              ],
                              decoration:InputDecoration(
                                enabledBorder: UnderlineInputBorder( /** 기본 텍스트 필드 인풋 라인 색상 **/
                                    borderSide: BorderSide(color: colorButton)
                                ),
                                focusedBorder: UnderlineInputBorder( /** 선택됐을 시 텍스트 필드 인풋 라인 색상 **/
                                  borderSide: BorderSide(color: Colors.orangeAccent),
                                ),
                                hintText: '*주민등록번호를 입력해 주세요.',
                                hintStyle: TextStyle(
                                    fontSize: fontSizeMainText,
                                    fontFamily:'NotoSans_NotoSansKR-Regular',
                                    color: colorHintText,
                                    letterSpacing: -0.5
                                ),
                              ),
                              style: TextStyle(
                                  fontSize: fontSizeMainText,
                                  fontFamily:'NotoSans_NotoSansKR-Regular',
                                  color: colorHintText,
                                  letterSpacing: -0.5
                              ),
                            ),
                            /** 주민등록번호 주의사항 **/
                            Container(
                                margin: EdgeInsets.only(top: 8.0),
                                child: Container(
                                  child: Row(
                                    children: [
                                      Text(
                                        '- 소득세법 등 관련 법령에 따라 주민번호를 수집 및 이용합니다.'
                                            '\n- 비용을 지급 받을 계좌번호를 등록해 주세요.',
                                        style: TextStyle(
                                            fontFamily: 'NotoSans_NotoSansKR-Regular',
                                            fontSize: fontSizeCareful,
                                            letterSpacing: -0.5,
                                            color: colorWarningText,
                                            height: 1.6
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                            ),
                            /** 계속 버튼 **/
                            Container(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(top: 80.0),
                                    height: 45,
                                    width: MediaQuery.of(context).size.width / 2.35,
                                    child: ElevatedButton(
                                      child: Text(
                                        '뒤로',
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: fontSizeMainText,
                                          fontFamily: 'NotoSans_NotoSansKR-Regular',
                                          letterSpacing: -0.5,
                                        ),
                                      ),
                                      style: ElevatedButton.styleFrom(
                                          primary: colorButton,
                                          onPrimary: Colors.grey,
                                          shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.circular(6)
                                          ),
                                          padding: EdgeInsets.zero,
                                          elevation: 0
                                      ),
                                      onPressed: () {
                                        Navigator.of(context).pop();
                                      },
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 80.0),
                                    height: 45,
                                    width: MediaQuery.of(context).size.width / 2.35,
                                    child: ElevatedButton(
                                      child: Text(
                                        '계속',
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: fontSizeMainText,
                                          fontFamily: 'NotoSans_NotoSansKR-Regular',
                                          letterSpacing: -0.5,
                                        ),
                                      ),
                                      style: ElevatedButton.styleFrom(
                                          primary: colorPoint,
                                          onPrimary: colorPoint,
                                          shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.circular(6)
                                          ),
                                          padding: EdgeInsets.zero,
                                          elevation: 0
                                      ),
                                      onPressed: () {

                                        if( _selectedDropdown == '' ){
                                          showAlertDialog(context,
                                              title: "은행정보가 입력되지않았습니다.",
                                              content: "은행을 다시 선택해주세요.",
                                              actions: <Widget>[
                                                TextButton(onPressed: ()=>Navigator.pop(context), child: const Text("확인"))
                                              ]);
                                          return;
                                        }

                                        if(_formKey.currentState!.saveAndValidate()) {

                                          JoinRequest joinRequest = widget.joinRequest;

                                          joinRequest.step2 = Step2Request(
                                            _formKey.currentState!.fields['bankOwner']!.value,
                                            _selectedDropdown,
                                            _formKey.currentState!.fields['bankIdNum']!.value,
                                            _formKey.currentState!.fields['bankNumber']!.value,
                                          );


                                          Navigator.push(
                                              context, MaterialPageRoute(builder: (context) => PageDeliveryInfo(joinRequest: joinRequest)));

                                        }
                                      },
                                    ),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),

                    ],
                  )
              )
            ],
          ),
        ),
      ),
    );
  }
}

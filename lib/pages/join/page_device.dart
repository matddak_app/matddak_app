import 'package:flutter/material.dart';
import 'package:matddak_v1/config/config_color.dart';
import 'package:matddak_v1/model/rider/join_request.dart';
import 'package:matddak_v1/pages/join/page_accont_info.dart';



import '../../config/config_text_size.dart';

class PageDevice extends StatefulWidget {
  const PageDevice({
    super.key,
    required this.joinRequest
  });

  final JoinRequest joinRequest;

  @override
  State<PageDevice> createState() => _PageDeviceState();
}

class _PageDeviceState extends State<PageDevice> {

  //
  // //step1에서 입력받은 데이터를 받아와서 변경하는 부분
  // void putData() {
  //   setState(() {
  //     widget.joinRequest.step1;
  //   });
  // }
  //
  // @override
  // void initState() {
  //   super.initState();
  //     putData();
  // }





  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
          child: Container(
            /** 배경색상 **/
            padding: EdgeInsets.fromLTRB(25, 70, 25, 25),
            color: Color(0xff1A1A1A),
            child: Column(
              children: [
                /** 전체 감싸는 컨테이너 **/
                Container(
                  height: MediaQuery.of(context).size.height*1.05,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      /** 기기 정보 타이틀 텍스트 **/
                      Container(
                        child: Row(
                          children: [
                            Container(
                              child: Text(
                                '기기 정보',
                                style: TextStyle(
                                    fontSize: fontSizeTitle,
                                    fontFamily: 'NotoSans_Bold',
                                    letterSpacing:-0.5,
                                    color: colorPoint
                                ),
                              ),
                            ),
                            Container(
                              child: Text(
                                '를',
                                style: TextStyle(
                                    fontSize: fontSizeTitle,
                                    fontFamily: 'NotoSans_NotoSansKR-Light',
                                    letterSpacing:-0.5,
                                    color: colorPoint
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Text(
                        '확인해 주세요',
                        style: TextStyle(
                            fontSize: fontSizeTitle,
                            fontFamily: 'NotoSans_NotoSansKR-Light',
                            letterSpacing:-0.5,
                            color: colorPoint
                        ),
                      ),
                      /** 기기 정보 서브 텍스트 **/
                      Container(
                          margin: EdgeInsets.only(top: 8.0),
                          child: Text(
                            '타인 계정 도용 방지를 위해서\n등록된 기기에서만 운행이 가능합니다.',
                            style: TextStyle(
                                fontFamily: 'NotoSans_NotoSansKR-Light',
                                fontSize: fontSizeMainText,
                                letterSpacing: -0.5,
                                color: colorText,
                                height: 1.6
                            ),
                          ),
                      ),
                      /** 현재 접속중인 기기 텍스트 **/
                      Container(
                          margin: EdgeInsets.only(top: 60.0),
                          child: Text(
                            '현재 접속중인 기기',
                            style: TextStyle(
                              fontFamily: 'NotoSans_NotoSansKR-Light',
                              fontSize: fontSizeMainText,
                              letterSpacing: -0.5,
                              color: colorText,
                            ),
                          ),
                      ),
                      /** 현재 접속중인 기기 보더 박스 **/
                      Container(
                        margin: EdgeInsets.fromLTRB(0, 8, 0, 0),
                        padding: EdgeInsets.all(15),
                        decoration: BoxDecoration(
                            border: Border.all(
                              color: colorText,
                              width: 1,
                            ),
                            borderRadius: BorderRadius.circular(6.0)
                        ),
                        child: Column(
                          children: [
                            Container(
                              child: Row(
                                children: [
                                  Container(
                                      child: Text(
                                        '현재 기기',
                                        style: TextStyle(
                                            fontSize: fontSizeMainText,
                                            fontFamily: 'NotoSans_Bold',
                                            color: colorText
                                        ),
                                      ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 15.0),
                                    child: Text(
                                      'SM-F721N',
                                      style: TextStyle(
                                          fontSize: fontSizeMainText,
                                          fontFamily: 'NotoSans_Bold',
                                          color: colorText
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              child: Row(
                                children: [
                                  Container(
                                      margin: EdgeInsets.only(top: 5.0),
                                      child: Text(
                                        '기기 번호',
                                        style: TextStyle(
                                            fontSize: fontSizeMainText,
                                            fontFamily: 'NotoSans_Bold',
                                            color: colorText
                                        ),
                                      ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 15.0),
                                    child: Text(
                                      'phoneNumber:${widget.joinRequest.step1?.phoneNumber}',
                                      style: TextStyle(
                                          fontSize: fontSizeMainText,
                                          fontFamily: 'NotoSans_Bold',
                                          color: colorText
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      /** 등록된 기기 텍스트 **/
                      GestureDetector(
                        child: Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                margin: EdgeInsets.only(top: 40.0),
                                child: Text(
                                  '등록된 기기 (0)',
                                  style: TextStyle(
                                      fontFamily: 'NotoSans_NotoSansKR-Light',
                                      fontSize: fontSizeMainText,
                                      letterSpacing: -0.5,
                                      color: colorText,
                                      height: 1.55
                                  ),
                                )
                              ),
                              /** 등록된 기기 박스 **/
                              Container(
                                margin: EdgeInsets.only(top: 8.0),
                                padding: EdgeInsets.all(15),
                                decoration: BoxDecoration(
                                    color: colorBox,
                                    borderRadius: BorderRadius.circular(6.0)
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      '등록된 기기가 없습니다.\n신규 기기를 등록해 주세요.',
                                      style: TextStyle(
                                          fontFamily: 'NotoSans_NotoSansKR-Light',
                                          fontSize: fontSizeMainText,
                                          letterSpacing: -0.5,
                                          color: colorText,
                                          height: 1.55
                                      ),
                                    ),
                                    Text(
                                      '+',
                                      style: TextStyle(
                                          fontFamily: 'NotoSans_NotoSansKR-SemiBold',
                                          fontSize: 18,
                                          letterSpacing: -0.5,
                                          color: colorText,
                                          height: 1.55
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              /** 기기 등록 주의사항 **/
                              Container(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(top: 8.0),
                                      child: Text(
                                        '- 타인 계정에 등록된 기기는 등록하실 수 없습니다.'
                                            '\n- 계정 도용이 의심되거나 본인기기가 맞음에도'
                                            '\n  기기 중복으로 확인되는 경우 고객센터로 신고해 주세요.'
                                            '\n- 타인 계정 사용 시 법적 제재 및 보험 미적용에 의한'
                                            '\n  불이익을 부담하실 수 있습니다.',
                                        style: TextStyle(
                                            fontFamily: 'NotoSans_NotoSansKR-Regular',
                                            fontSize: fontSizeCareful,
                                            letterSpacing: -0.5,
                                            color: colorWarningText,
                                            height: 1.6
                                        ),
                                      ),
                                    ),
                                    /** 기기 등록하기 버튼 **/
                                    Container(
                                      margin: EdgeInsets.only(top: 80.0),
                                      height: 45,
                                      width: MediaQuery.of(context).size.width,
                                      child: ElevatedButton(
                                        child: Text(
                                          '기기 등록하기',
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: fontSizeMainText,
                                            fontFamily: 'NotoSans_NotoSansKR-Regular',
                                            letterSpacing: -0.5,
                                          ),
                                        ),
                                        /** 등록하기 버튼 탭 했을 때 팝업 **/
                                        onPressed: () {
                                          showDialog(
                                              context: context,
                                              barrierDismissible: true, /** 바깥 영역 터치시 닫을지 여부 **/
                                              builder: (BuildContext context){
                                            return AlertDialog(
                                              surfaceTintColor: Colors.grey,
                                              backgroundColor: colorButton,
                                              content: Column(
                                                mainAxisSize: MainAxisSize.min,
                                                children: [
                                                  Container(
                                                    margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                                                    child: Text(
                                                      '기기를 등록하시겠습니까?',
                                                      style: TextStyle(
                                                        fontFamily: 'NotoSans_Bold',
                                                        fontSize: fontSizeSuper,
                                                        letterSpacing: -0.5,
                                                        color: colorTextBright,
                                                      ),
                                                    ),
                                                  ),
                                                  Text(
                                                    textAlign: TextAlign.center,
                                                    '현재 접속 중인 SM-F721N기기를'
                                                        '\n라이더님 계정에 등록합니다.',
                                                    style: TextStyle(
                                                      fontFamily: 'NotoSans_NotoSansKR-Light',
                                                      fontSize: fontSizeMainText,
                                                      letterSpacing: -0.5,
                                                        color: colorTextBright,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              /** 취소, 확인 버튼 **/
                                              actions: [
                                                TextButton(
                                                  child: const Text(
                                                    '취소',
                                                    style: TextStyle(
                                                      fontFamily: 'NotoSans_NotoSansKR-Light',
                                                      fontSize: fontSizeMainText,
                                                      letterSpacing: -0.5,
                                                      color: colorTextBright,
                                                    ),
                                                  ),
                                                  onPressed: () {
                                                    Navigator.of(context).pop();
                                                  },
                                                ),
                                                TextButton(
                                                  child: const Text(
                                                    '확인',
                                                    style: TextStyle(
                                                      fontFamily: 'NotoSans_NotoSansKR-Light',
                                                      fontSize: fontSizeMainText,
                                                      letterSpacing: -0.5,
                                                      color: colorTextBright,
                                                    ),
                                                  ),
                                                  onPressed: () {
                                                    Navigator.push(context,
                                                      MaterialPageRoute(builder: (context) => PageAccountInfo(joinRequest: widget.joinRequest,)));
                                                  },
                                                ),
                                              ],
                                            );
                                          }
                                          );
                                        },
                                        style: ElevatedButton.styleFrom(
                                            primary: colorButton,
                                            onPrimary: Colors.grey,
                                            shape: RoundedRectangleBorder(
                                                borderRadius: BorderRadius.circular(6)
                                            ),
                                            padding: EdgeInsets.zero,
                                            elevation: 0
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        )
    );
  }
}
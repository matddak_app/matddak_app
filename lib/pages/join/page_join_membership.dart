import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:matddak_v1/config/config_form_validator.dart';
import 'package:matddak_v1/functions/util_dialog.dart';
import 'package:matddak_v1/input_form_hyphen/id_num.dart';
import 'package:matddak_v1/input_form_hyphen/phone_number.dart';
import 'package:matddak_v1/model/rider/join_request.dart';
import 'package:matddak_v1/pages/join/page_accont_info.dart';
import 'package:matddak_v1/pages/join/page_device.dart';


import '../../config/config_color.dart';
import '../../config/config_text_size.dart';

class PageJoinMembership extends StatefulWidget {
  const PageJoinMembership({super.key});

  @override
  State<PageJoinMembership> createState() => _PageJoinMembershipState();
}

class _PageJoinMembershipState extends State<PageJoinMembership> {

  final _formKey = GlobalKey<FormBuilderState>();
  /** 이미지피커 전역 변수 선언 **/
  // final ImagePicker _picker = ImagePicker();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /** 키보드가 올라왔을 때 입력 창 키보드에 가려지지 않게 함 **/
      resizeToAvoidBottomInset: true,
      body: SingleChildScrollView(
        /** 전체 감싸는 컨테이너 **/
        child: Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.fromLTRB(25, 70, 25, 25),
          color: colorBg,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              /** 로고 **/
              Container(
                width: 60,
                height: 60,
                child: Image.asset('assets/logo.png'),
              ),
              /** 회원가입 타이틀 텍스트 **/
              Container(
                margin: EdgeInsets.only(top: 10.0),
                height: 31,
                child: Row(
                  children: [
                    Text(
                        '맛있는',
                        style: TextStyle(
                            fontSize: fontSizeTitle,
                            fontFamily: 'NotoSans_NotoSansKR-Light',
                            letterSpacing:-0.5,
                            color: colorPoint
                        )
                    ),
                    Container(
                        child: Text(
                            ' 맛,',
                            style: TextStyle(
                                fontSize: fontSizeTitle,
                                fontFamily: 'NotoSans_Bold',
                                letterSpacing:-0.5,
                                color: colorPoint
                            )
                        )
                    )
                  ],
                ),
              ),
              Container(
                child: Row(
                  children: [
                    Container(
                      child: Text(
                        '딱 드리러',
                        style: TextStyle(
                            fontSize: fontSizeTitle,
                            fontFamily: 'NotoSans_Bold',
                            letterSpacing:-0.5,
                            color: colorPoint
                        ),
                      ),

                    ),
                    Container(
                      child: Text(
                        ' 가볼까요?',
                        style: TextStyle(
                            fontSize: fontSizeTitle,
                            fontFamily: 'NotoSans_NotoSansKR-Light',
                            letterSpacing:-0.5,
                            color: colorPoint
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Container(
                color: colorBg,
                child: FormBuilder (
                    key: _formKey,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        /** 이름 입력 폼 전체 **/
                        Container(
                          margin: EdgeInsets.only(top: 60.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                '이름 입력',
                                style: TextStyle(
                                    fontSize: fontSizeSuper,
                                    fontFamily: 'NotoSans_Bold',
                                    color: colorText,
                                    letterSpacing: -0.5
                                ),
                              ),
                              FormBuilderTextField(
                                name: 'name',
                                // @TODO 테스트 값
                                initialValue: '홍길동',
                                validator: FormBuilderValidators.compose([
                                  FormBuilderValidators.required(errorText: formErrorRequired), /** 필수 **/
                                ]),
                                /** 커서 색상 **/
                                cursorColor: colorPoint,
                                /** 키보드 바깥 영역 탭할 시 키보드 닫기 **/
                                onTapOutside: (event) => FocusManager.instance.primaryFocus?.unfocus(),
                                keyboardType: TextInputType.text, /** 키보드 기본 텍스트 키 올라오게 하기 **/
                                decoration:InputDecoration(
                                  enabledBorder: UnderlineInputBorder( /** 기본 텍스트 필드 인풋 라인 색상 **/
                                    borderSide: BorderSide(color: colorButton),
                                  ),
                                  focusedBorder: UnderlineInputBorder( /** 선택됐을 시 텍스트 필드 인풋 라인 색상 **/
                                    borderSide: BorderSide(color: Colors.orangeAccent),
                                  ),
                                  hintText: '* 이름을 입력해 주세요.',
                                  hintStyle: TextStyle(
                                      fontSize: fontSizeMainText,
                                      fontFamily:'NotoSans_NotoSansKR-Regular',
                                      color: colorHintText,
                                      letterSpacing: -0.5
                                  ),
                                ),
                                style: TextStyle(
                                    fontSize: fontSizeMainText,
                                    fontFamily:'NotoSans_NotoSansKR-Regular',
                                    color: colorHintText,
                                    letterSpacing: -0.5
                                ),
                              ),
                            ],
                          ),
                        ),
                        /** 아이디 입력 폼 전체 **/
                        Container(
                          margin: EdgeInsets.only(top: 40.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                '아이디 입력',
                                style: TextStyle(
                                    fontSize: fontSizeSuper,
                                    fontFamily: 'NotoSans_Bold',
                                    color: colorText,
                                    letterSpacing: -0.5
                                ),
                              ),
                              FormBuilderTextField(
                                name: 'email',
                                // @TODO 테스트 값
                                initialValue: 'hgd123@naver.com',
                                validator: FormBuilderValidators.compose([
                                  FormBuilderValidators.required(errorText: formErrorRequired),
                                  FormBuilderValidators.email(),/** 필수 **/
                                ]),
                                /** 커서 색상 **/
                                cursorColor: colorPoint,
                                /** 키보드 바깥 영역 탭할 시 키보드 닫기 **/
                                onTapOutside: (event) => FocusManager.instance.primaryFocus?.unfocus(),
                                inputFormatters: <TextInputFormatter>[
                                  // FilteringTextInputFormatter.allow(RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")), /** 이메일 형식 **/
                                ],
                                keyboardType: TextInputType.text, /** 키보드 기본 텍스트 키 올라오게 하기 **/
                                decoration:InputDecoration(
                                  enabledBorder: UnderlineInputBorder( /** 기본 텍스트 필드 인풋 라인 색상 **/
                                    borderSide: BorderSide(color: colorButton),
                                  ),
                                  focusedBorder: UnderlineInputBorder( /** 선택됐을 시 텍스트 필드 인풋 라인 색상 **/
                                    borderSide: BorderSide(color: Colors.orangeAccent),
                                  ),
                                  hintText: '* 이메일 형식으로 입력해 주세요.',
                                  hintStyle: TextStyle(
                                      fontSize: fontSizeMainText,
                                      fontFamily:'NotoSans_NotoSansKR-Regular',
                                      color: colorHintText,
                                      letterSpacing: -0.5
                                  ),
                                ),
                                style: TextStyle(
                                    fontSize: fontSizeMainText,
                                    fontFamily:'NotoSans_NotoSansKR-Regular',
                                    color: colorHintText,
                                    letterSpacing: -0.5
                                ),
                              ),
                            ],
                          ),
                        ),
                        /** 비밀번호 입력 폼 전체 **/
                        Container(
                          margin: EdgeInsets.only(top: 40.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                '비밀번호 입력',
                                style: TextStyle(
                                    fontSize: fontSizeSuper,
                                    fontFamily: 'NotoSans_Bold',
                                    color: colorText,
                                    letterSpacing: -0.5
                                ),
                              ),
                              FormBuilderTextField(
                                name: 'password',
                                // @TODO 테스트 값
                                initialValue: '12345678',
                                validator: FormBuilderValidators.compose([
                                  FormBuilderValidators.required(errorText: formErrorRequired), /** 필수 **/
                                  FormBuilderValidators.minLength(8, errorText: formErrorMinLength(8)), /** 8자 이상 **/
                                ]),
                                obscureText: true, /** 비밀번호 가리기 **/
                                /** 커서 색상 **/
                                cursorColor: colorPoint,
                                /** 키보드 바깥 영역 탭할 시 키보드 닫기 **/
                                onTapOutside: (event) => FocusManager.instance.primaryFocus?.unfocus(),
                                inputFormatters: <TextInputFormatter>[
                                  FilteringTextInputFormatter.allow(RegExp(r'[a-z|A-Z|0-9|ᆢ]')), /** 영문, 숫자만 입력 가능하게 하기 **/
                                ],
                                keyboardType: TextInputType.text, /** 키보드 기본 텍스트 키 올라오게 하기 **/
                                decoration:InputDecoration(
                                  enabledBorder: UnderlineInputBorder( /** 기본 텍스트 필드 인풋 라인 색상 **/
                                    borderSide: BorderSide(color: colorButton),
                                  ),
                                  focusedBorder: UnderlineInputBorder( /** 선택됐을 시 텍스트 필드 인풋 라인 색상 **/
                                    borderSide: BorderSide(color: Colors.orangeAccent),
                                  ),
                                  hintText: '* 8자 이상 입력해 주세요.',
                                  hintStyle: TextStyle(
                                      fontSize: fontSizeMainText,
                                      fontFamily:'NotoSans_NotoSansKR-Regular',
                                      color: colorHintText,
                                      letterSpacing: -0.5
                                  ),
                                ),
                                style: TextStyle(
                                    fontSize: fontSizeMainText,
                                    fontFamily:'NotoSans_NotoSansKR-Regular',
                                    color: colorHintText,
                                    letterSpacing: -0.5
                                ),
                              ),
                            ],
                          ),
                        ),
                        /** 비밀번호 재입력 폼 전체 **/
                        Container(
                          margin: EdgeInsets.only(top: 40.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                '비밀번호 재입력',
                                style: TextStyle(
                                    fontSize: fontSizeSuper,
                                    fontFamily: 'NotoSans_Bold',
                                    color: colorText,
                                    letterSpacing: -0.5
                                ),
                              ),
                              FormBuilderTextField(
                                name: 'passwordRe',
                                // @TODO 테스트 값
                                initialValue: '12345678',
                                validator: FormBuilderValidators.compose([
                                  FormBuilderValidators.required(errorText: formErrorRequired), /** 필수 **/
                                  FormBuilderValidators.minLength(8, errorText: formErrorMinLength(8)), /** 8자 이상 **/
                                ]),
                                obscureText: true, /** 비밀번호 가리기 **/
                                /** 커서 색상 **/
                                cursorColor: colorPoint,
                                /** 키보드 바깥 영역 탭할 시 키보드 닫기 **/
                                onTapOutside: (event) => FocusManager.instance.primaryFocus?.unfocus(),
                                inputFormatters: <TextInputFormatter>[
                                  FilteringTextInputFormatter.allow(RegExp(r'[a-z|A-Z|0-9|ᆢ]')), /** 영문, 숫자만 입력 가능하게 하기 **/
                                ],
                                keyboardType: TextInputType.text, /** 키보드 기본 텍스트 키 올라오게 하기 **/
                                decoration:InputDecoration(
                                  enabledBorder: UnderlineInputBorder( /** 기본 텍스트 필드 인풋 라인 색상 **/
                                    borderSide: BorderSide(color: colorButton),
                                  ),
                                  focusedBorder: UnderlineInputBorder( /** 선택됐을 시 텍스트 필드 인풋 라인 색상 **/
                                    borderSide: BorderSide(color: Colors.orangeAccent),
                                  ),
                                  hintText: '* 비밀번호를 다시한번 입력해 주세요.',
                                  hintStyle: TextStyle(
                                      fontSize: fontSizeMainText,
                                      fontFamily:'NotoSans_NotoSansKR-Regular',
                                      color: colorHintText,
                                      letterSpacing: -0.5
                                  ),
                                ),
                                style: TextStyle(
                                    fontSize: fontSizeMainText,
                                    fontFamily:'NotoSans_NotoSansKR-Regular',
                                    color: colorHintText,
                                    letterSpacing: -0.5
                                ),
                              ),
                            ],
                          ),
                        ),
                        /** 휴대폰 번호 입력 폼 전체 **/
                        Container(
                          margin: EdgeInsets.only(top: 40.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                '휴대폰 번호 입력',
                                style: TextStyle(
                                    fontSize: fontSizeSuper,
                                    fontFamily: 'NotoSans_Bold',
                                    color: colorText,
                                    letterSpacing: -0.5
                                ),
                              ),
                              FormBuilderTextField(
                                name: 'phoneNumber',
                                // @TODO 테스트 값
                                initialValue: '010-1234-5678',
                                validator: FormBuilderValidators.compose([
                                  FormBuilderValidators.required(errorText: formErrorRequired), /** 필수 **/
                                  FormBuilderValidators.minLength(12, errorText: formErrorMinLength(12)), /** 12자 이상 **/
                                ]),
                                /** 커서 색상 **/
                                cursorColor: colorPoint,
                                /** 키보드 바깥 영역 탭할 시 키보드 닫기 **/
                                onTapOutside: (event) => FocusManager.instance.primaryFocus?.unfocus(),
                                keyboardType: TextInputType.number, /** 키보드 기본 숫자키 올라오게 하기 **/
                                inputFormatters: [
                                  FilteringTextInputFormatter.digitsOnly, /** 숫자만 입력 받고 싶을 때 **/
                                  NumberFormatter(), /** 자동 하이픈 010-0000-0000 **/
                                  LengthLimitingTextInputFormatter(13) /** 13자리만 입력 받도록 하이픈 2개+숫자 11개 **/
                                ],
                                decoration:InputDecoration(
                                  enabledBorder: UnderlineInputBorder( /** 기본 텍스트 필드 인풋 라인 색상 **/
                                    borderSide: BorderSide(color: colorButton),
                                  ),
                                  focusedBorder: UnderlineInputBorder( /** 선택됐을 시 텍스트 필드 인풋 라인 색상 **/
                                    borderSide: BorderSide(color: Colors.orangeAccent),
                                  ),
                                  hintText: '* 휴대폰 번호를 입력해 주세요.',
                                  hintStyle: TextStyle(
                                      fontSize: fontSizeMainText,
                                      fontFamily:'NotoSans_NotoSansKR-Regular',
                                      color: colorHintText,
                                      letterSpacing: -0.5
                                  ),
                                ),
                                style: TextStyle(
                                    fontSize: fontSizeMainText,
                                    fontFamily:'NotoSans_NotoSansKR-Regular',
                                    color: colorHintText,
                                    letterSpacing: -0.5
                                ),
                              ),
                            ],
                          ),
                        ),
                        /** 주민등록번호 입력 폼 전체 **/
                        Container(
                          margin: EdgeInsets.only(top: 40.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                '주민등록번호 입력',
                                style: TextStyle(
                                    fontSize: fontSizeSuper,
                                    fontFamily: 'NotoSans_Bold',
                                    color: colorText,
                                    letterSpacing: -0.5
                                ),
                              ),
                              FormBuilderTextField(
                                name: 'idNum',
                                // @TODO 테스트 값
                                initialValue: '850101-1234567',
                                validator: FormBuilderValidators.compose([
                                  FormBuilderValidators.required(errorText: formErrorRequired), /** 필수 **/
                                  FormBuilderValidators.minLength(14, errorText: formErrorMinLength(14)), /** 8자 이상 **/
                                ]),
                                /** 커서 색상 **/
                                cursorColor: colorPoint,
                                /** 키보드 바깥 영역 탭할 시 키보드 닫기 **/
                                onTapOutside: (event) => FocusManager.instance.primaryFocus?.unfocus(),
                                keyboardType: TextInputType.number, /** 키보드 기본 숫자키 올라오게 하기 **/
                                inputFormatters: [
                                  FilteringTextInputFormatter.digitsOnly, /** 숫자만 입력 받고 싶을 때 **/
                                  IdNumFormatter(), /** 자동 하이픈 000000-0000000 **/
                                  LengthLimitingTextInputFormatter(14) /** 14자리만 입력 받도록 하이픈 1개+숫자 13개 **/
                                ],
                                decoration:InputDecoration(
                                  enabledBorder: UnderlineInputBorder( /** 기본 텍스트 필드 인풋 라인 색상 **/
                                    borderSide: BorderSide(color: colorButton),
                                  ),
                                  focusedBorder: UnderlineInputBorder( /** 선택됐을 시 텍스트 필드 인풋 라인 색상 **/
                                    borderSide: BorderSide(color: Colors.orangeAccent),
                                  ),
                                  hintText: '* 주민등록번호를 입력해 주세요.',
                                  hintStyle: TextStyle(
                                      fontSize: fontSizeMainText,
                                      fontFamily:'NotoSans_NotoSansKR-Regular',
                                      color: colorHintText,
                                      letterSpacing: -0.5
                                  ),
                                ),
                                style: TextStyle(
                                    fontSize: fontSizeMainText,
                                    fontFamily:'NotoSans_NotoSansKR-Regular',
                                    color: colorHintText,
                                    letterSpacing: -0.5
                                ),
                              ),
                              /** 주민등록번호 주의사항 **/
                              Container(
                                  margin: EdgeInsets.only(top: 8.0),
                                  child: Container(
                                    width: MediaQuery.of(context).size.width / 1.8,
                                    child: Text(
                                      '* 만 15세 이상, 만 18세 미만 청소년은'
                                          '\n   법적 보호자 동의가 필요합니다.',
                                      style: TextStyle(
                                          fontFamily: 'NotoSans_NotoSansKR-Regular',
                                          fontSize: fontSizeCareful,
                                          letterSpacing: -0.5,
                                          color: colorWarningText,
                                          height: 1.6
                                      ),
                                    ),
                                  )
                              ),
                            ],
                          ),
                        ),
                        /** 계속 버튼 **/
                        Container(
                          margin: EdgeInsets.only(top: 40.0),
                          height: 45,
                          width: MediaQuery.of(context).size.width,
                          child: ElevatedButton(
                            child: Text(
                              '계속',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: fontSizeMainText,
                                fontFamily: 'NotoSans_NotoSansKR-Regular',
                                letterSpacing: -0.5,
                              ),
                            ),
                            style: ElevatedButton.styleFrom(
                                primary: colorButton,
                                onPrimary: Colors.grey,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(6)
                                ),
                                padding: EdgeInsets.zero,
                                elevation: 0
                            ),
                            onPressed: () {
                              /** 법적 보호자 동의 파일 업로드 바텀 시트 **/
                              showModalBottomSheet<void>(
                                context: context,
                                builder: (BuildContext context) {
                                  return Container(
                                    padding: EdgeInsets.fromLTRB(20, 30, 20, 20),
                                    width: MediaQuery.of(context).size.width,
                                    height: 236,
                                    decoration: const BoxDecoration(
                                        color: colorButton,
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(20),
                                            topRight: Radius.circular(20)
                                        )
                                    ),
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        /** 법적 보호자 동의 파일 업로드 타이틀 **/
                                        Container(
                                          alignment: AlignmentDirectional.bottomStart,
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              const Text(
                                                '미성년자인 경우 보호자 동의가 필요합니다.',
                                                style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: fontSizeSuper,
                                                  fontFamily: 'NotoSans_Bold',
                                                  letterSpacing: -0.5,
                                                ),
                                              ),
                                              /** 텍스트 버튼 (파일업로드 폼) **/
                                              Container(
                                                margin: EdgeInsets.only(top: 8.0),
                                                child: Column(
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  children: [
                                                    Text(
                                                      'matddak@outlook.com으로 동의서를 보내주세요',
                                                      style: TextStyle(
                                                          fontSize: fontSizeMainText,
                                                          fontFamily:'NotoSans_NotoSansKR-Regular',
                                                          color: Colors.white,
                                                          letterSpacing: -0.5
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        /** 등록하기 버튼 **/
                                        Container(
                                            width: MediaQuery.of(context).size.width,
                                            height: 45,
                                            child: ElevatedButton(
                                                child: const Text(
                                                  '확인',
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: fontSizeMainText,
                                                    fontFamily: 'NotoSans_NotoSansKR-Regular',
                                                    letterSpacing: -0.5,
                                                  ),
                                                ),
                                                style: ElevatedButton.styleFrom(
                                                  primary: colorWarningText,
                                                  onPrimary: Colors.grey,
                                                  shape: RoundedRectangleBorder(
                                                      borderRadius: BorderRadius.circular(6)
                                                  ),
                                                  padding: EdgeInsets.zero,
                                                  elevation: 0,
                                                ),
                                                onPressed: () {
                                                  /** 등록하기 버튼 탭 했을 때 팝업 **/
                                                  showDialog(
                                                      context: context,
                                                      barrierDismissible: true, /** 바깥 영역 터치시 닫을지 여부 **/
                                                      builder: (BuildContext context){
                                                        return AlertDialog(
                                                          surfaceTintColor: Colors.grey,
                                                          backgroundColor: colorButton,
                                                          content: Column(
                                                            mainAxisSize: MainAxisSize.min,
                                                            children: [
                                                              Container(
                                                                margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                                                                child: Text(
                                                                  '등록 소요 시간을 확인해 주세요.',
                                                                  style: TextStyle(
                                                                    fontFamily: 'NotoSans_Bold',
                                                                    fontSize: fontSizeSuper,
                                                                    letterSpacing: -0.5,
                                                                    color: colorTextBright,
                                                                  ),
                                                                ),
                                                              ),
                                                              Text(
                                                                textAlign: TextAlign.center,
                                                                '동의서 확인까지 1~2일 정도'
                                                                    '\n소요될 예정입니다.',
                                                                style: TextStyle(
                                                                  fontFamily: 'NotoSans_NotoSansKR-Light',
                                                                  fontSize: fontSizeMainText,
                                                                  letterSpacing: -0.5,
                                                                  color: colorTextBright,
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                          /** 취소, 확인 버튼 **/
                                                          actions: [
                                                            TextButton(
                                                              child: const Text(
                                                                '취소',
                                                                style: TextStyle(
                                                                  fontFamily: 'NotoSans_NotoSansKR-Light',
                                                                  fontSize: fontSizeMainText,
                                                                  letterSpacing: -0.5,
                                                                  color: colorTextBright,
                                                                ),
                                                              ),
                                                              style: TextButton.styleFrom(
                                                                foregroundColor: Colors.white,
                                                              ),
                                                              onPressed: () {
                                                                Navigator.of(context).pop();
                                                              },
                                                            ),
                                                            TextButton(
                                                              child: const Text(
                                                                '확인',
                                                                style: TextStyle(
                                                                  fontFamily: 'NotoSans_NotoSansKR-Light',
                                                                  fontSize: fontSizeMainText,
                                                                  letterSpacing: -0.5,
                                                                  color: colorTextBright,
                                                                ),
                                                              ),
                                                              style: TextButton.styleFrom(
                                                                foregroundColor: Colors.white,
                                                              ),
                                                              onPressed: () {
                                                                if(_formKey.currentState!.saveAndValidate()) {

                                                                  JoinRequest joinRequest = JoinRequest();

                                                                  joinRequest.step1 = Step1Request(
                                                                      _formKey.currentState!.fields['name']!.value,
                                                                      _formKey.currentState!.fields['idNum']!.value,
                                                                      _formKey.currentState!.fields['email']!.value,
                                                                      _formKey.currentState!.fields['password']!.value,
                                                                      _formKey.currentState!.fields['passwordRe']!.value,
                                                                      _formKey.currentState!.fields['phoneNumber']!.value
                                                                  );

                                                                  Navigator.push(
                                                                      context, MaterialPageRoute(builder: (context) => PageAccountInfo(joinRequest: joinRequest)));

                                                                } else {
                                                                  showAlertDialog(context,
                                                                      title: "필수 입력 정보가 입력되지 않았습니다.",
                                                                      content: "입력 정보를 다시 한번 확인해 주세요.",
                                                                      actions: <Widget>[
                                                                        TextButton(onPressed: ()=>Navigator.pop(context),
                                                                            child: const Text(
                                                                                "확인",
                                                                              style: TextStyle(
                                                                                fontFamily: 'NotoSans_NotoSansKR-Light',
                                                                                fontSize: fontSizeMainText,
                                                                                letterSpacing: -0.5,
                                                                                color: colorTextBright,
                                                                              ),
                                                                            ))
                                                                      ]);
                                                                }
                                                              },
                                                            ),
                                                          ],
                                                        );
                                                      }
                                                  );
                                                }
                                            )
                                        )
                                      ],
                                    ),
                                  );
                                },
                              );
                            },
                          ),
                        ),

                      ],

                    )


                ),
              )




            ],
          ),
        ),
      ),
    );
  }
}
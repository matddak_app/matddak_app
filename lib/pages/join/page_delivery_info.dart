import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:matddak_v1/model/rider/join_request.dart';
import 'package:matddak_v1/repository/repo_rider.dart';
import '../../config/config_color.dart';
import '../../config/config_text_size.dart';
import '../../functions/util_dialog.dart';

class PageDeliveryInfo extends StatefulWidget {
  const PageDeliveryInfo({
    super.key,
    required this.joinRequest
  });

  final JoinRequest joinRequest;


  @override
  State<PageDeliveryInfo> createState() => _PageDeliveryInfoState();
}


class _PageDeliveryInfoState extends State<PageDeliveryInfo> {

  final _formKey = GlobalKey<FormBuilderState>();


  Future<void> _setRider(JoinRequest joinRequest) async {

    await RepoRider().setRider(joinRequest)
    .then((res)=>{

      showAlertDialog(context,
          title: "가입이 완료되었습니다. ",
          content: "다시 로그인해 주세요.",
          actions: <Widget>[
            TextButton(
              child: const Text("확인"),
              onPressed: (){
                Navigator.of(context).pushNamed('/page_safety_check');
              },
            )
          ])

    }).catchError((err) {

      debugPrint(err);

      showAlertDialog(
        context,
        title: "일시적인 오류로 회원가입에 실패했습니다.",
        content: "잠시 후 다시 시도해 주세요.",
        actions: <Widget>[
        TextButton(onPressed: ()=>Navigator.pop(context), child: const Text("확인"))
        ]);

    });

    await RepoRider().setRider(joinRequest)
        .then((res) => {
        '/page_safety_check'
    })

        .catchError((err){
          debugPrint(err);


          showAlertDialog(context,
              title: "일시적인 오류로 회원가입에 실패했습니다.",
              content: "잠시 후 다시 시도해 주세요.",
              actions: <Widget>[
                TextButton(onPressed: ()=>Navigator.pop(context), child: const Text("확인"))
              ]);

    });
  }

  @override
  void initState() {
    super.initState();

  }


  // /** 배달 지역 드롭 메뉴 전역 변수 선언 **/
  // List<String> _dropdownList=['* 희망하는 배달 지역을 선택해 주세요','단원구', '상록구'];


  List <Map<String,String>> _dropdownList = [
    { 'text': '* 희망하는 배달 지역을 선택해 주세요', 'value' : '' },
    { 'text': '단원구', 'value' :   'DANWON' },
    { 'text': '상록구', 'value' :   'SANGROCK' }
  ];
  //기본값 설정
  String? _selectedDropdown = '';





  // /** 배달 수단 드롭 메뉴 전역 변수 선언 **/
  // List<String> _kindList=['* 희망하는 배달 수단을 선택해 주세요','차', '오토바이', '전기 자전거', '자전거' ,'전동 킥보드', '도보'];
    List <Map<String,String>> _kindList = [
    { 'text': '* 희망하는 배달 수단을 선택해 주세요', 'value' : '' },
    { 'text': '자동차', 'value' :   'CARRY' },
    { 'text': '오토바이', 'value' :   'MOTORCYCLE' },
    { 'text': '도보', 'value' :   'WALKING' },
    { 'text': '전기 자전거', 'value' :   'ELECTRIC_BICYCLE' },
    { 'text': '자전거', 'value' :   'CYCLE' },
    { 'text': '전동 킥보드', 'value' :   'ELECTRIC_SCOOTER' },
    { 'text': '단원구', 'value' :   'DANWON' },
  ];
  //기본값 설정
  String? _selectedKindDropdown = '';




  /** 체크 박스 전역 변수 선언 **/
  bool _isPersonalInfo = false;
  bool _isMobility = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        /** 전체를 감싸는 컨테이너 **/
        child: Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.fromLTRB(25, 70, 25, 25),
          color: Color(0xff1A1A1A),
          child: Column(
            children: [
              /** 1, 2, 3 단계 표시 **/
              Container(
                child: Row(
                  children: [
                    /** 1단계 **/
                    Container(
                        padding: EdgeInsets.only(bottom: 2.0),
                        width: 20,
                        height: 20,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: colorPoint,
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              '1',
                              style: TextStyle(
                                fontSize: fontSizeMicro,
                                fontFamily: 'NotoSans_Bold',
                                color: Colors.white,
                              ),
                            ),
                          ],
                        )
                    ),
                    /** 단계 사이 선 **/
                    Container(
                      height: 1,
                      width: 25,
                      color: colorPoint,
                    ),
                    /** 2단계 **/
                    Container(
                      padding: EdgeInsets.only(bottom: 2.0),
                      width: 20,
                      height: 20,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: colorPoint,
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            '2',
                            style: TextStyle(
                              fontSize: fontSizeMicro,
                              fontFamily: 'NotoSans_NotoSansKR-Regular',
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                    ),
                    /** 단계 사이 선 **/
                    Container(
                      height: 1,
                      width: 25,
                      color: colorButton,
                    ),
                    /** 3단계 **/
                    Container(
                      padding: EdgeInsets.only(bottom: 2.0),
                      width: 20,
                      height: 20,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: colorButton,
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            '3',
                            style: TextStyle(
                              fontSize: fontSizeMicro,
                              fontFamily: 'NotoSans_NotoSansKR-Regular',
                              color: Color.fromRGBO(33, 33, 33, 1),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              /** 배달정보 타이틀 텍스트 **/
              Container(
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: 20.0),
                      child: Row(
                        children: [
                          Container(
                            child: Text(
                              '배달정보',
                              style: TextStyle(
                                  fontSize: fontSizeTitle,
                                  fontFamily: 'NotoSans_Bold',
                                  letterSpacing:-0.5,
                                  color: colorPoint
                              ),
                            ),
                          ),
                          Container(
                            child: Text(
                              '를',
                              style: TextStyle(
                                  fontSize: fontSizeTitle,
                                  fontFamily: 'NotoSans_NotoSansKR-Light',
                                  letterSpacing:-0.5,
                                  color: colorPoint
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                        child: Row(
                          children: [
                            Text(
                              '입력해 주세요',
                              style: TextStyle(
                                  fontSize: fontSizeTitle,
                                  fontFamily: 'NotoSans_NotoSansKR-Light',
                                  letterSpacing:-0.5,
                                  color: colorPoint
                              ),
                            ),
                          ],
                        )
                    ),
                    /** 배달 정보 입력 폼 전체 **/
                    FormBuilder(
                      key: _formKey,
                      child: Column(
                        children: [
                          /** 배달 희망 지역 타이틀 **/
                          Container(
                            margin: EdgeInsets.only(top: 60.0),
                            child: Row(
                              children: [
                                Text(
                                  '배달 희망 지역',
                                  style: TextStyle(
                                      fontSize: fontSizeSuper,
                                      fontFamily: 'NotoSans_Bold',
                                      color: colorText,
                                      letterSpacing: -0.5
                                  ),
                                ),
                              ],
                            ),
                          ),
                          /** 배달 지역 드롭 메뉴 **/
                          Container(
                            width: MediaQuery.of(context).size.width,
                            child: DropdownButton(
                              isExpanded: true,
                              style: TextStyle(
                                  fontSize: fontSizeMainText,
                                  fontFamily:'NotoSans_NotoSansKR-Regular',
                                  color: colorHintText,
                                  letterSpacing: -0.5
                              ),
                              value: _selectedDropdown,
                              items: _dropdownList.map((Map<String,dynamic> item) {
                                return DropdownMenuItem<String>(
                                  child: Text(item['text']),
                                  value: item['value'],
                                );
                              }).toList(),
                              onChanged: (dynamic value) {
                                setState(() {
                                  _selectedDropdown = value!;
                                });
                              },
                            )
                          ),
                          /** 배달 수단 선택 타이틀 **/
                          Container(
                              margin: EdgeInsets.only(top: 40.0),
                            child: Row(
                              children: [
                                Text(
                                  '배달 수단 선택',
                                  style: TextStyle(
                                      fontSize: fontSizeSuper,
                                      fontFamily: 'NotoSans_Bold',
                                      color: colorText,
                                      letterSpacing: -0.5
                                  ),
                                ),
                              ],
                            )
                          ),
                          /** 배달 수단 선택 **/
                          Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                    width: MediaQuery.of(context).size.width,
                                    child: DropdownButton(
                                      isExpanded: true,
                                      style: TextStyle(
                                          fontSize: fontSizeMainText,
                                          fontFamily:'NotoSans_NotoSansKR-Regular',
                                          color: colorHintText,
                                          letterSpacing: -0.5
                                      ),
                                      value: _selectedKindDropdown,
                                      items: _kindList.map((Map<String,dynamic> item) {
                                        return DropdownMenuItem<String>(
                                          child: Text(item['text']),
                                          value: item['value'],
                                        );
                                      }).toList(),
                                      onChanged: (dynamic value) {
                                        setState(() {
                                          _selectedKindDropdown = value!;
                                        });
                                      },
                                    )
                                ),
                                /** 배달 수단 선택 주의사항 **/
                                Container(
                                  child: Text('- 배달 업무는 등록된 배달 수단으로만 진행하여야 하고,'
                                      '\n  배달 수단을 변경할 경우 반드시 본 변경 절차를 거쳐야 합니다.'
                                      '\n  등록된 배달 수단과 실제 배달 수단 불일치로 인한 책임은'
                                      '\n  라이더님께 있습니다.',
                                    style: TextStyle(
                                        fontFamily: 'NotoSans_NotoSansKR-Regular',
                                        fontSize: fontSizeCareful,
                                        letterSpacing: -0.5,
                                        color: colorWarningText,
                                        height: 1.6
                                    ),
                                  )
                                ),
                                /** 배달 수단 정보 타이틀 **/
                                Container(
                                  margin: EdgeInsets.only(top: 40.0),
                                  child: Text(
                                    '배달 수단 정보',
                                    style: TextStyle(
                                        fontSize: fontSizeSuper,
                                        fontFamily: 'NotoSans_Bold',
                                        color: colorText,
                                        letterSpacing: -0.5
                                    ),
                                  )
                                ),
                                /** 차종 및 번호판 입력 텍스트 필드 **/
                                Container(
                                  margin: EdgeInsets.only(top: 10.0),
                                  padding: EdgeInsets.fromLTRB(15, 5, 15, 0),
                                  height: 50,
                                  decoration:BoxDecoration(
                                      color: colorJoin,
                                      borderRadius: BorderRadius.circular(6.0)
                                  ),
                                  child: FormBuilderTextField(
                                    name: 'driveNumber',
                                    // @Todo 테스트 값
                                    initialValue: '128나7855',


                                    /** 키보드 바깥 영역 탭할 시 키보드 닫기 **/
                                    onTapOutside: (event) => FocusManager.instance.primaryFocus?.unfocus(),
                                    keyboardType: TextInputType.text, /** 키보드 기본 텍스트 키 올라오게 하기 **/
                                    decoration:InputDecoration(
                                      isDense: true, /** 텍스트 필드 패딩 없애기 **/
                                      enabledBorder: UnderlineInputBorder(
                                        borderSide: BorderSide.none, /** 기본 텍스트 필드 인풋 라인 없음 **/
                                      ),
                                      focusedBorder: UnderlineInputBorder(
                                        borderSide: BorderSide.none, /** 선택됐을 시 텍스트 필드 인풋 라인 없음 **/
                                      ),
                                      hintText: '상세 배달 수단 정보 및 번호판',
                                      hintStyle: TextStyle(
                                          fontSize: fontSizeMainText,
                                          fontFamily:'NotoSans_NotoSansKR-Regular',
                                          color: colorHintText,
                                          letterSpacing: -0.5
                                      ),
                                    ),
                                    style: TextStyle(
                                        fontSize: fontSizeMainText,
                                        fontFamily:'NotoSans_NotoSansKR-Regular',
                                        color: colorHintText,
                                        letterSpacing: -0.5
                                    ),
                                  ),
                                ),
                                /** 배달 수단 정보 입력 시 주의사항 **/
                                Container(
                                    margin: EdgeInsets.only(top: 8.0),
                                    child: Text('- 오토바이의 경우, 차종 및 번호판을 입력해 주세요.'
                                        '\n  배달 수단 상세 예시: 오토바이 / 번호판 예시: 서울 동대문 가 1234'
                                        '\n- 전기 자전거, 자전거, 도보, 전동 킥보드의 경우 (없음)을 입력해 주세요.',
                                      style: TextStyle(
                                          fontFamily: 'NotoSans_NotoSansKR-Regular',
                                          fontSize: fontSizeCareful,
                                          letterSpacing: -0.5,
                                          color: colorWarningText,
                                          height: 1.6
                                      ),
                                    )
                                ),
                                /** 개인정보 수집 및 이용동의 **/
                                Container(
                                  margin: EdgeInsets.only(top: 40.0),
                                  child: Row(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        margin: EdgeInsets.only(top: 2.0),
                                        child:  Checkbox(
                                          materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                          visualDensity: const VisualDensity(
                                              horizontal: VisualDensity.minimumDensity,
                                              vertical: VisualDensity.minimumDensity
                                          ),
                                          value: _isPersonalInfo,
                                          onChanged: (bool? newValue) {
                                            setState(() {
                                              _isPersonalInfo = newValue!;
                                            });
                                          },
                                          activeColor: colorWarningText,
                                          checkColor: Colors.black54,
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(left: 10),
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              '개인정보 수집 및 이용동의',
                                              style: TextStyle(
                                                  fontSize: 18,
                                                  fontFamily: 'NotoSans_Bold',
                                                  color: colorText,
                                                  letterSpacing: -0.5
                                              ),
                                            ),
                                            Container(
                                              width: MediaQuery.of(context).size.width / 1.4,
                                                child: Text(
                                                  '화물차인 경우, 영업용 번호판(노란색)을 부착한 경우에만 자차 배송이 가능합니다.',
                                                  style: TextStyle(
                                                      fontFamily: 'NotoSans_NotoSansKR-Regular',
                                                      fontSize: 15,
                                                      letterSpacing: -0.5,
                                                      color: colorPoint,
                                                      height: 1.6
                                                  ),
                                                ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                /** 공유 모빌리티 사용책임 동의 **/
                                Container(
                                  margin: EdgeInsets.only(top: 40.0),
                                  child: Row(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        margin: EdgeInsets.only(top: 2.0),
                                        child: Checkbox(
                                          materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                          visualDensity: const VisualDensity(
                                              horizontal: VisualDensity.minimumDensity,
                                              vertical: VisualDensity.minimumDensity
                                          ),
                                          value: _isMobility,
                                          onChanged: (bool? newValue) {
                                            setState(() {
                                              _isMobility = newValue!;
                                            });
                                          },
                                          activeColor: colorWarningText,
                                          checkColor: Colors.black54,
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.fromLTRB(10, 0, 0, 4),
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              '공유 모빌리티 사용 책임 동의',
                                              style: TextStyle(
                                                  fontSize: 18,
                                                  fontFamily: 'NotoSans_Bold',
                                                  color: colorText,
                                                  letterSpacing: -0.5
                                              ),
                                            ),
                                            Container(
                                              width: MediaQuery.of(context).size.width / 1.4,
                                              child: Text(
                                                '배달 목적으로 사용할 수 없는 이동수단 (공유 스쿠터, 렌트카, 공유 자전거 등)의 사용은 금지되며, 사용으로 인한 책임은 전적으로 라이더가 부담합니다.',
                                                style: TextStyle(
                                                    fontFamily: 'NotoSans_NotoSansKR-Regular',
                                                    fontSize: 15,
                                                    letterSpacing: -0.5,
                                                    color: colorPoint,
                                                    height: 1.6
                                                ),
                                              ),
                                            )
                                          ],
                                        )
                                      ),
                                    ],
                                  ),
                                ),
                                /** 계속 버튼 **/
                                Container(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,

                                    children: [
                                      Container(
                                        margin: EdgeInsets.only(top: 80.0),
                                        height: 45,
                                        width: MediaQuery.of(context).size.width / 2.35,
                                        child: ElevatedButton(
                                          child: Text(
                                            '뒤로',
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontSize: fontSizeMainText,
                                              fontFamily: 'NotoSans_NotoSansKR-Regular',
                                              letterSpacing: -0.5,
                                            ),
                                          ),
                                          style: ElevatedButton.styleFrom(
                                              primary: colorButton,
                                              onPrimary: Colors.grey,
                                              shape: RoundedRectangleBorder(
                                                  borderRadius: BorderRadius.circular(6)
                                              ),
                                              padding: EdgeInsets.zero,
                                              elevation: 0
                                          ),
                                          onPressed: () {
                                            Navigator.of(context).pushNamed('/page_account_info');
                                          },
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(top: 80.0),
                                        height: 45,
                                        width: MediaQuery.of(context).size.width / 2.35,
                                        child: ElevatedButton(
                                          child: Text(
                                            '계속',
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontSize: fontSizeMainText,
                                              fontFamily: 'NotoSans_NotoSansKR-Regular',
                                              letterSpacing: -0.5,
                                            ),
                                          ),
                                          style: ElevatedButton.styleFrom(
                                              primary: colorPoint,
                                              onPrimary: colorPoint,
                                              shape: RoundedRectangleBorder(
                                                  borderRadius: BorderRadius.circular(6)
                                              ),
                                              padding: EdgeInsets.zero,
                                              elevation: 0
                                          ),
                                          onPressed: () {
                                            if(_selectedDropdown == '' || _selectedKindDropdown == ''){
                                              showAlertDialog(context,
                                                  title: "배달정보를 확인해 주세요.",
                                                  content: "배달정보가 선택되지 않았습니다.",
                                                  actions: <Widget>[
                                                    TextButton(onPressed: ()=>Navigator.pop(context), child: const Text("확인"))
                                                  ]);
                                              return;
                                            }


                                            if ( _isPersonalInfo && _isMobility ){

                                              JoinRequest joinRequest = widget.joinRequest;

                                              joinRequest.step3 = Step3Request(
                                                _selectedDropdown,
                                                _selectedKindDropdown,
                                                _formKey.currentState!.fields['driveNumber']!.value,
                                              );

                                              _setRider(joinRequest);

                                            } else {
                                              showAlertDialog(context,
                                                  title: "필수 동의가 선택되지 않았습니다",
                                                  content: "공유 모빌리티 사용 책임 동의와 \n개인정보 수집 및 동의를 모두 동의해 주세요.",
                                                  actions: <Widget>[
                                                    TextButton(onPressed: ()=>Navigator.pop(context), child: const Text("확인"))
                                                  ]);
                                            }
                                          },
                                        ),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

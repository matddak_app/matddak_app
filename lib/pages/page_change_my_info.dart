

import 'package:flutter/material.dart';
import 'package:matddak_v1/component/component_appbar.dart';
import 'package:matddak_v1/config/config_color.dart';
import 'package:matddak_v1/config/config_text_size.dart';
import 'package:matddak_v1/model/rider/rider_response.dart';

import 'package:matddak_v1/repository/repo_rider.dart';

class PageChangeMyInfo extends StatefulWidget {
  const PageChangeMyInfo({
    super.key,


  });

  @override
  State<PageChangeMyInfo> createState() => _PageChangeMyInfoState();
}

class _PageChangeMyInfoState extends State<PageChangeMyInfo> {

  RiderResponse? _response;

  Future<void> _loadDetail() async {
    await RepoRider().getRiderInfo()
        .then((res) =>{
      setState(() {
        _response = res.data;
      })
    })
        .catchError((err) => {
      debugPrint(err)
    });
  }

  @override
  void initState(){
    super.initState();
    _loadDetail();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: ComponentAppbar(
          text: '내 정보 변경',
          page: '/page_rider_main',
        ),
        body: _buildBody(context)
    );
  }

  Widget _buildBody(BuildContext context) {
    // double phoneWidth = MediaQuery.of(context).size.width;
    // double phoneHeight = MediaQuery.of(context).size.height;

    return Container(
      padding: EdgeInsets.all(16),
      width: MediaQuery
          .of(context)
          .size
          .width,
      height: MediaQuery
          .of(context)
          .size
          .height,
      decoration: BoxDecoration(
          color: colorMainBg
      ),

      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,

        children: [
          Container(
            width: MediaQuery
                .of(context)
                .size
                .width * 0.2,
            height: MediaQuery
                .of(context)
                .size
                .height * 0.2,
            alignment: Alignment.center,
            child: Align(
              alignment: Alignment.center,
              child: CircleAvatar(
                backgroundImage: AssetImage(
                    './assets/piak.png'),
                radius: 70,
              ),
            ),
            ),
          Container(
            width: double.infinity,
            height: 300,
            padding: EdgeInsets.all(20),

            decoration: BoxDecoration(
                color: Color.fromRGBO(60, 60, 60, 1),
                borderRadius: BorderRadius.circular(5)
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        child: Text(
                          '이름',
                          style: TextStyle(
                              fontFamily: 'NotoSans_NotoSansKR-Regular',
                              fontSize: fontSizeMainText,
                              color: colorText,
                              letterSpacing: -0.5
                          ),
                        ),
                      ),
                      Container(
                        child: Text(
                          '${_response?.name}',
                          style: TextStyle(
                              fontFamily: 'NotoSans_Bold',
                              fontSize: fontSizeMainText,
                              color: colorTextBright,
                              letterSpacing: -0.5
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        child: Text(
                          '배달지역 변경',
                          style: TextStyle(
                              fontFamily: 'NotoSans_NotoSansKR-Regular',
                              fontSize: fontSizeMainText,
                              color: colorText,
                              letterSpacing: -0.5
                          ),
                        ),
                      ),
                      Container(
                        child: Text(
                          '${_response?.addressWish}',
                          style: TextStyle(
                              fontFamily: 'NotoSans_Bold',
                              fontSize: fontSizeMainText,
                              color: colorTextBright,
                              letterSpacing: -0.5
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        child: Text(
                          '운송 수단 관리',
                          style: TextStyle(
                              fontFamily: 'NotoSans_NotoSansKR-Regular',
                              fontSize: fontSizeMainText,
                              color: colorText,
                              letterSpacing: -0.5
                          ),
                        ),
                      ),
                      Container(
                        child:
                        Text(
                          '${_response?.driveType} / ${_response?.driveNumber}',
                          style: TextStyle(
                              fontFamily: 'NotoSans_Bold',
                              fontSize: fontSizeMainText,
                              color: colorTextBright,
                              letterSpacing: -0.5
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        child: Text(
                          '기기 관리',
                          style: TextStyle(
                              fontFamily: 'NotoSans_NotoSansKR-Regular',
                              fontSize: fontSizeMainText,
                              color: colorText,
                              letterSpacing: -0.5
                          ),
                        ),
                      ),
                      Container(
                        child: Text(
                          '${_response?.phoneType}',
                          style: TextStyle(
                              fontFamily: 'NotoSans_Bold',
                              fontSize: fontSizeMainText,
                              color: colorTextBright,
                              letterSpacing: -0.5
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                // Divider(
                //   thickness: 1,
                //   height: 0,
                //   color: colorDivider,
                // ),
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        child: Text(
                          '계좌 관리',
                          style: TextStyle(
                              fontFamily: 'NotoSans_NotoSansKR-Regular',
                              fontSize: fontSizeMainText,
                              color: colorText,
                              letterSpacing: -0.5
                          ),
                        ),
                      ),
                      Container(
                        child: Text(
                          '${_response?.bankName}',
                          style: TextStyle(
                              fontFamily: 'NotoSans_Bold',
                              fontSize: fontSizeMainText,
                              color: colorTextBright,
                              letterSpacing: -0.5
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 12),
            decoration: BoxDecoration(
                color: Color.fromRGBO(60, 60, 60, 1),
                borderRadius: BorderRadius.circular(5)
            ),
            width: double.infinity,
            height: 55,
            padding: EdgeInsets.only(left: 20),
            child: GestureDetector(
              onTap: () {
                // Navigator.of(context).pushNamed("/page_delivery_fee");
              },
              behavior: HitTestBehavior.translucent,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                      '안전보건교육 수료증',
                      style: TextStyle(
                          fontFamily: 'NotoSans_NotoSansKR-Regular',
                          fontSize: fontSizeMainText,
                          color: colorText,
                          letterSpacing: -0.5
                      ),
                  ),
                ],
              )
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 12),
            decoration: BoxDecoration(
                color: Color.fromRGBO(60, 60, 60, 1),
                borderRadius: BorderRadius.circular(5)
            ),
            width: double.infinity,
            height: 55,
            padding: EdgeInsets.only(left: 20),
            child: GestureDetector(
              onTap: () {
                // Navigator.of(context).pushNamed("/page_delivery_fee");
              },
              behavior: HitTestBehavior.translucent,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    '비밀번호 재설정',
                    style: TextStyle(
                        fontFamily: 'NotoSans_NotoSansKR-Regular',
                        fontSize: fontSizeMainText,
                        color: colorText,
                        letterSpacing: -0.5
                    ),
                  ),
                ],
              )
            ),
          ),
          /**수정 페이지 이동 버튼**/
          Container(
            padding: EdgeInsets.only(top: 12, bottom: 20),
            alignment: Alignment.center,
            child: ElevatedButton(
              child: Text(
                  '수정하기',
                style: TextStyle(
                    fontFamily: 'NotoSans_Bold',
                    fontSize: fontSizeMainText,
                    color: Colors.white,
                    letterSpacing: -0.5
                ),
              ),
              onPressed: () {
                Navigator.of(context).pushNamed('/page_modify_my_info');
              },
              style: ElevatedButton.styleFrom(
                  minimumSize: Size(400, 50),
                  foregroundColor: Colors.white,
                  backgroundColor: colorPoint,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5)
                  )
              ),
            ),

          ),
        ],
      ),
    );
  }
}




import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:intl/intl.dart';
import 'package:matddak_v1/component/component_order_cancel_item.dart';
import 'package:matddak_v1/component/component_order_fin_item.dart';
import 'package:matddak_v1/component/component_order_get.dart';
import 'package:matddak_v1/component/component_order_request_item.dart';
import 'package:matddak_v1/component/component_order_start_response.dart';
import 'package:matddak_v1/config/config_color.dart';
import 'package:matddak_v1/config/config_text_size.dart';
import 'package:matddak_v1/map_order/map_order_get.dart';
import 'package:matddak_v1/map_order/map_order_start.dart';
import 'package:matddak_v1/model/order/cancel/order_cancel_item.dart';
import 'package:matddak_v1/model/order/fin/order_fin_item.dart';
import 'package:matddak_v1/model/order/request/order_request_item.dart';
import 'package:matddak_v1/model/order/total_count/tab_count_response.dart';
import 'package:matddak_v1/model/rider/rider_info_response.dart';
import 'package:matddak_v1/pages/page_change_my_info.dart';
import 'package:matddak_v1/pages/page_order_history.dart';
import 'package:matddak_v1/pages/payment/page_delivery_fee.dart';
import 'package:matddak_v1/pages/payment/page_income_declatarion.dart';
import 'package:matddak_v1/pages/payment/page_notice_list.dart';
import 'package:matddak_v1/pages/payment/page_out_money.dart';
import 'package:matddak_v1/pages/payment/page_out_money_detail.dart';
import 'package:matddak_v1/pages/payment/page_pay_details.dart';
import 'package:matddak_v1/pages/payment/page_pay_main.dart';
import 'package:matddak_v1/repository/repo_order.dart';
import 'package:matddak_v1/repository/repo_rider_info.dart';

class PageRiderMain extends StatefulWidget {
  const PageRiderMain({Key? key}) : super(key:key);

  @override
  State<PageRiderMain> createState() => _PageRiderMainState();
}

class _PageRiderMainState extends State<PageRiderMain> with TickerProviderStateMixin {
  late TabController _tabController = TabController(length: 5, vsync: this);
  bool _isWorked = true; // 드로우 안 on & off
  final formatCurrency = new NumberFormat.simpleCurrency(locale: "ko_KR", name: "", decimalDigits: 0);

  @override
  void initState() {
    super.initState();
    _tabController.addListener(() {
    });
    _loadList(); // 요청 리스트
    _loadFinList(); // 완료 리스트
    _loadCancelList(); // 취소 리스트
    _loadRiderInfo(); // 드로워 라이더 인포
    _loadTabCount(); // 탭 카운트
    _loadFinList(); // 완료
  }

  // 탭 컨트롤러는 애니메이션을 사용하기에 dispose() 함수를 호출해야 메모리 누수를 막을 수 있다.
  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }


  /** drawer 라이더 정보 **/
  RiderInfoResponse? _response;

  Future<void> _loadRiderInfo() async {
    await RepoRiderInfo().getRiderInfo()
        .then((res) =>{
      setState(() {
        _response = res.data;
      })
    })
        .catchError((err) => {
      debugPrint(err)
    });
  }

  /** tabBar 토탈 카운트 **/
  TabCountResponse? _tabCountResponse;

  Future<void> _loadTabCount() async {
    await RepoOrder().getTabCount()
        .then((res) => {
          setState(() {
            _tabCountResponse = res.data;
          })
    })
        .catchError((err) => {
          debugPrint(err)
    });
  }

  final _formKey = GlobalKey<FormBuilderState>();

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: 0,
      length: 5,
      child: Scaffold(
        /** 앱바 **/
        appBar: PreferredSize( /** 앱바 높이 지정 **/
          preferredSize: Size.fromHeight(130),
          child: AppBar(
            centerTitle: true,
            title: Image.asset(
              "./assets/img.png",
              width: MediaQuery.of(context).size.width * 0.23,
            ),
            foregroundColor: colorText,
            backgroundColor: colorBox,
            actions: <Widget>[
              /** 보유페이 **/
              TextButton(
                onPressed: () {
                  Navigator.of(context).pushNamed('/page_pay_main');
                },
                child: Text(
                  '${formatCurrency.format(_response!.payNow)} 원',
                  style: TextStyle(
                      fontSize: fontSizeSuper,
                      fontFamily: 'NotoSans_NotoSansKR-Medium',
                      color: colorText,
                      letterSpacing: -0.5
                  ),
                ),
                style: TextButton.styleFrom(
                    foregroundColor: Colors.grey
                ),
              )
            ],
            /** 탭바 **/
            bottom: TabBar(
              controller: _tabController,
              tabs: [
                Tab(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        '${_tabCountResponse!.requestCount}',
                        // '15',
                        style: TextStyle(
                            fontFamily: 'NotoSans_NotoSansKR-regular',
                            height: 0.5,
                            fontSize: tabBarText
                        ),
                      ),
                      Text(
                        '요청',
                        style: TextStyle(
                          height: 0,
                        ),
                      ),
                    ],
                  ),
                ),
                Tab(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        '${_tabCountResponse!.pickCount}',
                        style: TextStyle(
                            fontFamily: 'NotoSans_NotoSansKR-regular',
                            height: 0.5,
                            fontSize: tabBarText
                        ),
                      ),
                      Text(
                        '배차',
                        style: TextStyle(
                            height: 0
                        ),
                      ),
                    ],
                  ),
                ),
                Tab(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        '${_tabCountResponse!.goCount}',
                        style: TextStyle(
                            fontFamily: 'NotoSans_NotoSansKR-regular',
                            height: 0.5,
                            fontSize: tabBarText
                        ),
                      ),
                      Text(
                        '출발',
                        style: TextStyle(
                            height: 0
                        ),
                      ),
                    ],
                  ),
                ),
                Tab(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        '${_tabCountResponse!.doneCount}',
                        style: TextStyle(
                            fontFamily: 'NotoSans_NotoSansKR-regular',
                            height: 0.5,
                            fontSize: tabBarText
                        ),
                      ),
                      Text(
                        '완료',
                        style: TextStyle(
                            height: 0
                        ),
                      ),
                    ],
                  ),
                ),
                Tab(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        '${_tabCountResponse!.cancelCount}',
                        style: TextStyle(
                            fontFamily: 'NotoSans_NotoSansKR-regular',
                            height: 0.5,
                            fontSize: tabBarText
                        ),
                      ),
                      Text(
                        '취소',
                        style: TextStyle(
                            height: 0
                        ),
                      ),
                    ],
                  ),
                )
              ],
              labelPadding: EdgeInsets.fromLTRB(0, 16, 0, 8), /** 탭바 패딩 **/
              labelColor: colorLogin, /** 선택된 탭바 텍스트 컬러 **/
              labelStyle: TextStyle( /** 탭바 텍스트 **/
                fontFamily: 'NotoSans_Bold',
                fontSize: 20,
                letterSpacing: -0.5,
              ),
              unselectedLabelColor: colorText, /** 선택 안된 탭바 텍스트 컬러 **/
              overlayColor: MaterialStatePropertyAll(colorPrimary), /** 클릭 이벤트 컬러 **/
              indicatorSize: TabBarIndicatorSize.tab, /** 탭바 선택 시 사이즈 **/
              indicator: BoxDecoration( /** 탭바 선택 시 배경 컬러 **/
                color: colorPoint,
              ),
              dividerColor: colorPoint, /** 탭 마다 밑줄 넣어주기 **/
              dividerHeight: 1.5, /** 탭 밑줄 굵기 **/
            ),
          ),
        ),
        /** 드로어 메뉴 **/
        drawer: Drawer(
          backgroundColor: colorBox,
          surfaceTintColor : colorBox,
          child: ListView(
            children: [
              SizedBox(
                height: 260,
                /** 메뉴 헤더 **/
                child: UserAccountsDrawerHeader(
                  margin: EdgeInsets.zero,
                  arrowColor: colorText,
                  /** 프로필 아이콘 **/
                  currentAccountPicture:  Align(
                    alignment: Alignment.center,
                    child: Container(
                      margin: EdgeInsets.fromLTRB(10, 50, 0, 0),
                      child: CircleAvatar(
                        backgroundImage: AssetImage(
                            './assets/piak.png'),
                        radius: 70,
                      ),
                    ),
                  ),
                  /** 라이더 이름, 상태, 지역 텍스트 **/
                  otherAccountsPictures: [
                    Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                                margin: EdgeInsets.fromLTRB(125, 7, 0, 0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      crossAxisAlignment: CrossAxisAlignment.end,
                                      children: [
                                        Text(
                                          _response!.riderName,
                                          // '민희진',
                                            style: TextStyle(
                                                fontFamily: 'NotoSans_Bold',
                                                fontSize: fontSizeSuper,
                                                letterSpacing: -0.5,
                                                color: colorPoint
                                            )
                                        ),
                                        Text(
                                            ' 라이더님',
                                            style: TextStyle(
                                                fontFamily: 'NotoSans_NotoSansKR-Regular',
                                                fontSize: fontSizeMainText,
                                                letterSpacing: -0.5,
                                                color: colorText
                                            )
                                        ),
                                      ],
                                    ),
                                    Text(
                                        // '${_response!.isWork} 중입니다.',
                                      _isWorked == true ? '정상 업무중입니다.' : '휴식중입니다.',
                                        style: TextStyle(
                                            fontFamily: 'NotoSans_NotoSansKR-Regular',
                                            fontSize: fontSizeMainText,
                                            letterSpacing: -0.5,
                                            color: colorText
                                        )
                                    ),
                                    Text(
                                        _response!.addressWish,
                                      // '안산시 단원구',
                                        style: TextStyle(
                                            fontFamily: 'NotoSans_NotoSansKR-Regular',
                                            fontSize: fontSizeMainText,
                                            letterSpacing: -0.5,
                                            color: colorText
                                        )
                                    ),
                                  ],
                                )
                            ),
                            /** 업무 상태 스위치 **/
                            Container(
                              margin: EdgeInsets.fromLTRB(15, 20, 0, 0),
                              child: CupertinoSwitch(
                                  value: _isWorked,
                                  activeColor: colorPoint,
                                  onChanged: (bool? value) {
                                    setState(() {
                                      _isWorked = value ?? true;
                                    });
                                  },
                              ),
                            ),
                          ],
                        )
                    ),
                  ],
                  /** 라이더 이름, 상태, 지역 사이즈 **/
                  otherAccountsPicturesSize: MediaQuery.of(context).size / 1.55,
                  // otherAccountsPicturesSize: const Size.square(265.0),
                  /** 완료건, 페이 타이틀 **/
                  accountName: Container(
                      margin: EdgeInsets.fromLTRB(8, 43, 0, 0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Container(
                                child: Text(
                                    '오늘 총 완료건',
                                    style: TextStyle(
                                        fontFamily: 'NotoSans_NotoSansKR-Regular',
                                        fontSize: fontSizeMainText,
                                        letterSpacing: -0.5,
                                        color: colorText
                                    )
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.fromLTRB(65, 0, 0, 0),
                                child: Text(
                                    '오늘의 페이',
                                    style: TextStyle(
                                        fontFamily: 'NotoSans_NotoSansKR-Regular',
                                        fontSize: fontSizeMainText,
                                        letterSpacing: -0.5,
                                        color: colorText
                                    )
                                ),
                              ),
                            ],
                          ),
                        ],
                      )
                  ),
                  /** 완료건, 페이 수치 **/
                  accountEmail: Container(
                    margin: EdgeInsets.only(left: 8.0),
                    child: Row(
                      children: [
                        Container(
                          child: Text(
                              '${_tabCountResponse!.doneCount}',
                              style: TextStyle(
                                  fontFamily: 'NotoSans_Bold',
                                  fontSize: fontSizeTitle,
                                  letterSpacing: -0.5,
                                  color: colorText
                              )
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 139.0),
                          child: Text(
                              formatCurrency.format(_response!.payNow),
                            // '6500',
                              style: TextStyle(
                                  fontFamily: 'NotoSans_Bold',
                                  fontSize: fontSizeTitle,
                                  letterSpacing: -0.5,
                                  color: colorText
                              )
                          ),
                        ),
                      ],
                    ),
                  ),
                  decoration: BoxDecoration(
                    color: colorBox,
                  ),
                ),
              ),
              /** 메뉴 리스트 **/
              Ink(
                color: colorMainBg,
                child: Container(
                  margin: EdgeInsets.zero,
                  child: Column(
                    children: [
                      ListTile(
                        title: Text(
                          '내 정보 관리',
                          style: TextStyle(
                              fontFamily: 'NotoSans_NotoSansKR-Regular',
                              fontSize: fontSizeMainText,
                              letterSpacing: -0.5,
                              color: colorText
                          ),
                        ),
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PageChangeMyInfo()));
                        },
                        tileColor: colorDrawerListColor,
                      ),
                      Container(
                        color: Color.fromRGBO(127, 127, 127, 0.3),
                        height: 1,
                      ),
                      ListTile(
                        title: Text(
                          '페이 및 수입 내역',
                          style: TextStyle(
                              fontFamily: 'NotoSans_NotoSansKR-Regular',
                              fontSize: fontSizeMainText,
                              letterSpacing: -0.5,
                              color: colorText
                          ),
                        ),
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PagePayMain()));
                        },
                        tileColor: colorDrawerListColor,
                      ),
                      Container(
                        color: Color.fromRGBO(127, 127, 127, 0.3),
                        height: 1,
                      ),
                      ListTile(
                        title: Text(
                          '공지사항',
                          style: TextStyle(
                              fontFamily: 'NotoSans_NotoSansKR-Regular',
                              fontSize: fontSizeMainText,
                              letterSpacing: -0.5,
                              color: colorText
                          ),
                        ),
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PageNoticeList()));
                        },
                        tileColor: colorDrawerListColor,
                      ),
                      Container(
                        color: Color.fromRGBO(127, 127, 127, 0.3),
                        height: 1,
                      ),
                      ListTile(
                        title: Text(
                          '주문 과거 내역',
                          style: TextStyle(
                              fontFamily: 'NotoSans_NotoSansKR-Regular',
                              fontSize: fontSizeMainText,
                              letterSpacing: -0.5,
                              color: colorText
                          ),
                        ),
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PageOrderHistory()));
                        },
                        tileColor: colorDrawerListColor,
                      ),
                      Container(
                        color: Color.fromRGBO(127, 127, 127, 0.3),
                        height: 1,
                      ),
                      ListTile(
                        title: Text(
                          '페이 내역',
                          style: TextStyle(
                              fontFamily: 'NotoSans_NotoSansKR-Regular',
                              fontSize: fontSizeMainText,
                              letterSpacing: -0.5,
                              color: colorText
                          ),
                        ),
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PagePayDetails()));
                        },
                        tileColor: colorDrawerListColor,
                      ),
                      Container(
                        color: Color.fromRGBO(127, 127, 127, 0.3),
                        height: 1,
                      ),
                      ListTile(
                        title: Text(
                          '배송료 수익 내역',
                          style: TextStyle(
                              fontFamily: 'NotoSans_NotoSansKR-Regular',
                              fontSize: fontSizeMainText,
                              letterSpacing: -0.5,
                              color: colorText
                          ),
                        ),
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PageDeliveryFee()));
                        },
                        tileColor: colorDrawerListColor,
                      ),
                      Container(
                        color: Color.fromRGBO(127, 127, 127, 0.3),
                        height: 1,
                      ),
                      ListTile(
                        title: Text(
                          '페이 출금하기',
                          style: TextStyle(
                              fontFamily: 'NotoSans_NotoSansKR-Regular',
                              fontSize: fontSizeMainText,
                              letterSpacing: -0.5,
                              color: colorText
                          ),
                        ),
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PageOutMoney()));
                        },
                        tileColor: colorDrawerListColor,
                      ),
                      Container(
                        color: Color.fromRGBO(127, 127, 127, 0.3),
                        height: 1,
                      ),
                      ListTile(
                        title: Text(
                          '페이 출금 내역',
                          style: TextStyle(
                              fontFamily: 'NotoSans_NotoSansKR-Regular',
                              fontSize: fontSizeMainText,
                              letterSpacing: -0.5,
                              color: colorText
                          ),
                        ),
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PageOutMoneyDetail()));
                        },
                        tileColor: colorDrawerListColor,
                      ),
                      Container(
                        color: Color.fromRGBO(127, 127, 127, 0.3),
                        height: 1,
                      ),
                      ListTile(
                        title: Text(
                          '월단위 소득신고 내역',
                          style: TextStyle(
                              fontFamily: 'NotoSans_NotoSansKR-Regular',
                              fontSize: fontSizeMainText,
                              letterSpacing: -0.5,
                              color: colorText
                          ),
                        ),
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PageIncomeDeclatarion()));
                        },
                        tileColor: colorDrawerListColor,
                      ),
                      Container(
                        color: Color.fromRGBO(127, 127, 127, 0.3),
                        height: 1,
                      ),
                      ListTile(
                        title: Text(
                          '앱 버전 정보',
                          style: TextStyle(
                              fontFamily: 'NotoSans_NotoSansKR-Regular',
                              fontSize: fontSizeMainText,
                              letterSpacing: -0.5,
                              color: colorText
                          ),
                        ),
                        onTap: () {},
                        tileColor: colorDrawerListColor,
                      ),
                    ],
                  ),
                )
              )
            ],
          ),
        ),
        /** 탭바 컨텐츠 **/
        body: FormBuilder(
          key: _formKey,
          child: Column(
            children: [
              Expanded(
                child: TabBarView(
                  controller: _tabController,
                  /** 구글맵 확대, 축소 **/
                  physics: NeverScrollableScrollPhysics(),
                  children: [
                    /** 요청 컨텐츠 **/
                    orderRequest(),
                    /** 배차 컨텐츠 **/
                    orderGet(),
                    /** 출발 컨텐츠 **/
                    orderStart(),
                    /** 완료 컨텐츠 **/
                    orderFin(),
                    /** 취소 컨텐츠 **/
                    orderCancel(),
                  ],
                ),
              ),
            ],
          )
        ),
      ),
    );
  }

  /** 요청 리스트 연동 **/
  List<OrderRequestItem> _OrderRequestItem = [];

  final _scrollController = ScrollController();

  Future<void> _loadList() async {
    await RepoOrder().getList()
        .then((res) =>{
      setState(() {
        _OrderRequestItem = res.list;
      })
    })
        .catchError((err) => {
      debugPrint(err)
    });
  }

  /** 배달 요청 -> 배차 신청 C **/
  Future<void> _setPick({required num id}) async {
    await RepoOrder().setPick(id).then((value) {
      print('성공 토스트 팝업 위치');
      _loadTabCount();
      _tabController.index = 1;
    }).catchError((onError) {
      debugPrint(onError);
    });
  }

  /** 요청 컨텐츠 **/
  Widget orderRequest() {
    return Scaffold(
      body: Ink( color: colorMainBg,
        child: ListView.builder(
          controller: _scrollController,
          itemCount: _OrderRequestItem.length,
          itemBuilder: (BuildContext ctx, int idx) {
            return ComponentRequestItem(
                orderRequestItem: _OrderRequestItem[idx],
                callback: () {
                  _tabController.index = 1;
                  _setPick(id: _OrderRequestItem[idx].id);
                }
            );
          },
        ),
      ),
    );
  }

  /** 배달 배차 -> 배차 출발 U **/
  Future<void> _putStart() async {
    await RepoOrder().putStart().then((value) {
      print('성공 토스트 팝업 위치');
      _loadTabCount();
      _tabController.index = 2;
      // askId = widget.askId;
    }).catchError((onError) {
      debugPrint(onError);
    });
  }

  /** 배차 컨텐츠 **/
  Widget orderGet() {
    return Stack(
      children: [
        Container(
          child: Column(
            children: [
              Container(
                  height: MediaQuery.of(context).size.height / 2.65,
                  child: MapOrderGet()
              ),
            ],
          ),
        ),
        Container(
          child: Column(
            children: [
              Expanded(
                child: Column(
                    children: [
                      ComponentOrderGet(),
                      /** 출발하기 버튼 **/
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: 55,
                        child: ElevatedButton(
                          child: Text(
                            '출발하기',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: fontSizeSuper,
                              fontFamily: 'NotoSans_Bold',
                              letterSpacing: -0.5,
                            ),
                          ),
                          style: ElevatedButton.styleFrom(
                              primary: colorPoint,
                              onPrimary: Colors.yellow,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(0)
                              ),
                              padding: EdgeInsets.zero,
                              elevation: 0
                          ),
                          onPressed: () {
                            _tabController.index = 2;
                            _putStart();
                          },
                        ),
                      ),
                    ]
                ),
              )
            ],
          ),
        )
      ],
    );
  }

  /** 배달 출발 -> 완료 수정 U **/
  Future<void> _putFin() async {
    await RepoOrder().putFin().then((value) {
      print('성공 토스트 팝업 위치');
      _loadTabCount();
      _tabController.index = 3;
      _loadFinList();
    }).catchError((onError) {
      debugPrint(onError);
    });
  }

  /** 출발 컨텐츠 **/
  Widget orderStart() {
    return Stack(
      children: [
        Container(
          color: colorMainBg,
          child: Column(
            children: [
              /** 구글맵 **/
              Container(
                  height: MediaQuery.of(context).size.height / 2.45,
                  child: MapOrderStart()
              ),
            ],
          ),
        ),
        Container(
          child: Column(
            children: [
              Expanded(
                  child: Column(
                    children: [
                      ComponentOrderStartResponse(),
                      /** 완료하기 버튼 **/
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: 55,
                        child: ElevatedButton(
                          child: Text(
                            '완료하기',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: fontSizeSuper,
                              fontFamily: 'NotoSans_Bold',
                              letterSpacing: -0.5,
                            ),
                          ),
                          style: ElevatedButton.styleFrom(
                              primary: colorPoint,
                              onPrimary: Colors.yellow,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(0)
                              ),
                              padding: EdgeInsets.zero,
                              elevation: 0
                          ),
                          onPressed: () {
                            // Navigator.of(context).pushNamed('/page_order_fin_detail');
                            _tabController.index = 3;
                            _putFin();
                          },
                        ),
                      ),
                    ],
                  )
              ),
            ],
          ),
        )
      ],
    );
  }

  /** 완료 리스트 연동 **/
  List<OrderFinItem> _OrderFinItem = [];

  Future<void> _loadFinList() async {
    await RepoOrder().getFins()
        .then((res) =>{
      setState(() {
        _OrderFinItem = res.list;
      })
    })
        .catchError((err) => {
      debugPrint(err)
    });
  }

  /** 완료 컨텐츠 **/
  Widget orderFin() {
    return Scaffold(
      body: Ink( color: colorMainBg,
        child: ListView.builder(
          controller: _scrollController,
          itemCount: _OrderFinItem.length,
          itemBuilder: (BuildContext ctx, int idx) {
            return Container(
              color: colorMainBg,
              child: ComponentOrderFinItem(
                orderFinItem: _OrderFinItem[idx],
              ),
            );
          },
        ),
      )
    );
  }

  /** 취소 리스트 연동 **/
  List<OrderCancelItem> _OrderCancelItem = [];

  Future<void> _loadCancelList() async {
    await RepoOrder().getCancels()
        .then((res) =>{
      setState(() {
        _OrderCancelItem = res.list;
      })
    })
        .catchError((err) => {
      debugPrint(err)
    });
  }

  /** 취소 컨텐츠 **/
  Widget orderCancel() {
    return Scaffold(
      body: Ink( color: colorMainBg,
        child: ListView.builder(
          controller: _scrollController,
          itemCount: _OrderCancelItem.length,
          itemBuilder: (BuildContext ctx, int idx) {
            return Container(
              color: colorMainBg,
              child: ComponentOrderCancelItem(
                  orderCancelItem: _OrderCancelItem[idx]
              ),
            );
          },
        ),
      ),
    );
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:intl/intl.dart';
import 'package:matddak_v1/config/config_color.dart';
import 'package:matddak_v1/model/order/request/order_get_response.dart';
import 'package:matddak_v1/model/order/total_count/tab_count_response.dart';
import 'package:matddak_v1/model/rider/rider_info_response.dart';
import 'package:matddak_v1/pages/page_change_my_info.dart';
import 'package:matddak_v1/pages/page_order_history.dart';
import 'package:matddak_v1/pages/payment/page_delivery_fee.dart';
import 'package:matddak_v1/pages/payment/page_income_declatarion.dart';
import 'package:matddak_v1/pages/payment/page_notice_list.dart';
import 'package:matddak_v1/pages/payment/page_out_money.dart';
import 'package:matddak_v1/pages/payment/page_out_money_detail.dart';
import 'package:matddak_v1/pages/payment/page_pay_details.dart';
import 'package:matddak_v1/repository/repo_order.dart';
import 'package:matddak_v1/repository/repo_rider_info.dart';

import '../../config/config_text_size.dart';
import '../payment/page_pay_main.dart';

class PageOrderFinDetail extends StatefulWidget {
  const PageOrderFinDetail({
    super.key,
    required this.deliveryId
  });

  final num deliveryId;

  @override
  State<PageOrderFinDetail> createState() => _PageOrderFinDetailState();
}

class _PageOrderFinDetailState extends State<PageOrderFinDetail> {
  bool _isWorked = true; // 드로우 안 on & off
  final formatCurrency = new NumberFormat.simpleCurrency(locale: "ko_KR", name: "", decimalDigits: 0);

  OrderGetResponse? _orderContents;

  Future<void> _loadOrderContents() async {
    await RepoOrder().getFinDetail(widget.deliveryId)
        .then((res) => {
      setState(() {
        _orderContents = res.data;
      })
    });
  }

  /** drawer 라이더 정보 **/
  RiderInfoResponse? _response;

  Future<void> _loadRiderInfo() async {
    await RepoRiderInfo().getRiderInfo()
        .then((res) =>{
      setState(() {
        _response = res.data;
      })
    })
        .catchError((err) => {
      debugPrint(err)
    });
  }

  /** tabBar 토탈 카운트 **/
  TabCountResponse? _tabCountResponse;

  Future<void> _loadTabCount() async {
    await RepoOrder().getTabCount()
        .then((res) => {
      setState(() {
        _tabCountResponse = res.data;
      })
    })
        .catchError((err) => {
      debugPrint(err)
    });
  }

  void initState() {
    super.initState();
    _loadOrderContents();
    _loadRiderInfo();
    _loadTabCount();
  }

  @override
  Widget build(BuildContext context) {
    final formatCurrency = new NumberFormat.simpleCurrency(locale: "ko_KR", name: "", decimalDigits: 0);

    if (_orderContents == null) {
      return Scaffold(
        appBar: PreferredSize( /** 앱바 높이 지정 **/
          preferredSize: Size.fromHeight(55.0),
          /** 앱바 **/
          child: AppBar(
            title: Text(
              '완료내역',
              style: TextStyle(
                  fontFamily: 'NotoSans_Bold',
                  fontSize: fontSizeTitle,
                  color: colorPoint,
                  letterSpacing: -0.5
              ),
            ),
            centerTitle: true,
            foregroundColor: colorText,
            backgroundColor: colorBox,
            actions: <Widget>[
              TextButton(
                onPressed: () {
                  Navigator.of(context).pushNamed('/page_pay_main');
                },
                child: Text(
                  '${formatCurrency.format(_response!.payNow)} 원',
                  style: TextStyle(
                    fontSize: fontSizeSuper,
                    fontFamily: 'NotoSans_NotoSansKR-Medium',
                    color: colorText,
                    letterSpacing: -0.5,
                  ),
                ),
                style: TextButton.styleFrom(
                    foregroundColor: Colors.grey
                ),
              )
            ],
          ),
        ),
        /** 드로어 메뉴 **/
        drawer: Drawer(
          backgroundColor: colorBox,
          surfaceTintColor : colorBox,
          child: ListView(
            children: [
              SizedBox(
                height: 260,
                /** 메뉴 헤더 **/
                child: UserAccountsDrawerHeader(
                  margin: EdgeInsets.zero,
                  arrowColor: colorText,
                  /** 프로필 아이콘 **/
                  currentAccountPicture: CircleAvatar(
                      child: Container(
                        margin: EdgeInsets.only(top: 15.0),
                        child: Icon(
                          Icons.account_circle,
                          size: 80,
                          color: colorText,
                        ),
                      )
                  ),
                  /** 라이더 이름, 상태, 지역 텍스트 **/
                  otherAccountsPictures: [
                    Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                                margin: EdgeInsets.fromLTRB(105, 15, 0, 0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      crossAxisAlignment: CrossAxisAlignment.end,
                                      children: [
                                        Text(
                                            _response!.riderName,
                                            // '민희진',
                                            style: TextStyle(
                                                fontFamily: 'NotoSans_Bold',
                                                fontSize: fontSizeSuper,
                                                letterSpacing: -0.5,
                                                color: colorPoint
                                            )
                                        ),
                                        Text(
                                            ' 라이더님',
                                            style: TextStyle(
                                                fontFamily: 'NotoSans_NotoSansKR-Regular',
                                                fontSize: fontSizeMainText,
                                                letterSpacing: -0.5,
                                                color: colorText
                                            )
                                        ),
                                      ],
                                    ),
                                    Text(
                                      // '${_response!.isWork} 중입니다.',
                                        _isWorked == true ? '정상 업무중입니다.' : '휴식중입니다.',
                                        style: TextStyle(
                                            fontFamily: 'NotoSans_NotoSansKR-Regular',
                                            fontSize: fontSizeMainText,
                                            letterSpacing: -0.5,
                                            color: colorText
                                        )
                                    ),
                                    Text(
                                        _response!.addressWish,
                                        // '안산시 단원구',
                                        style: TextStyle(
                                            fontFamily: 'NotoSans_NotoSansKR-Regular',
                                            fontSize: fontSizeMainText,
                                            letterSpacing: -0.5,
                                            color: colorText
                                        )
                                    ),
                                  ],
                                )
                            ),
                            /** 업무 상태 스위치 **/
                            Container(
                              margin: EdgeInsets.fromLTRB(13, 15, 0, 0),
                              child: CupertinoSwitch(
                                value: _isWorked,
                                activeColor: colorPoint,
                                onChanged: (bool? value) {
                                  setState(() {
                                    _isWorked = value ?? true;
                                  });
                                },
                              ),
                            ),
                          ],
                        )
                    ),
                  ],
                  /** 라이더 이름, 상태, 지역 사이즈 **/
                  otherAccountsPicturesSize: MediaQuery.of(context).size / 1.55,
                  // otherAccountsPicturesSize: const Size.square(265.0),
                  /** 완료건, 페이 타이틀 **/
                  accountName: Container(
                      margin: EdgeInsets.fromLTRB(8, 40, 0, 0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Container(
                                child: Text(
                                    '오늘 총 완료건',
                                    style: TextStyle(
                                        fontFamily: 'NotoSans_NotoSansKR-Regular',
                                        fontSize: fontSizeMainText,
                                        letterSpacing: -0.5,
                                        color: colorText
                                    )
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.fromLTRB(65, 0, 0, 0),
                                child: Text(
                                    '오늘의 페이',
                                    style: TextStyle(
                                        fontFamily: 'NotoSans_NotoSansKR-Regular',
                                        fontSize: fontSizeMainText,
                                        letterSpacing: -0.5,
                                        color: colorText
                                    )
                                ),
                              ),
                            ],
                          ),
                        ],
                      )
                  ),
                  /** 완료건, 페이 수치 **/
                  accountEmail: Container(
                    margin: EdgeInsets.only(left: 8.0),
                    child: Row(
                      children: [
                        Container(
                          child: Text(
                              '${_tabCountResponse!.doneCount}',
                              style: TextStyle(
                                  fontFamily: 'NotoSans_Bold',
                                  fontSize: fontSizeTitle,
                                  letterSpacing: -0.5,
                                  color: colorText
                              )
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 139.0),
                          child: Text(
                              formatCurrency.format(_response!.payNow),
                              // '6500',
                              style: TextStyle(
                                  fontFamily: 'NotoSans_Bold',
                                  fontSize: fontSizeTitle,
                                  letterSpacing: -0.5,
                                  color: colorText
                              )
                          ),
                        ),
                      ],
                    ),
                  ),
                  decoration: BoxDecoration(
                    color: colorBox,
                  ),
                ),
              ),
              /** 메뉴 리스트 **/
              Container(
                margin: EdgeInsets.zero,
                child: Column(
                  children: [
                    ListTile(
                      title: Text(
                        '내 정보 관리',
                        style: TextStyle(
                            fontFamily: 'NotoSans_NotoSansKR-Regular',
                            fontSize: fontSizeMainText,
                            letterSpacing: -0.5,
                            color: colorText
                        ),
                      ),
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PageChangeMyInfo()));
                      },
                      tileColor: colorDrawerListColor,
                    ),
                    Container(
                      color: Color.fromRGBO(127, 127, 127, 0.3),
                      height: 1,
                    ),
                    ListTile(
                      title: Text(
                        '페이 및 수입 내역',
                        style: TextStyle(
                            fontFamily: 'NotoSans_NotoSansKR-Regular',
                            fontSize: fontSizeMainText,
                            letterSpacing: -0.5,
                            color: colorText
                        ),
                      ),
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PagePayMain()));
                      },
                      tileColor: colorDrawerListColor,
                    ),
                    Container(
                      color: Color.fromRGBO(127, 127, 127, 0.3),
                      height: 1,
                    ),
                    ListTile(
                      title: Text(
                        '공지사항',
                        style: TextStyle(
                            fontFamily: 'NotoSans_NotoSansKR-Regular',
                            fontSize: fontSizeMainText,
                            letterSpacing: -0.5,
                            color: colorText
                        ),
                      ),
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PageNoticeList()));
                      },
                      tileColor: colorDrawerListColor,
                    ),
                    Container(
                      color: Color.fromRGBO(127, 127, 127, 0.3),
                      height: 1,
                    ),
                    ListTile(
                      title: Text(
                        '주문 과거 내역',
                        style: TextStyle(
                            fontFamily: 'NotoSans_NotoSansKR-Regular',
                            fontSize: fontSizeMainText,
                            letterSpacing: -0.5,
                            color: colorText
                        ),
                      ),
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PageOrderHistory()));
                      },
                      tileColor: colorDrawerListColor,
                    ),
                    Container(
                      color: Color.fromRGBO(127, 127, 127, 0.3),
                      height: 1,
                    ),
                    ListTile(
                      title: Text(
                        '페이 내역',
                        style: TextStyle(
                            fontFamily: 'NotoSans_NotoSansKR-Regular',
                            fontSize: fontSizeMainText,
                            letterSpacing: -0.5,
                            color: colorText
                        ),
                      ),
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PagePayDetails()));
                      },
                      tileColor: colorDrawerListColor,
                    ),
                    Container(
                      color: Color.fromRGBO(127, 127, 127, 0.3),
                      height: 1,
                    ),
                    ListTile(
                      title: Text(
                        '배송료 수익 내역',
                        style: TextStyle(
                            fontFamily: 'NotoSans_NotoSansKR-Regular',
                            fontSize: fontSizeMainText,
                            letterSpacing: -0.5,
                            color: colorText
                        ),
                      ),
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PageDeliveryFee()));
                      },
                      tileColor: colorDrawerListColor,
                    ),
                    Container(
                      color: Color.fromRGBO(127, 127, 127, 0.3),
                      height: 1,
                    ),
                    ListTile(
                      title: Text(
                        '페이 출금하기',
                        style: TextStyle(
                            fontFamily: 'NotoSans_NotoSansKR-Regular',
                            fontSize: fontSizeMainText,
                            letterSpacing: -0.5,
                            color: colorText
                        ),
                      ),
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PageOutMoney()));
                      },
                      tileColor: colorDrawerListColor,
                    ),
                    Container(
                      color: Color.fromRGBO(127, 127, 127, 0.3),
                      height: 1,
                    ),
                    ListTile(
                      title: Text(
                        '페이 출금 내역',
                        style: TextStyle(
                            fontFamily: 'NotoSans_NotoSansKR-Regular',
                            fontSize: fontSizeMainText,
                            letterSpacing: -0.5,
                            color: colorText
                        ),
                      ),
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PageOutMoneyDetail()));
                      },
                      tileColor: colorDrawerListColor,
                    ),
                    Container(
                      color: Color.fromRGBO(127, 127, 127, 0.3),
                      height: 1,
                    ),
                    ListTile(
                      title: Text(
                        '월단위 소득신고 내역',
                        style: TextStyle(
                            fontFamily: 'NotoSans_NotoSansKR-Regular',
                            fontSize: fontSizeMainText,
                            letterSpacing: -0.5,
                            color: colorText
                        ),
                      ),
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PageIncomeDeclatarion()));
                      },
                      tileColor: colorDrawerListColor,
                    ),
                    Container(
                      color: Color.fromRGBO(127, 127, 127, 0.3),
                      height: 1,
                    ),
                    ListTile(
                      title: Text(
                        '앱 버전 정보',
                        style: TextStyle(
                            fontFamily: 'NotoSans_NotoSansKR-Regular',
                            fontSize: fontSizeMainText,
                            letterSpacing: -0.5,
                            color: colorText
                        ),
                      ),
                      onTap: () {},
                      tileColor: colorDrawerListColor,
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
        body: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
              color: colorBox,
              boxShadow: [
                BoxShadow(
                    color: Colors.black.withOpacity(0.5),
                    spreadRadius: 3,
                    blurRadius: 10,
                    offset: Offset(0,-2)
                )
              ]
          ),
          padding: EdgeInsets.fromLTRB(30, 30, 30, 31.9),
          /** 로딩중 아이콘 **/
          child: SpinKitRing(
            color: colorText,
            size: 50.0,
          ),
        ),
      );
    } else {
      return Scaffold(
        appBar: PreferredSize( /** 앱바 높이 지정 **/
          preferredSize: Size.fromHeight(55.0),
          /** 앱바 **/
          child: AppBar(
            title: Text(
              '완료내역',
              style: TextStyle(
                  fontFamily: 'NotoSans_Bold',
                  fontSize: fontSizeTitle,
                  color: colorPoint,
                  letterSpacing: -0.5
              ),
            ),
            centerTitle: true,
            foregroundColor: colorText,
            backgroundColor: colorBox,
            actions: <Widget>[
              TextButton(
                onPressed: () {
                  Navigator.of(context).pushNamed('/page_pay_main');
                },
                child: Text(
                  '85,600원',
                  style: TextStyle(
                    fontSize: fontSizeSuper,
                    fontFamily: 'NotoSans_NotoSansKR-Medium',
                    color: colorText,
                    letterSpacing: -0.5,
                  ),
                ),
                style: TextButton.styleFrom(
                    foregroundColor: Colors.grey
                ),
              )
            ],
          ),
        ),
        /** 드로어 메뉴 **/
        drawer: Drawer(
          backgroundColor: colorBox,
          surfaceTintColor : colorBox,
          child: ListView(
            children: [
              SizedBox(
                height: 260,
                /** 메뉴 헤더 **/
                child: UserAccountsDrawerHeader(
                  margin: EdgeInsets.zero,
                  arrowColor: colorText,
                  /** 프로필 아이콘 **/
                  currentAccountPicture: CircleAvatar(
                      child: Container(
                        margin: EdgeInsets.only(top: 15.0),
                        child: Icon(
                          Icons.account_circle,
                          size: 80,
                          color: colorText,
                        ),
                      )
                  ),
                  /** 라이더 이름, 상태, 지역 텍스트 **/
                  otherAccountsPictures: [
                    Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                                margin: EdgeInsets.fromLTRB(105, 15, 0, 0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      crossAxisAlignment: CrossAxisAlignment.end,
                                      children: [
                                        Text(
                                            '홍길동',
                                            style: TextStyle(
                                                fontFamily: 'NotoSans_Bold',
                                                fontSize: fontSizeSuper,
                                                letterSpacing: -0.5,
                                                color: colorPoint
                                            )
                                        ),
                                        Text(
                                            ' 라이더님',
                                            style: TextStyle(
                                                fontFamily: 'NotoSans_NotoSansKR-Regular',
                                                fontSize: fontSizeMainText,
                                                letterSpacing: -0.5,
                                                color: colorText
                                            )
                                        ),
                                      ],
                                    ),
                                    Text(
                                        '정상업무 중입니다.',
                                        style: TextStyle(
                                            fontFamily: 'NotoSans_NotoSansKR-Regular',
                                            fontSize: fontSizeMainText,
                                            letterSpacing: -0.5,
                                            color: colorText
                                        )
                                    ),
                                    Text(
                                        '안산시 단원구',
                                        style: TextStyle(
                                            fontFamily: 'NotoSans_NotoSansKR-Regular',
                                            fontSize: fontSizeMainText,
                                            letterSpacing: -0.5,
                                            color: colorText
                                        )
                                    ),
                                  ],
                                )
                            ),
                            /** 업무 상태 스위치 **/
                            Container(
                              margin: EdgeInsets.fromLTRB(13, 18, 0, 0),
                              child: CupertinoSwitch(
                                  value: _isWorked,
                                  activeColor: colorPoint,
                                  onChanged: (bool? value) {
                                    setState(() {
                                      _isWorked = value ?? true;
                                    });
                                  }),
                            ),
                          ],
                        )
                    ),
                  ],
                  /** 라이더 이름, 상태, 지역 사이즈 **/
                  otherAccountsPicturesSize: const Size.square(265.0),
                  /** 완료건, 페이 타이틀 **/
                  accountName: Container(
                      margin: EdgeInsets.fromLTRB(8, 43, 0, 0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Container(
                                margin: EdgeInsets.only(top: 0.0),
                                child: Text(
                                    '오늘 총 완료건',
                                    style: TextStyle(
                                        fontFamily: 'NotoSans_NotoSansKR-Regular',
                                        fontSize: fontSizeMainText,
                                        letterSpacing: -0.5,
                                        color: colorText
                                    )
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.fromLTRB(65, 0, 0, 0),
                                child: Text(
                                    '오늘의 페이',
                                    style: TextStyle(
                                        fontFamily: 'NotoSans_NotoSansKR-Regular',
                                        fontSize: fontSizeMainText,
                                        letterSpacing: -0.5,
                                        color: colorText
                                    )
                                ),
                              ),
                            ],
                          ),
                        ],
                      )
                  ),
                  /** 완료건, 페이 수치 **/
                  accountEmail: Container(
                    margin: EdgeInsets.only(left: 8.0),
                    child: Row(
                      children: [
                        Container(
                          child: Text(
                              '2',
                              style: TextStyle(
                                  fontFamily: 'NotoSans_Bold',
                                  fontSize: fontSizeTitle,
                                  letterSpacing: -0.5,
                                  color: colorText
                              )
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 139.0),
                          child: Text(
                              '85,600',
                              style: TextStyle(
                                  fontFamily: 'NotoSans_Bold',
                                  fontSize: fontSizeTitle,
                                  letterSpacing: -0.5,
                                  color: colorText
                              )
                          ),
                        ),
                      ],
                    ),
                  ),
                  decoration: BoxDecoration(
                    color: colorBox,
                  ),
                ),
              ),
              /** 메뉴 리스트 **/
              Container(
                margin: EdgeInsets.zero,
                child: Column(
                  children: [
                    ListTile(
                      title: Text(
                        '내 정보 관리',
                        style: TextStyle(
                            fontFamily: 'NotoSans_NotoSansKR-Regular',
                            fontSize: fontSizeMainText,
                            letterSpacing: -0.5,
                            color: colorText
                        ),
                      ),
                      onTap: () {},
                      tileColor: colorDrawerListColor,
                    ),
                    Container(
                      color: Color.fromRGBO(127, 127, 127, 0.3),
                      height: 1,
                    ),
                    ListTile(
                      title: Text(
                        '페이 및 수입 내역',
                        style: TextStyle(
                            fontFamily: 'NotoSans_NotoSansKR-Regular',
                            fontSize: fontSizeMainText,
                            letterSpacing: -0.5,
                            color: colorText
                        ),
                      ),
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(builder: (context) => const PagePayMain()));
                      },
                      tileColor: colorDrawerListColor,
                    ),
                    Container(
                      color: Color.fromRGBO(127, 127, 127, 0.3),
                      height: 1,
                    ),
                    ListTile(
                      title: Text(
                        '공지사항',
                        style: TextStyle(
                            fontFamily: 'NotoSans_NotoSansKR-Regular',
                            fontSize: fontSizeMainText,
                            letterSpacing: -0.5,
                            color: colorText
                        ),
                      ),
                      onTap: () {},
                      tileColor: colorDrawerListColor,
                    ),
                    Container(
                      color: Color.fromRGBO(127, 127, 127, 0.3),
                      height: 1,
                    ),
                    ListTile(
                      title: Text(
                        '주문 과거 내역',
                        style: TextStyle(
                            fontFamily: 'NotoSans_NotoSansKR-Regular',
                            fontSize: fontSizeMainText,
                            letterSpacing: -0.5,
                            color: colorText
                        ),
                      ),
                      onTap: () {},
                      tileColor: colorDrawerListColor,
                    ),
                    Container(
                      color: Color.fromRGBO(127, 127, 127, 0.3),
                      height: 1,
                    ),
                    ListTile(
                      title: Text(
                        'VAN 설정',
                        style: TextStyle(
                            fontFamily: 'NotoSans_NotoSansKR-Regular',
                            fontSize: fontSizeMainText,
                            letterSpacing: -0.5,
                            color: colorText
                        ),
                      ),
                      onTap: () {},
                      tileColor: colorDrawerListColor,
                    ),
                    Container(
                      color: Color.fromRGBO(127, 127, 127, 0.3),
                      height: 1,
                    ),
                    ListTile(
                      title: Text(
                        '기능 설정',
                        style: TextStyle(
                            fontFamily: 'NotoSans_NotoSansKR-Regular',
                            fontSize: fontSizeMainText,
                            letterSpacing: -0.5,
                            color: colorText
                        ),
                      ),
                      onTap: () {},
                      tileColor: colorDrawerListColor,
                    ),
                    Container(
                      color: Color.fromRGBO(127, 127, 127, 0.3),
                      height: 1,
                    ),
                    ListTile(
                      title: Text(
                        '시간제 유상운송보험 신청',
                        style: TextStyle(
                            fontFamily: 'NotoSans_NotoSansKR-Regular',
                            fontSize: fontSizeMainText,
                            letterSpacing: -0.5,
                            color: colorText
                        ),
                      ),
                      onTap: () {},
                      tileColor: colorDrawerListColor,
                    ),
                    Container(
                      color: Color.fromRGBO(127, 127, 127, 0.3),
                      height: 1,
                    ),
                    ListTile(
                      title: Text(
                        '이용약관',
                        style: TextStyle(
                            fontFamily: 'NotoSans_NotoSansKR-Regular',
                            fontSize: fontSizeMainText,
                            letterSpacing: -0.5,
                            color: colorText
                        ),
                      ),
                      onTap: () {},
                      tileColor: colorDrawerListColor,
                    ),
                    Container(
                      color: Color.fromRGBO(127, 127, 127, 0.3),
                      height: 1,
                    ),
                    ListTile(
                      title: Text(
                        '라이더 인증 관리',
                        style: TextStyle(
                            fontFamily: 'NotoSans_NotoSansKR-Regular',
                            fontSize: fontSizeMainText,
                            letterSpacing: -0.5,
                            color: colorText
                        ),
                      ),
                      onTap: () {},
                      tileColor: colorDrawerListColor,
                    ),
                    Container(
                      color: Color.fromRGBO(127, 127, 127, 0.3),
                      height: 1,
                    ),
                    ListTile(
                      title: Text(
                        '앱 버전 정보',
                        style: TextStyle(
                            fontFamily: 'NotoSans_NotoSansKR-Regular',
                            fontSize: fontSizeMainText,
                            letterSpacing: -0.5,
                            color: colorText
                        ),
                      ),
                      onTap: () {},
                      tileColor: colorDrawerListColor,
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(30),
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            color: colorMainBg,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                /** 배달 완료 **/
                Container(
                  child: Text(
                    '배달이 완료되었습니다.',
                    style: TextStyle(
                        fontFamily: 'NotoSans_Bold',
                        fontSize: fontSizeMainText,
                        letterSpacing: -0.5,
                        color: colorPoint
                    ),
                  ),
                ),
                /** 배달지 주소 **/
                Container(
                  margin: EdgeInsets.only(top: 5.0),
                  child: Text(
                    _orderContents!.addressClientDo,
                    style: TextStyle(
                        fontFamily: 'NotoSans_Bold',
                        fontSize: fontSizeSuper,
                        letterSpacing: -0.5,
                        color: colorText
                    ),
                  ),
                ),
                /** 선/후 결제, 산정 배달료 감싸는 큰틀 **/
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      /** 선/후 결제 **/
                      Container(
                        margin: EdgeInsets.only(top: 50.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              child: Row(
                                children: [
                                  /** 선,후불 **/
                                  Container(
                                      padding: EdgeInsets.only(bottom: 3.0),
                                      width: 23,
                                      decoration: BoxDecoration(
                                        color: colorPoint,
                                        shape: BoxShape.circle,
                                      ),
                                      child: Column(
                                        children: [
                                          Text(
                                            _orderContents!.isPay,
                                            style: TextStyle(
                                                fontSize: fontSizeMainText,
                                                fontFamily: 'Notosans_bold',
                                                color: colorTextBright,
                                                letterSpacing: -0.5
                                            ),
                                          ),
                                        ],
                                      )
                                  ),
                                  /** 선결제 금액 **/
                                  Container(
                                    margin: EdgeInsets.fromLTRB(8, 0, 0, 3),
                                    child: Text(
                                      '${_orderContents!.isPay}결제 금액',
                                      style: TextStyle(
                                          fontFamily: 'Notosans_bold',
                                          fontSize: fontSizeMainText,
                                          color: colorText,
                                          letterSpacing: -0.5
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                            /** 결제 금액 **/
                            Container(
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  Container(
                                    child: Text(
                                      /** 1000단위 당 콤마(,) 찍기 **/
                                      formatCurrency.format(_orderContents!.priceFood),
                                      style: TextStyle(
                                        fontSize: 45,
                                        fontFamily: 'NotoSans_Bold',
                                        color: Colors.white,
                                        letterSpacing: -0.5,
                                      ),
                                    ),
                                  ),
                                  /** 결제 단위 **/
                                  Container(
                                    margin: EdgeInsets.fromLTRB(5, 0, 0, 7),
                                    child: Text(
                                      '원',
                                      style: TextStyle(
                                        fontSize: 22,
                                        fontFamily: 'NotoSans_NotoSansKR-Regular',
                                        color: colorText,
                                        letterSpacing: -0.5,
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 50.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            /** 선결제 금액 **/
                            Container(
                              margin: EdgeInsets.fromLTRB(8, 0, 0, 3),
                              child: Text(
                                '산정 배달료',
                                style: TextStyle(
                                    fontFamily: 'Notosans_bold',
                                    fontSize: fontSizeMainText,
                                    color: colorText,
                                    letterSpacing: -0.5
                                ),
                              ),
                            ),
                            Container(
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    Text(
                                      /** 1000단위 당 콤마(,) 찍기 **/
                                      formatCurrency.format(_orderContents!.feeTotal),
                                      style: TextStyle(
                                        fontSize: 45,
                                        fontFamily: 'NotoSans_Bold',
                                        color: Colors.white,
                                        letterSpacing: -0.5,
                                      ),
                                    ),
                                    /** 결제 단위 **/
                                    Container(
                                      margin: EdgeInsets.fromLTRB(5, 0, 0, 7),
                                      child: Text(
                                        '원',
                                        style: TextStyle(
                                          fontSize: 22,
                                          fontFamily: 'NotoSans_NotoSansKR-Regular',
                                          color: colorText,
                                          letterSpacing: -0.5,
                                        ),
                                      ),
                                    )
                                  ],
                                )
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                /** 요청 사항 감싸는 큰틀 **/
                Container(
                  margin: EdgeInsets.only(top: 30.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      /** 요청 사항 타이틀 **/
                      Text(
                        '요청 사항',
                        style: TextStyle(
                            fontFamily: 'Notosans_bold',
                            fontSize: fontSizeMainText,
                            color: colorText,
                            letterSpacing: -0.5
                        ),
                      ),
                      /** 고객 라이더 요청 사항 내용 **/
                      Container(
                        margin: EdgeInsets.only(top: 5.0),
                        child: Text(
                          _orderContents!.requestRider,
                          style: TextStyle(
                              fontFamily: 'NotoSans_NotoSansKR-Regular',
                              fontSize: fontSizeMainText,
                              color: Color.fromRGBO(158, 158, 158, 1),
                              letterSpacing: -0.5
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                /** 추가 결제하기 버튼 **/
                Container(
                  margin: EdgeInsets.only(top: 40.0),
                  height: 50,
                  width: MediaQuery.of(context).size.width,
                  child: ElevatedButton(
                    child: Text(
                      '추가 결제하기',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: fontSizeMainText,
                        fontFamily: 'NotoSans_Bold',
                        letterSpacing: -0.5,
                      ),
                    ),
                    style: ElevatedButton.styleFrom(
                        primary: colorButton,
                        onPrimary: Colors.grey,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(6)
                        ),
                        padding: EdgeInsets.zero,
                        elevation: 0
                    ),
                    onPressed: () {},
                  ),
                ),
                /** 페이 송금하기 버튼 **/
                Container(
                  margin: EdgeInsets.only(top: 8.0),
                  height: 50,
                  width: MediaQuery.of(context).size.width,
                  child: ElevatedButton(
                    child: Text(
                      '페이 송금하기',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: fontSizeMainText,
                        fontFamily: 'NotoSans_Bold',
                        letterSpacing: -0.5,
                      ),
                    ),
                    style: ElevatedButton.styleFrom(
                        primary: colorButton,
                        onPrimary: Colors.grey,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(6)
                        ),
                        padding: EdgeInsets.zero,
                        elevation: 0
                    ),
                    onPressed: () {},
                  ),
                ),
                /** 사진 촬영, 문자 전송 버튼 **/
                Container(
                  margin: EdgeInsets.only(top: 8.0),
                  height: 50,
                  width: MediaQuery.of(context).size.width,
                  child: ElevatedButton(
                    child: Text(
                      '완료하기',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: fontSizeMainText,
                        fontFamily: 'NotoSans_Bold',
                        letterSpacing: -0.5,
                      ),
                    ),
                    style: ElevatedButton.styleFrom(
                        primary: colorPoint,
                        onPrimary: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(6)
                        ),
                        padding: EdgeInsets.zero,
                        elevation: 0
                    ),
                    onPressed: () {
                      Navigator.of(context).pushNamed('/page_rider_main');
                    },
                  ),
                ),
                /** 연락 버튼 큰틀 **/
                Container(
                  margin: EdgeInsets.only(top: 50.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      /** 매장 전화 버튼 **/
                      Container(
                        margin: EdgeInsets.only(top: 8.0),
                        height: 50,
                        width: MediaQuery.of(context).size.width/3.8,
                        child: ElevatedButton(
                          child: Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                  margin: EdgeInsets.only(top: 3.0),
                                  child: Icon(
                                    Icons.call,
                                    color: Colors.white,
                                    size: 15,
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 5),
                                  child: Text(
                                    '매장',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: fontSizeMainText,
                                      fontFamily: 'NotoSans_Bold',
                                      letterSpacing: -0.5,
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          style: ElevatedButton.styleFrom(
                              primary: colorButton,
                              onPrimary: Colors.grey,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(6)
                              ),
                              padding: EdgeInsets.zero,
                              elevation: 0
                          ),
                          onPressed: () {},
                        ),
                      ),
                      /** 고객 전화 버튼 **/
                      Container(
                        margin: EdgeInsets.only(top: 8.0),
                        height: 50,
                        width: MediaQuery.of(context).size.width/3.8,
                        child: ElevatedButton(
                          child: Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                  margin: EdgeInsets.only(top: 3.0),
                                  child: Icon(
                                    Icons.call,
                                    color: Colors.white,
                                    size: 15,
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 5),
                                  child: Text(
                                    '고객',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: fontSizeMainText,
                                      fontFamily: 'NotoSans_Bold',
                                      letterSpacing: -0.5,
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          style: ElevatedButton.styleFrom(
                              primary: colorButton,
                              onPrimary: Colors.grey,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(6)
                              ),
                              padding: EdgeInsets.zero,
                              elevation: 0
                          ),
                          onPressed: () {},
                        ),
                      ),
                      /** 고객 문자 버튼 **/
                      Container(
                        margin: EdgeInsets.only(top: 8.0),
                        height: 50,
                        width: MediaQuery.of(context).size.width/3.5,
                        child: ElevatedButton(
                          child: Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                  margin: EdgeInsets.only(top: 4.0),
                                  child: Icon(
                                    Icons.messenger_outlined,
                                    color: Colors.white,
                                    size: 15,
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 5),
                                  child: Text(
                                    '고객 문자',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: fontSizeMainText,
                                      fontFamily: 'NotoSans_Bold',
                                      letterSpacing: -0.5,
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          style: ElevatedButton.styleFrom(
                              primary: colorButton,
                              onPrimary: Colors.grey,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(6)
                              ),
                              padding: EdgeInsets.zero,
                              elevation: 0
                          ),
                          onPressed: () {},
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      );
    }

  }
}

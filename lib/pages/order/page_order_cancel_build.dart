import 'package:flutter/material.dart';
import 'package:matddak_v1/component/component_order_cancel_item.dart';
import 'package:matddak_v1/config/config_color.dart';
import 'package:matddak_v1/model/order/cancel/order_cancel_item.dart';
import 'package:matddak_v1/repository/repo_order.dart';

class PageOrderCancelBuild extends StatefulWidget {
  const PageOrderCancelBuild({super.key});

  @override
  State<PageOrderCancelBuild> createState() => _PageOrderCancelBuildState();
}

class _PageOrderCancelBuildState extends State<PageOrderCancelBuild> {
  final _scrollController = ScrollController();

  List<OrderCancelItem> _list = [];
  int _currentPage = 1;
  int _totalPage = 1;
  int _totalItemCount = 0;

  /** 취소 리스트 연동 **/
  List<OrderCancelItem> _OrderCancelItem = [];

  Future<void> _loadList() async {
    await RepoOrder().getCancels()
        .then((res) =>{
      setState(() {
        _OrderCancelItem = res.list;
      })
    })
        .catchError((err) => {
      debugPrint(err)
    });
  }

  @override
  void initState() {
    super.initState();
    _loadList();

    _scrollController.addListener(() {
      if (_scrollController.offset == _scrollController.position.maxScrollExtent) {
        _loadItem();
      }
    });

    _loadItem();
  }

  Future<void> _loadItem({bool reFresh = false}) async {
    if (reFresh) {
      setState(() {
        _list = [];
        _currentPage = 1;
        _totalPage = 1;
        _totalItemCount = 0;
      });
    }

    if (_currentPage <= _totalPage) {
      await RepoOrder().getCancels(page: _currentPage)
          .then((res) {
        setState(() {
          _totalPage = res.totalCount.toInt();
          _list = [..._list, ...res.list!];

          _currentPage++;
        });
      }).catchError((err) {
        debugPrint(err);
      });
    }

    if (reFresh) {
      _scrollController.animateTo(
          0,
          duration: const Duration(milliseconds: 300),
          curve: Curves.easeOut
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
        // controller: _scrollController,
        itemCount: _OrderCancelItem.length,
        itemBuilder: (BuildContext ctx, int idx) {
          return Container(
            color: colorMainBg,
            child: Column(
              children: [
                ComponentOrderCancelItem(
                    orderCancelItem: _OrderCancelItem[idx]
                ),
                // _buildBody(),
              ],
            ),
          );
        },
      ),
    );
  }

  Widget _buildBody() {
    // 총 데이터 개수 => 초기값 0
    if (_totalItemCount > 0) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          ListView.builder(
            controller: _scrollController,
            //physics, shrinkWrap 짝궁
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: _list.length,
            itemBuilder: (_, index) => ComponentOrderCancelItem(
                orderCancelItem: _list[index],
            ),
          )
        ],
      );
    } else {
      return SizedBox(
        // height: MediaQuery.of(context).size.height -60,
        // child: const ComponentNoContents(icon: Icons.cancel, msg: '요청 내용이 없습니다.')
      );
    }
  }
}

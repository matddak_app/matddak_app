import 'package:flutter/material.dart';
import 'package:matddak_v1/component/component_order_request_item.dart';
import 'package:matddak_v1/model/order/request/order_request_item.dart';
import 'package:matddak_v1/repository/repo_order.dart';

class PageInfinityScroll extends StatefulWidget {
  const PageInfinityScroll({super.key});

  @override
  State<PageInfinityScroll> createState() => _PageInfinityScrollState();
}

class _PageInfinityScrollState extends State<PageInfinityScroll> {
  final _scrollController = ScrollController();

  List<OrderRequestItem> _list = [];
  int _currentPage = 1;
  int _totalPage = 1;
  int _totalItemCount = 0;

  @override
  void initState() {
    super.initState();

    _scrollController.addListener(() {
      if (_scrollController.offset == _scrollController.position.maxScrollExtent) {
       _loadItem();
      }
    });

    _loadItem();
  }

  Future<void> _loadItem({bool reFresh = false}) async {
    if (reFresh) {
      setState(() {
        _list = [];
        _currentPage = 1;
        _totalPage = 1;
        _totalItemCount = 0;
      });
    }

    if (_currentPage <= _totalPage) {
      await RepoOrder().getList(page: _currentPage)
          .then((res) {
            setState(() {
              _totalPage = res.totalCount.toInt();
              _list = [..._list, ...res.list!];

              _currentPage++;
        });
    }).catchError((err) {
      debugPrint(err);
      });
    }

    if (reFresh) {
      _scrollController.animateTo(
        0,
        duration: const Duration(milliseconds: 300),
        curve: Curves.easeOut
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        controller: _scrollController,
        children: [
          // ComponentRequestItem(
          //   orderRequestItem: _list[],
          //   callback: () {},
          // ),
          _buildBody(),
        ],
      ),
    );
  }

  Widget _buildBody() {
    if (_totalItemCount > 0) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: _list.length,
            itemBuilder: (_, index) => ComponentRequestItem(
              orderRequestItem: _list[index],
                callback: () {}
            ),
          )
        ],
      );
    } else {
      return SizedBox(
        height: MediaQuery.of(context).size.height -60,
        child: Container(
          child: Column(
            children: [
              Icon(Icons.cancel),
              Text('요청 데이터가 없습니다.')
            ],
          ),
        ),
      );
    }
  }
}

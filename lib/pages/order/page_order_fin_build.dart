import 'package:flutter/material.dart';
import 'package:matddak_v1/component/component_order_fin_item.dart';
import 'package:matddak_v1/config/config_color.dart';
import 'package:matddak_v1/model/order/fin/order_fin_item.dart';
import 'package:matddak_v1/repository/repo_order.dart';

class PageOrderFinBuild extends StatefulWidget {
  const PageOrderFinBuild({super.key});

  @override
  State<PageOrderFinBuild> createState() => _PageOrderFinBuildState();
}

class _PageOrderFinBuildState extends State<PageOrderFinBuild> {
  final _scrollController = ScrollController();

  List<OrderFinItem> _list = [];
  int _currentPage = 1;
  int _totalPage = 1;
  int _totalItemCount = 0;

  /** 완료 리스트 연동 **/
  List<OrderFinItem> _OrderFinItem = [];

  Future<void> _loadList() async {
    await RepoOrder().getFins()
        .then((res) =>{
      setState(() {
        _OrderFinItem = res.list;
      })
    })
        .catchError((err) => {
      debugPrint(err)
    });
  }

  @override
  void initState() {
    super.initState();
    _loadList();

    _scrollController.addListener(() {
      if (_scrollController.offset == _scrollController.position.maxScrollExtent) {
        _loadItem();
      }
    });

    _loadItem();
  }

  Future<void> _loadItem({bool reFresh = false}) async {
    if (reFresh) {
      setState(() {
        _list = [];
        _currentPage = 1;
        _totalPage = 1;
        _totalItemCount = 0;
      });
    }
    //
    // if (_currentPage <= _totalPage) {
    //   await RepoOrder().getFins(page: _currentPage)
    //       .then((res) {
    //     setState(() {
    //       _totalPage = res.totalCount.toInt();
    //       _list = [..._list, ...res.list!];
    //
    //       _currentPage++;
    //     });
    //   }).catchError((err) {
    //     debugPrint(err);
    //   });
    // }

    if (reFresh) {
      _scrollController.animateTo(
          0,
          duration: const Duration(milliseconds: 300),
          curve: Curves.easeOut
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
        controller: _scrollController,
          itemCount: _OrderFinItem.length,
          itemBuilder: (BuildContext ctx, int idx) {
          return Container(
            color: colorMainBg,
            child: Column(
              children: [
                ComponentOrderFinItem(
                  orderFinItem: _OrderFinItem[idx],
                ),
                _buildBody(),
              ],
            ),
          );
        }
      ),
    );
  }

  Widget _buildBody() {
    if (_totalItemCount > 0) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          ListView.builder(
            controller: _scrollController,
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: _list.length,
            itemBuilder: (_, index) => ComponentOrderFinItem(
                orderFinItem: _list[index]
            ),
          )
        ],
      );
    } else {
      return SizedBox();
    }
  }
}

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:matddak_v1/component/component_appbar.dart';
import 'package:matddak_v1/component/component_calender.dart';
import 'package:matddak_v1/component/component_out_money.dart';
import 'package:matddak_v1/config/config_color.dart';
import 'package:matddak_v1/config/config_text_size.dart';
import 'package:matddak_v1/functions/util_basic.dart';
import 'package:matddak_v1/model/payment/money_history_item.dart';
import 'package:matddak_v1/repository/repo_payment.dart';
class PageOutMoneyDetail extends StatefulWidget {
  const PageOutMoneyDetail({super.key});

  @override
  State<PageOutMoneyDetail> createState() => _PageOutMoneyDetailState();
}

class _PageOutMoneyDetailState extends State<PageOutMoneyDetail> {

  List<MoneyHistoryItem>? _list;

  Future<void> _loadList() async {
    await RepoPayment().getOutMoney()
        .then((res) => {
          setState((){
            _list = res.list;
          })
    }).catchError((err) => {
      ApiErrorHandler.handlingError(context, err)
    });
  }

  @override
  void initState() {
    super.initState();
    _loadList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /** 앱바 **/
      appBar: ComponentAppbar(
        text: '페이 출금 내역',
        page: '/page_rider_main',
      ),
      body: SingleChildScrollView(
        child: Container(
          width: MediaQuery.of(context).size.width,
          height:MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
              color: colorMainBg
          ),
          child: Column(
            children: [
              /** 캘린더 입력 **/
              // Container(
              //   child: Row(
              //     mainAxisAlignment: MainAxisAlignment.center,
              //     children: [
              //       Container(child: ComponentCalender(date: DateTime.now(), changeDate: (){})),
              //       /** 사이 물결 **/
              //       Container(
              //         margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
              //         child: Text(
              //           '~',
              //           style: TextStyle(
              //             fontFamily: 'NotoSans_NotoSansKR-SemiBold',
              //             color: colorTextBright,
              //             fontSize: fontSizeTitle,
              //           ),
              //         ),
              //       ),
              //       Container(child: ComponentCalender(date: DateTime.now(), changeDate: (){}))
              //
              //     ],
              //   ),
              // ),
              /** 날짜 / 금액 띠 **/
              Container(
                  margin: EdgeInsets.fromLTRB(0, 8, 0, 2),
                  padding: EdgeInsets.fromLTRB(16, 6, 16, 10),
                  width: MediaQuery.of(context).size.width,
                  color: colorBg,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        '출금 날짜',
                        style: TextStyle(
                            fontFamily: 'NotoSans_NotoSansKR-Regular',
                            fontSize: fontSizeMainText,
                            color: colorText,
                            letterSpacing: -0.5
                        ),
                      ),
                      Text(
                        '금액',
                        style: TextStyle(
                            fontFamily: 'NotoSans_NotoSansKR-Regular',
                            fontSize: fontSizeMainText,
                            color: colorText,
                            letterSpacing: -0.5
                        ),
                      )
                    ],
                  )
              ),
              /** 컴포넌트 시작 **/
              Expanded(
                  child:SizedBox(
                    child: new ListView.builder(
                        itemCount: _list?.length,
                        itemBuilder: (BuildContext ctx, int idx) {
                          return ComponentOutMoney(
                            moneyHistoryItem: _list![idx],
                          );
                        }
                    ),
                  )
              ),


              /*ListView.builder(
                  itemCount: _list?.length,
                  itemBuilder:(BuildContext ctx,int idx) {
                    return ComponentOutMoney(
                        moneyHistoryItem: _list![idx]);
                  }
              )*/
            ],
          ),
        ),
      ),
    );
  }
}

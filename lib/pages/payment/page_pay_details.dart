 import 'package:flutter/material.dart';
import 'package:matddak_v1/component/component_appbar.dart';
import 'package:matddak_v1/config/config_color.dart';
import 'package:matddak_v1/config/config_text_size.dart';

class PagePayDetails extends StatefulWidget {
  const PagePayDetails({super.key});

  @override
  State<PagePayDetails> createState() => _PagePayDetailsState();
}

class _PagePayDetailsState extends State<PagePayDetails> {
  DateTime date = DateTime.now(); // 선택한 날짜를 입력받을 변수 선언

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /** 앱바 **/
      appBar: ComponentAppbar(
        text: '페이 내역',
        page: '/page_pay_main',
      ),
      body: SingleChildScrollView(
        /** 전체를 감싸는 컨테이너 **/
        child: Container(
          width: MediaQuery.of(context).size.width,
          height:MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            color: colorMainBg
          ),
          child: Column(
            children: [
              /** 캘린더 **/
              Container(
                padding: EdgeInsets.fromLTRB(16, 5, 16, 0),
                child: Container(
                  // width: double.infinity,
                  width: MediaQuery.of(context).size.width,
                  height: 55,
                  decoration: BoxDecoration(
                      color: colorButton,
                      borderRadius: BorderRadius.circular(8)
                  ),
                  child: Container(
                    alignment: FractionalOffset.center,
                    margin: EdgeInsets.fromLTRB(10, 5, 0, 8),
                    child: TextButton(
                      onPressed: () async{
                        final selectedDate = await showDatePicker(
                          builder: (BuildContext context, Widget ?child) {
                            return Theme(data: ThemeData.dark(), child: child ??Text(''));
                          },
                          context: context, // 팝업으로 띄우기 때문에 context 전달
                          initialDate: date, // 달력을 띄웠을 때 선택된 날짜. 위에서 date 변수에 오늘 날짜를 넣었으므로 오늘 날짜가 선택돼서 나옴
                          firstDate: DateTime(1980), // 시작 년도
                          lastDate: DateTime.now(), // 마지막 년도. 오늘로 지정하면 미래의 날짜 선택불가
                        );
                        if (selectedDate != null) {
                          setState(() {
                            date = selectedDate; // 선택한 날짜는 dart 변수에 저장
                          });
                        }
                      }, child: Text(
                      '$date'.substring(0, 10), //출력하면 시간도 같이 출력이 되는데 날짜만 표시하고 싶어서 substring으로 잘라냄
                      style: TextStyle(
                        color: colorTextBright,
                        fontSize: fontSizeSuper,
                      ),
                    ),
                    ),
                  ),
                ),
              ),
              /** 보유 페이 **/
              Container(
                alignment: AlignmentDirectional.bottomStart,
                padding: EdgeInsets.all(16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      child: Text(
                        '보유 페이',
                        style: TextStyle(
                            fontFamily:'NotoSans_NotoSansKR-Regular',
                            fontSize: fontSizeMainText,
                            color: colorText,
                            letterSpacing: -0.5
                        ),
                      ),
                    ),
                    Container(
                      child: Text(
                        '85,600',
                        style: TextStyle(
                            fontFamily: 'NotoSans_Bold',
                            fontSize: fontSizeTitle,
                            color: colorTextBright,
                            letterSpacing: -0.5
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              /** 페이 내역 테이블 시작 **/
              Container(
                padding: EdgeInsets.fromLTRB(16 , 5, 16, 6),
                width: MediaQuery.of(context).size.width,
                height: 40,
                decoration: BoxDecoration(
                  color: colorBg,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width / 2,
                      child: Text(
                        '항목',
                        style: TextStyle(
                            fontFamily: 'NotoSans_NotoSansKR-Regular',
                            fontSize: fontSizeMainText,
                            color: colorTextBright,
                            letterSpacing: -0.5
                        ),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width / 3.69,
                      child: Text(
                        '금액',
                        style: TextStyle(
                            fontFamily: 'NotoSans_NotoSansKR-Regular',
                            fontSize: fontSizeMainText,
                            color: colorTextBright,
                            letterSpacing: -0.5
                        ),
                      ),
                    ),
                    Container(
                      child: Text(
                        '누적 페이',
                        style: TextStyle(
                            fontFamily: 'NotoSans_NotoSansKR-Regular',
                            fontSize: fontSizeMainText,
                            color: colorTextBright,
                            letterSpacing: -0.5
                        ),
                      )
                    )
                  ],
                ),
              ),
              /** 컴포넌트 시작 **/
              Container(
                margin: EdgeInsets.all(16),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    /** 산재 보험료 **/
                    Container(
                        width: MediaQuery.of(context).size.width / 2.15,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              '산재보험료 선 차감',
                              style: TextStyle(
                                  fontFamily: 'NotoSans_Bold',
                                  fontSize: fontSizeMainText,
                                  color: colorTextBright,
                                  letterSpacing: -0.5
                              ),
                            ),
                            /** 시간 **/
                            Text(
                              '2024-03-13 15:07:49',
                              style: TextStyle(
                                  fontFamily: 'NotoSans_NotoSansKR-Regular',
                                  fontSize: 15,
                                  color: colorText,
                                  letterSpacing: -0.5
                              ),
                            ),
                            /** 메모 **/
                            Text(
                              '라이더 수익 : 5600원',
                              style: TextStyle(
                                  fontFamily: 'NotoSans_NotoSansKR-Regular',
                                  fontSize: 15,
                                  color: colorText,
                                  letterSpacing: -0.5
                              ),
                            ),
                          ],
                        )
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width / 3.7,
                      child: Text(
                        '-24',
                        style: TextStyle(
                            fontFamily: 'NotoSans_Bold',
                            fontSize: fontSizeMainText,
                            color: colorTextBright,
                            letterSpacing: -0.5
                        ),
                      ),
                    ),
                    Text(
                      '85,600',
                      style: TextStyle(
                          fontFamily: 'NotoSans_Bold',
                          fontSize: fontSizeMainText,
                          color: colorTextBright,
                          letterSpacing: -0.5
                      ),
                      textAlign: TextAlign.right,
                    )
                  ],
                ),
              ),
              /** 구분선 **/
              Container(
                height: 1,
                color: Color.fromRGBO(190, 190, 190, 0.2),
              ),
              // Container(
              //   child: ListView.builder(
              //       // itemCount: _list.length,
              //       itemBuilder: (context, index){
              //         return ComponentPayDetails();
              //       }
              //   ),





            ],
          ),
        ),
      ),
    );
  }
}

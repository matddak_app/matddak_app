

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:intl/intl.dart';
import 'package:matddak_v1/component/component_appbar.dart';
import 'package:matddak_v1/config/config_color.dart';
import 'package:matddak_v1/config/config_text_size.dart';
import 'package:matddak_v1/functions/util_dialog.dart';
import 'package:matddak_v1/model/payment/money_history_create_request.dart';

import 'package:matddak_v1/model/payment/rider_pay_response.dart';
import 'package:matddak_v1/repository/repo_payment.dart';

import '../../functions/util_basic.dart';

class PageOutMoney extends StatefulWidget {
  const PageOutMoney({Key?key}):super (key: key);



  @override
  State<PageOutMoney> createState() => _PageOutMoneyState();
}
class _PageOutMoneyState extends State<PageOutMoney> {
  final formatCurrency = new NumberFormat.simpleCurrency(locale: "ko_KR", name: "", decimalDigits: 0);
  final _formKey = GlobalKey<FormBuilderState>();

//데이터 불러오는 부분
  RiderPayResponse? _response;

  Future<void> _loadDetail() async {
    await RepoPayment().getCurrentPay()
        .then((res) => {
      setState((){
        _response = res.data;
      })
    })
        .catchError((err)=>{
      debugPrint(err)
    });
  }
  //요청을 보내는 부분



  Future<void> _setOutMoney(MoneyHistoryCreateRequest request) async {
    await RepoPayment().setOutMoney(request)
        .then((res) {
      Navigator.of(context).popAndPushNamed('/page_rider_main');
    })
        .catchError((err){
      ApiErrorHandler.handlingError(context, err);
    });
  }

  @override
  void initState() {
    super.initState();
    _loadDetail();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /** 앱바 **/
        appBar: ComponentAppbar(
          text: '페이 출금하기',
          page: '/page_pay_main',
        ),
        body: SingleChildScrollView(
          /** 전체를 감싸는 컨테이너 **/
          child: Container(
            padding: EdgeInsets.fromLTRB(16, 5, 16, 0),
            width: MediaQuery.of(context).size.width,
            height:MediaQuery.of(context).size.height,
            color: colorMainBg,
            child: Column(
              children: [
                /** 메인 보더 **/
                Container(
                  padding: EdgeInsets.all(25),
                  height: 200,
                  decoration: BoxDecoration(
                      color: colorButton,
                      borderRadius: BorderRadius.circular(8)
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              '보유 페이',
                              style: TextStyle(
                                  fontFamily: 'NotoSans_NotoSansKR-Regular',
                                  fontSize: fontSizeMainText,
                                  color: colorText,
                                  letterSpacing: -0.5
                              ),
                            ),
                            Text(
                              /** 1000단위 당 콤마(,) 찍기 **/
                              '${formatCurrency.format(_response!.payNow)} 원',
                              style: TextStyle(
                                  fontFamily: 'NotoSans_Bold',
                                  fontSize: fontSizeMainText,
                                  color: colorTextBright,
                                  letterSpacing: -0.5
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              '은행명',
                              style: TextStyle(
                                  fontFamily: 'NotoSans_NotoSansKR-Regular',
                                  fontSize: fontSizeMainText,
                                  color: colorText,
                                  letterSpacing: -0.5
                              ),
                            ),
                            Text(
                              '${_response!.bankName}',
                              style: TextStyle(
                                  fontFamily: 'NotoSans_Bold',
                                  fontSize: fontSizeMainText,
                                  color: colorTextBright,
                                  letterSpacing: -0.5
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              '계좌번호',
                              style: TextStyle(
                                  fontFamily: 'NotoSans_NotoSansKR-Regular',
                                  fontSize: fontSizeMainText,
                                  color: colorText,
                                  letterSpacing: -0.5
                              ),
                            ),
                            Text(
                              '${_response!.bankNumber}',
                              style: TextStyle(
                                  fontFamily: 'NotoSans_Bold',
                                  fontSize: fontSizeMainText,
                                  color: colorTextBright,
                                  letterSpacing: -0.5
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              '예금주명',
                              style: TextStyle(
                                  fontFamily: 'NotoSans_NotoSansKR-Regular',
                                  fontSize: fontSizeMainText,
                                  color: colorText,
                                  letterSpacing: -0.5
                              ),
                            ),
                            Text(
                              '${_response!.bankOwner}',
                              style: TextStyle(
                                  fontFamily: 'NotoSans_Bold',
                                  fontSize: fontSizeMainText,
                                  color: colorTextBright,
                                  letterSpacing: -0.5
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                FormBuilder(
                  key: _formKey,
                  autovalidateMode: AutovalidateMode.disabled,
                  child: Container(
                    margin: EdgeInsets.only(top: 16),
                    padding: EdgeInsets.all(25),
                    decoration: BoxDecoration(
                        color: colorButton,
                        borderRadius: BorderRadius.circular(8)
                    ),
                    child: Column(
                      children: [
                        Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              /** 출금 금액 **/
                              Text(
                                '출금 금액',
                                style: TextStyle(
                                    fontFamily: 'NotoSans_NotoSansKR-Regular',
                                    fontSize: fontSizeMainText,
                                    color: colorText,
                                    letterSpacing: -0.5
                                ),
                              ),
                              FormBuilderTextField(
                                name: 'moneyAmount',
                                // @TODO 테스트 값
                                initialValue: '1000',
                                /** 커서 색상 **/
                                cursorColor: colorPoint,
                                /** 키보드 바깥 영역 탭할 시 키보드 닫기 **/
                                onTapOutside: (event) => FocusManager.instance.primaryFocus?.unfocus(),
                                inputFormatters: <TextInputFormatter>[
                                  FilteringTextInputFormatter.allow(RegExp(r'[0-9|ᆢ]')), /** 숫자만 입력 가능하게 하기 **/
                                ],
                                keyboardType: TextInputType.number, /** 키보드 기본 숫자 키 올라오게 하기 **/
                                decoration:InputDecoration(
                                  enabledBorder: UnderlineInputBorder( /** 기본 텍스트 필드 인풋 라인 색상 **/
                                    borderSide: BorderSide(color: colorHintText),
                                  ),
                                  focusedBorder: UnderlineInputBorder( /** 선택됐을 시 텍스트 필드 인풋 라인 색상 **/
                                    borderSide: BorderSide(color: Colors.orangeAccent),
                                  ),
                                  hintText: '출금 금액을 입력해 주세요.',
                                  hintStyle: TextStyle(
                                      fontSize: fontSizeMainText,
                                      fontFamily:'NotoSans_NotoSansKR-Regular',
                                      color: colorHintText,
                                      letterSpacing: -0.5
                                  ),
                                ),
                                style: TextStyle(
                                    fontSize: fontSizeMainText,
                                    fontFamily:'NotoSans_NotoSansKR-Regular',
                                    color: colorTextBright,
                                    letterSpacing: -0.5
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 20.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              /** 메모 **/
                              Text(
                                '메모',
                                style: TextStyle(
                                    fontFamily: 'NotoSans_NotoSansKR-Regular',
                                    fontSize: fontSizeMainText,
                                    color: colorText,
                                    letterSpacing: -0.5
                                ),
                              ),
                              FormBuilderTextField(
                                name: 'etcMemo' ,
                                /** 커서 색상 **/
                                cursorColor: colorPoint,
                                /** 키보드 바깥 영역 탭할 시 키보드 닫기 **/
                                onTapOutside: (event) => FocusManager.instance.primaryFocus?.unfocus(),
                                keyboardType: TextInputType.text, /** 키보드 기본 텍스트 키 올라오게 하기 **/
                                decoration:InputDecoration(
                                  enabledBorder: UnderlineInputBorder( /** 기본 텍스트 필드 인풋 라인 색상 **/
                                    borderSide: BorderSide(color: colorHintText),
                                  ),
                                  focusedBorder: UnderlineInputBorder( /** 선택됐을 시 텍스트 필드 인풋 라인 색상 **/
                                    borderSide: BorderSide(color: Colors.orangeAccent),
                                  ),
                                  hintText: '메모',
                                  hintStyle: TextStyle(
                                      fontSize: fontSizeMainText,
                                      fontFamily:'NotoSans_NotoSansKR-Regular',
                                      color: colorHintText,
                                      letterSpacing: -0.5
                                  ),
                                ),
                                style: TextStyle(
                                    fontSize: fontSizeMainText,
                                    fontFamily:'NotoSans_NotoSansKR-Regular',
                                    color: colorTextBright,
                                    letterSpacing: -0.5
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 45,
                  margin: EdgeInsets.only(top: 16.0),
                  child:ElevatedButton(
                    child: Text(
                      '출금하기',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: fontSizeMainText,
                        fontFamily: 'NotoSans_NotoSansKR-Regular',
                        letterSpacing: -0.5,
                      ),
                    ),
                    style: ElevatedButton.styleFrom(
                      foregroundColor: Colors.white,
                      backgroundColor: colorPoint,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(6)
                      ),
                      padding: EdgeInsets.zero,
                      elevation: 0,
                    ),
                    onPressed: (){
                      /** 출금하기 버튼 탭 했을 때 팝업 **/
                      showDialog(
                          context: context,
                          barrierDismissible: true, /** 바깥 영역 터치시 닫을지 여부 **/
                          builder: (BuildContext context){
                            return AlertDialog(
                              surfaceTintColor: Colors.grey,
                              backgroundColor: colorButton,
                              content: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Container(
                                    margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                                    child: Text(
                                      '출금하시겠습니까?',
                                      style: TextStyle(
                                        fontFamily: 'NotoSans_Bold',
                                        fontSize: fontSizeSuper,
                                        letterSpacing: -0.5,
                                        color: colorTextBright,
                                      ),
                                    ),
                                  ),
                                  Text(
                                    textAlign: TextAlign.center,
                                    '현재 등록된 계좌번호로 출금됩니다.',
                                    style: TextStyle(
                                      fontFamily: 'NotoSans_NotoSansKR-Light',
                                      fontSize: fontSizeMainText,
                                      letterSpacing: -0.5,
                                      color: colorTextBright,
                                    ),
                                  ),
                                ],
                              ),
                              /** 취소, 확인 버튼 **/
                              actions: [
                                TextButton(
                                  child: const Text(
                                    '취소',
                                    style: TextStyle(
                                      fontFamily: 'NotoSans_NotoSansKR-Light',
                                      fontSize: fontSizeMainText,
                                      letterSpacing: -0.5,
                                      color: colorTextBright,
                                    ),
                                  ),
                                  style: TextButton.styleFrom(
                                    foregroundColor: Colors.white,
                                  ),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                ),
                                TextButton(
                                  child: const Text(
                                    '확인',
                                    style: TextStyle(
                                      fontFamily: 'NotoSans_NotoSansKR-Light',
                                      fontSize: fontSizeMainText,
                                      letterSpacing: -0.5,
                                      color: colorTextBright,
                                    ),
                                  ),
                                  style: TextButton.styleFrom(
                                    foregroundColor: Colors.white,
                                  ),
                                  onPressed: () {
                                    if(_formKey.currentState!.saveAndValidate()){

                                      MoneyHistoryCreateRequest request = MoneyHistoryCreateRequest(
                                        num.parse(_formKey.currentState!.fields['moneyAmount']!.value),
                                        StringUtils.defaultString(_formKey.currentState?.fields['etcMemo']?.value),
                                      );

                                      _setOutMoney(request);

                                      Navigator.pop(context);
                                    }
                                  },
                                ),
                              ],
                            );
                          }
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
        )
    );
  }
}

import 'package:flutter/material.dart';
import 'package:matddak_v1/component/component_notice_list.dart';
import 'package:matddak_v1/config/config_color.dart';
import 'package:matddak_v1/config/config_text_size.dart';
import 'package:matddak_v1/model/notice/notice_item.dart';
import 'package:matddak_v1/pages/payment/page_notice_detail.dart';
import 'package:matddak_v1/repository/repo_notice.dart';

class PageNoticeList extends StatefulWidget {
  const PageNoticeList({super.key});

  @override
  State<PageNoticeList> createState() => _PageNoticeListState();
}

class _PageNoticeListState extends State<PageNoticeList> {

  List<NoticeItem> _list = [
    // NoticeItem(1, '서비스 긴급 점검 안내', '2024-03-13'),
  ];

  Future<void> _loadList() async {
    await RepoNotice().getNoticeList()
        .then((res) => {
          setState((){
            print("setState");
            print(res);
            _list =res.list;
          })
        })
        .catchError((err) => {
          debugPrint(err)
        });
  }


  @override
  void initState() {
    super.initState();
    _loadList();
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          leading: IconButton(
            onPressed:(){
              Navigator.of(context).pushNamed("/page_rider_main");
            },
            icon:Icon(Icons.keyboard_backspace,color: Colors.white,),
          ),
          backgroundColor: colorMainBg ,
          title: Text(
            "공지사항",
            style: TextStyle(
                fontFamily: 'NotoSans_Bold',
                fontSize: fontSizeSuper,
                color: Colors.white,
                letterSpacing: -0.5
            ),
            ),
          centerTitle: true,
      ),
      body: Ink(
        color: colorMainBg,
        child: ListView.builder(
          itemCount: _list.length,
          itemBuilder: (BuildContext ctx,int idx) {
            return ComponentNoticeList(
                noticeItem: _list[idx],
                callback: () {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder:(context) =>PageNoticeDetail(id: _list[idx].id)));
                }
            );
          },
        ),
      )
      // body: SingleChildScrollView(
      //   child: Container(
      //     height: MediaQuery.of(context).size.height,
      //     color: colorMainBg,
      //     child: Column(
      //       children: [
      //         ComponentNoticeList(noticeItem:_list),
      //
      //       ],
      //     ),
      //   ),
      // ),
    );
  }
}

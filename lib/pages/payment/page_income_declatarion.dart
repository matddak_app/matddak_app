import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:matddak_v1/component/component_appbar.dart';
import 'package:matddak_v1/config/config_color.dart';
import 'package:matddak_v1/config/config_text_size.dart';
class PageIncomeDeclatarion extends StatefulWidget {
  const PageIncomeDeclatarion({super.key});

  @override
  State<PageIncomeDeclatarion> createState() => _PageIncomeDeclatarionState();
}

class _PageIncomeDeclatarionState extends State<PageIncomeDeclatarion> {
  DateTime date = DateTime.now(); // 선택한 날짜를 입력받을 변수 선언

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /** 앱바 **/
      appBar: ComponentAppbar(
        text: '월 단위 소득 신고 내역',
        page: '/page_pay_main',
      ),
      body: SingleChildScrollView(
        /** 전체를 감싸는 컨테이너 **/
        child: Container(
          width: MediaQuery.of(context).size.width,
          height:MediaQuery.of(context).size.height,
            color: colorMainBg,
          child: Column(
            children: [
              /** 캘린더 **/
              Container(
                padding: EdgeInsets.fromLTRB(16, 5, 16, 0),
                child: Container(
                  // width: double.infinity,
                  width: MediaQuery.of(context).size.width,
                  height: 55,
                  decoration: BoxDecoration(
                      color: colorButton,
                      borderRadius: BorderRadius.circular(8)
                  ),
                  child: Container(
                    alignment: FractionalOffset.center,
                    margin: EdgeInsets.fromLTRB(10, 5, 0, 6),
                    child: TextButton(
                      onPressed: () async{
                        final selectedDate = await showDatePicker(
                          builder: (BuildContext context, Widget ?child) {
                            return Theme(data: ThemeData.dark(), child: child ??Text(''));
                          },
                          context: context, // 팝업으로 띄우기 때문에 context 전달
                          initialDate: date, // 달력을 띄웠을 때 선택된 날짜. 위에서 date 변수에 오늘 날짜를 넣었으므로 오늘 날짜가 선택돼서 나옴
                          firstDate: DateTime(1980), // 시작 년도
                          lastDate: DateTime.now(), // 마지막 년도. 오늘로 지정하면 미래의 날짜 선택불가
                        );
                        if (selectedDate != null) {
                          setState(() {
                            date = selectedDate; // 선택한 날짜는 dart 변수에 저장
                          });
                        }
                      }, child: Text(
                      '$date'.substring(0, 7), //출력하면 시간도 같이 출력이 되는데 날짜만 표시하고 싶어서 substring으로 잘라냄   (글자수)
                      style: TextStyle(
                        color: colorTextBright,
                        fontSize: fontSizeSuper,
                      ),
                    ),
                    ),
                  ),
                ),
              ),
              /** 소득 신고 내역 **/
              Container(
                alignment: AlignmentDirectional.bottomStart,
                padding: EdgeInsets.all(16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      child: Text(
                        '소득 신고 내역',
                        style: TextStyle(
                            fontFamily:'NotoSans_NotoSansKR-Regular',
                            fontSize: fontSizeMainText,
                            color: colorText,
                            letterSpacing: -0.5
                        ),
                      ),
                    ),
                    Container(
                      child: Text(
                        '0',
                        style: TextStyle(
                            fontFamily: 'NotoSans_Bold',
                            fontSize: fontSizeTitle,
                            color: colorTextBright,
                            letterSpacing: -0.5
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              /** 주민등록번호 입력창 **/
              Container(
                width: double.infinity,
                margin: EdgeInsets.only(left: 16,right: 16),
                padding: EdgeInsets.all(16),
                decoration: BoxDecoration(
                  color: Color.fromRGBO(60, 60, 60, 1),
                  borderRadius: BorderRadius.circular(8)
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    /** 주민등록번호 입력 폼 전체 **/
                    Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            '주민등록번호',
                            style: TextStyle(
                                fontFamily: 'NotoSans_NotoSansKR-Regular',
                                fontSize: fontSizeMainText,
                                color: colorText,
                                letterSpacing: -0.5
                            ),
                          ),
                          TextField(
                            /** 커서 색상 **/
                            cursorColor: colorPoint,
                            /** 키보드 바깥 영역 탭할 시 키보드 닫기 **/
                            onTapOutside: (event) => FocusManager.instance.primaryFocus?.unfocus(),
                            keyboardType: TextInputType.number, /** 키보드 기본 숫자키 올라오게 하기 **/
                            inputFormatters: [
                              FilteringTextInputFormatter.digitsOnly, /** 숫자만 입력 받고 싶을 때 **/
                            ],
                            decoration:InputDecoration(
                              enabledBorder: UnderlineInputBorder( /** 기본 텍스트 필드 인풋 라인 색상 **/
                                borderSide: BorderSide(color: colorHintText),
                              ),
                              focusedBorder: UnderlineInputBorder( /** 선택됐을 시 텍스트 필드 인풋 라인 색상 **/
                                borderSide: BorderSide(color: Colors.orangeAccent),
                              ),
                              hintText: '주민 번호 13자리를 입력해 주세요',
                              hintStyle: TextStyle(
                                  fontSize: fontSizeMainText,
                                  fontFamily:'NotoSans_NotoSansKR-Regular',
                                  color: colorHintText,
                                  letterSpacing: -0.5
                              ),
                            ),
                            style: TextStyle(
                                fontSize: fontSizeMainText,
                                fontFamily:'NotoSans_NotoSansKR-Regular',
                                color: colorTextBright,
                                letterSpacing: -0.5
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              /**조회 버튼**/
              Container(
                width: MediaQuery.of(context).size.width,
                height: 45,
                margin: EdgeInsets.fromLTRB(16, 16, 16, 0),
                child:ElevatedButton(
                  child: Text(
                      '조회하기',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: fontSizeMainText,
                      fontFamily: 'NotoSans_NotoSansKR-Regular',
                      letterSpacing: -0.5,
                    ),
                  ),
                  style: ElevatedButton.styleFrom(
                      foregroundColor: Colors.white,
                      backgroundColor: colorPoint,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(6)
                      ),
                    padding: EdgeInsets.zero,
                    elevation: 0,
                  ),
                  onPressed: (){},
                ),
              ),
            ],
          ),
        ),
    ),
    );
  }
}

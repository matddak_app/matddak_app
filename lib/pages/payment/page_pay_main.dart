import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:matddak_v1/component/component_appbar.dart';
import 'package:matddak_v1/config/config_color.dart';
import 'package:matddak_v1/config/config_text_size.dart';
import 'package:matddak_v1/model/payment/income_statistics_response.dart';
import 'package:matddak_v1/repository/repo_payment.dart';


class PagePayMain extends StatefulWidget {
  const PagePayMain({super.key});

  @override
  State<PagePayMain> createState() => _PagePayMainState();
}

class _PagePayMainState extends State<PagePayMain> {


  IncomeStatisticsResponse? _response;

  Future<void> _loadDetail() async {
    await RepoPayment().getDateFee()
        .then((res) => {
          setState((){
            _response = res.data;
          })
        })
        .catchError((err) => {
          debugPrint(err)
        });
  }

  @override
  void initState() {
    super.initState();
    _loadDetail();
  }

  DateTime date = DateTime.now(); // 선택한 날짜를 입력받을 변수 선언

  /** 달력 테마 다크로 적용 **/
  Widget bulid(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(brightness: Brightness.dark),
    );
  }

  @override
  Widget build(BuildContext context) {
    final formatCurrency = new NumberFormat.simpleCurrency(locale: "ko_KR", name: "", decimalDigits: 0);

    return Scaffold(
      /** 앱바 **/
      appBar: ComponentAppbar(
        text: '당일 수입내역',
        page: '/page_rider_main',
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        /** 전체 컨테이너 **/
        child: Container(
          padding: EdgeInsets.fromLTRB(16, 5, 16, 16),
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          color: colorMainBg,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              /** 캘린더 시작 날짜 **/
              Container(
                width: MediaQuery.of(context).size.width / 1.03,
                height: 55,
                decoration: BoxDecoration(
                    color: colorButton,
                    borderRadius: BorderRadius.circular(5)
                ),
                child: Container(
                  alignment: FractionalOffset.center,
                  margin: EdgeInsets.fromLTRB(10, 5, 0, 10),
                  child: TextButton(
                    onPressed: () async{
                      final selectedDate = await showDatePicker(
                        builder: (BuildContext context, Widget ?child) {
                          return Theme(data: ThemeData.dark(), child: child ??Text(''));
                        },
                        context: context, // 팝업으로 띄우기 때문에 context 전달
                        initialDate: date, // 달력을 띄웠을 때 선택된 날짜. 위에서 date 변수에 오늘 날짜를 넣었으므로 오늘 날짜가 선택돼서 나옴
                        firstDate: DateTime(1980), // 시작 년도
                        lastDate: DateTime.now(), // 마지막 년도. 오늘로 지정하면 미래의 날짜 선택불가
                      );
                      if (selectedDate != null) {
                        setState(() {
                          date = selectedDate; // 선택한 날짜는 dart 변수에 저장
                        });
                      }
                    },
                    child: Text(
                      '$date'.substring(0, 10), //출력하면 시간도 같이 출력이 되는데 날짜만 표시하고 싶어서 substring으로 잘라냄
                      style: TextStyle(
                        fontFamily: 'NotoSans_Bold',
                        color: colorTextBright,
                        fontSize: fontSizeSuper,
                      ),
                    ),
                  ),
                ),
              ),
              /** 메인 보더 **/
              Container(
                margin: EdgeInsets.only(top: 10.0),
                // width: double.infinity,
                height: 330,
                padding: EdgeInsets.all(25),
                decoration: BoxDecoration(
                color: colorButton,
                borderRadius: BorderRadius.circular(5)
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    /** 완료건수 **/
                    Container(
                      child:Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            '완료건수',
                            style: TextStyle(
                                fontFamily: 'NotoSans_NotoSansKR-Regular',
                                fontSize: fontSizeMainText,
                                color: colorText,
                                letterSpacing: -0.5
                            ),
                          ),
                          Text(
                            '${_response!.totalCount} 건',
                            style: TextStyle(
                                fontFamily: 'NotoSans_Bold',
                                fontSize: fontSizeMainText,
                                color: colorTextBright,
                                letterSpacing: -0.5
                            ),
                          ),
                        ],
                      ),
                    ),
                    /** 배달료 **/
                    Container(
                      child:Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            '배달료',
                            style: TextStyle(
                                fontFamily: 'NotoSans_NotoSansKR-Regular',
                                fontSize: fontSizeMainText,
                                color: colorText,
                                letterSpacing: -0.5
                            ),
                          ),
                          Text(
                            '${formatCurrency.format(_response!.totalFee)} 원',
                            style: TextStyle(
                                fontFamily: 'NotoSans_Bold',
                                fontSize: fontSizeMainText,
                                color: colorTextBright,
                                letterSpacing: -0.5
                            ),
                          ),
                        ],
                      ),
                    ),
                    /** 수수료 **/
                    Container(
                      child:Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            '수수료',
                            style: TextStyle(
                                fontFamily: 'NotoSans_NotoSansKR-Regular',
                                fontSize: fontSizeMainText,
                                color: colorText,
                                letterSpacing: -0.5
                            ),
                          ),
                          Text(
                            '${formatCurrency.format(_response!.totalAdmin)} 원',
                            style: TextStyle(
                                fontFamily: 'NotoSans_Bold',
                                fontSize: fontSizeMainText,
                                color: colorTextBright,
                                letterSpacing: -0.5
                            ),
                          ),
                        ],
                      ),
                    ),
                    /** 라이더 수 **/
                    Container(
                      child:Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            '라이더 수수료',
                            style: TextStyle(
                                fontFamily: 'NotoSans_NotoSansKR-Regular',
                                fontSize: fontSizeMainText,
                                color: colorText,
                                letterSpacing: -0.5
                            ),
                          ),
                          Text(
                            '${formatCurrency.format(_response!.totalRider)} 원',
                            style: TextStyle(
                                fontFamily: 'NotoSans_Bold',
                                fontSize: fontSizeMainText,
                                color: colorTextBright,
                                letterSpacing: -0.5
                            ),
                          ),
                        ],
                      ),
                    ),
                    /** 총 배달 거리 **/
                    Container(
                      child:Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            '총 배달거리',
                            style: TextStyle(
                                fontFamily: 'NotoSans_NotoSansKR-Regular',
                                fontSize: fontSizeMainText,
                                color: colorText,
                                letterSpacing: -0.5
                            ),
                          ),
                          Text(
                            '${_response!.totalDistance} Km',
                            style: TextStyle(
                                fontFamily: 'NotoSans_Bold',
                                fontSize: fontSizeMainText,
                                color: colorTextBright,
                                letterSpacing: -0.5
                            ),
                          ),
                        ],
                      ),
                    ),
                    /** 평균 배달 거리 **/
                    Container(
                      child:Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            '평균 배달거리',
                            style: TextStyle(
                                fontFamily: 'NotoSans_NotoSansKR-Regular',
                                fontSize: fontSizeMainText,
                                color: colorText,
                                letterSpacing: -0.5
                            ),
                          ),
                          Text(
                            '${_response!.perDistance} Km',
                            style: TextStyle(
                                fontFamily: 'NotoSans_Bold',
                                fontSize: fontSizeMainText,
                                color: colorTextBright,
                                letterSpacing: -0.5
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              /** 상세페이지 이동 버튼 **/
              // Container(
              //   margin: EdgeInsets.only(top: 16.0),
              //   decoration: BoxDecoration(
              //     color: colorButton,
              //     borderRadius: BorderRadius.circular(8)
              //   ),
              //   child: Column(
              //     crossAxisAlignment: CrossAxisAlignment.start,
              //     children: [
              //       /** 페이 내역 **/
              //       Container(
              //         height: 50,
              //         padding: EdgeInsets.only(left: 25, top: 15),
              //         child: GestureDetector(
              //           onTap: (){
              //             Navigator.of(context).pushNamed("/page_pay_details");
              //           },
              //           behavior: HitTestBehavior.translucent,
              //           child: Text(
              //               '페이 내역',
              //               style: TextStyle(
              //                   fontFamily: 'NotoSans_NotoSansKR-Regular',
              //                   fontSize: fontSizeMainText,
              //                   color: colorText,
              //                   letterSpacing: -0.5
              //               ),
              //           ),
              //         ),
              //       ),
              //       /** 구분선 **/
              //       Container(
              //         height: 1,
              //         color: Color.fromRGBO(190, 190, 190, 0.2),
              //       ),
              //       /** 배송료 수익 내역 **/
              //       Container(
              //         height: 50,
              //         padding: EdgeInsets.only(left: 25,top: 15),
              //         child: GestureDetector(
              //           onTap: (){
              //             Navigator.of(context).pushNamed("/page_delivery_fee");
              //           },
              //           behavior: HitTestBehavior.translucent,
              //           child: Text(
              //               '배송료 수익 내역',
              //               style: TextStyle(
              //                   fontFamily: 'NotoSans_NotoSansKR-Regular',
              //                   fontSize: fontSizeMainText,
              //                   color: colorText,
              //                   letterSpacing: -0.5
              //               ),
              //           ),
              //         ),
              //       ),
              //       /** 구분선 **/
              //       Container(
              //         height: 1,
              //         color: Color.fromRGBO(190, 190, 190, 0.2),
              //       ),
              //       /** 페이 출금하기 **/
              //       Container(
              //         height: 50,
              //         padding: EdgeInsets.only(left: 25,top: 15),
              //         child: GestureDetector(
              //           onTap: (){
              //             Navigator.of(context).pushNamed("/page_out_money");
              //           },
              //           behavior: HitTestBehavior.translucent,
              //           child: Text(
              //               '페이 출금하기',
              //               style: TextStyle(
              //                   fontFamily: 'NotoSans_NotoSansKR-Regular',
              //                   fontSize: fontSizeMainText,
              //                   color: colorText,
              //                   letterSpacing: -0.5
              //               ),
              //           ),
              //         ),
              //       ),
              //       /** 구분선 **/
              //       Container(
              //         height: 1,
              //         color: Color.fromRGBO(190, 190, 190, 0.2),
              //       ),
              //       /** 월단위 소득신고 내역 **/
              //       Container(
              //         height: 50,
              //         padding: EdgeInsets.only(left: 25,top: 15),
              //         child: GestureDetector(
              //           onTap: (){
              //             Navigator.of(context).pushNamed("/page_income_declatarion");
              //           },
              //           behavior: HitTestBehavior.translucent,
              //           child: Text(
              //               '월단위 소득신고 내역',
              //               style: TextStyle(
              //                   fontFamily: 'NotoSans_NotoSansKR-Regular',
              //                   fontSize: fontSizeMainText,
              //                   color: colorText,
              //                   letterSpacing: -0.5
              //               ),
              //           ),
              //         ),
              //       ),
              //       /** 구분선 **/
              //       Container(
              //         height: 1,
              //         color: Color.fromRGBO(190, 190, 190, 0.2),
              //       ),
              //       /** 페이 출근 내역 **/
              //       Container(
              //         height: 50,
              //         padding: EdgeInsets.only(left: 25, top: 15),
              //         margin: EdgeInsets.only(bottom: 3.0),
              //         child: GestureDetector(
              //           onTap: (){
              //             Navigator.of(context).pushNamed( "/page_out_money_detail");
              //           },
              //           behavior: HitTestBehavior.translucent,
              //           child: Text(
              //               '페이 출금 내역',
              //               style: TextStyle(
              //                   fontFamily: 'NotoSans_NotoSansKR-Regular',
              //                   fontSize: fontSizeMainText,
              //                   color: colorText,
              //                   letterSpacing: -0.5
              //               ),
              //           ),
              //         ),
              //       )
              //     ],
              //   ),
              // ),
            ],
          ),
        ),
      ),
    );
  }
}

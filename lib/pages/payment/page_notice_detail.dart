import 'package:flutter/material.dart';
import 'package:matddak_v1/component/component_spinkit.dart';
import 'package:matddak_v1/config/config_color.dart';
import 'package:matddak_v1/config/config_text_size.dart';
import 'package:matddak_v1/model/notice/notice_response.dart';
import 'package:matddak_v1/repository/repo_notice.dart';

class PageNoticeDetail extends StatefulWidget {

  const PageNoticeDetail({
    super.key,
    required this.id,

  });

  final num id;

  @override
  State<PageNoticeDetail> createState() => _PageNoticeDetailState();

}

class _PageNoticeDetailState extends State<PageNoticeDetail> {

  NoticeResponse? _response;

  Future<void> _loadDetail() async {

    await RepoNotice().getNotice(widget.id)
        .then((res) => {
          setState((){
            _response = res.data;
          })
        })
        .catchError((err) => {
          debugPrint(err)
    });
  }

  @override
  void initState() {
    super.initState();
    _loadDetail();
  }




  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /** 앱바 **/
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).pushNamed("/page_notice_list");
          },
          icon: Icon(
            Icons.keyboard_backspace,
            color: Colors.white,
          ),
        ),
        backgroundColor: colorMainBg,
        title: Text(
          '공지사항',
          style: TextStyle(
              fontFamily: 'NotoSans_Bold',
              fontSize: fontSizeSuper,
              color: Colors.white,
              letterSpacing: -0.5
          ),
        ),
        centerTitle: true,
      ),
      body: _response == null ? CompontentSpinkit()
          :SingleChildScrollView(
        /** 전체를 감싸는 틀 **/
        child: Container(
          height: MediaQuery
              .of(context)
              .size
              .height,
          color: colorMainBg,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              /** 구분선 **/
              Container(
                margin: EdgeInsets.fromLTRB(0, 5, 0, 5),
                height: 1,
                color: colorButton,
              ),
              Container(
                margin: EdgeInsets.fromLTRB(16, 8, 0, 0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      '${_response!.title}',
                      style: TextStyle(
                          fontFamily: 'NotoSans_Bold',
                          fontSize: fontSizeSuper,
                          color: colorTextBright,
                          letterSpacing: -0.5
                      ),
                    ),
                    Text(
                      '${_response!.dateCreate}',
                      style: TextStyle(
                          fontFamily: 'NotoSans_NotoSansKR-Light',
                          fontSize: fontSizeMainText,
                          color: colorHintText,
                          letterSpacing: -0.5
                      ),
                    ),
                  ],
                ),
              ),
              /** 구분선 **/
              Container(
                margin: EdgeInsets.fromLTRB(0, 15, 0, 5),
                height: 1,
                color: colorButton,
              ),
              Container(
                margin: EdgeInsets.fromLTRB(16, 8, 16, 0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      '${_response!.text}',
                      style: TextStyle(
                          fontFamily: 'NotoSans_NotoSansKR-Regular',
                          fontSize: fontSizeSuper,
                          color: colorText,
                          letterSpacing: -0.5
                      ),
                    ),
                    Container(
                      width: MediaQuery
                          .of(context)
                          .size
                          .width,
                      height: 300,
                      margin: EdgeInsets.only(top: 30.0),
                      color: colorButton,
                    ),

                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

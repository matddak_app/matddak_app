import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:matddak_v1/component/component_appbar.dart';
import 'package:matddak_v1/component/component_calender.dart';
import 'package:matddak_v1/component/component_pay_list.dart';
import 'package:matddak_v1/config/config_color.dart';
import 'package:matddak_v1/config/config_text_size.dart';
import 'package:matddak_v1/functions/util_basic.dart';
import 'package:matddak_v1/model/payment/income_statistics_item.dart';
import 'package:matddak_v1/model/payment/income_statistics_response.dart';
import 'package:matddak_v1/repository/repo_payment.dart';


class PageDeliveryFee extends StatefulWidget {
  const PageDeliveryFee({super.key});

  @override
  State<PageDeliveryFee> createState() => _PageDeliveryFeeState();
}

class _PageDeliveryFeeState extends State<PageDeliveryFee> {

  IncomeStatisticsResponse? _response;
  List<IncomeStatisticsItem>? _list = [];



  Future<void> _loadDetail(String type) async {
    switch( type ){
      case 'W':
        return _loadWeekFee();
      case 'M':
        return _loadMonthFee();
        break;
      case 'D':_loadDateFee();
        break;
      default:
        throw Exception("미구현");
    }
  }
  Future<void> _loadList(String type) async {
    switch( type ){
      case 'W':
        return _loadWeekList();
      case 'M':
        return _loadMonthList();
        break;
      case 'D':_loadDateList();
      break;
      default:
        throw Exception("미구현");
    }
  }

  /**총 수익 주별**/
  Future<void> _loadWeekFee() async {
    await RepoPayment().getWeekFee()
        .then((res) => {
      setState((){
        _response = res.data;
      })
    }).catchError((err) => {
      ApiErrorHandler.handlingError(context, err)
    });
  }

  /**총 수익 월별**/
  Future<void> _loadMonthFee() async {
    await RepoPayment().getMonthFee()
        .then((res) => {
      setState((){
        _response = res.data;
      })
    }).catchError((err) => {
      ApiErrorHandler.handlingError(context, err)
    });
  }


  /**총 수익 일자별**/
  Future<void> _loadDateFee() async {
    await RepoPayment().getDateFee()
        .then((res) => {
      setState((){
        _response = res.data;
      })
    }).catchError((err) => {
      ApiErrorHandler.handlingError(context, err)
    });
  }


  /**주별 조회 api**/
  Future<void> _loadWeekList() async {
    await RepoPayment().getWeekBundleFee()
        .then((res) => {
      setState((){
        _list = res.list;
      })
    }).catchError((err) => {
      ApiErrorHandler.handlingError(context, err)
    });
  }

  /**월별 조회 api**/
  Future<void> _loadMonthList() async {
    await RepoPayment().getMonthBundleFee()
        .then((res) => {
      setState((){
        _list = res.list;
      })
    }).catchError((err) => {
      ApiErrorHandler.handlingError(context, err)
    });
  }

  /**일자별 조회 api**/
  Future<void> _loadDateList() async {
    await RepoPayment().getDateBundleFee()
        .then((res) => {
      setState((){
        _list = res.list;
      })
    }).catchError((err) => {
      ApiErrorHandler.handlingError(context, err)
    });
  }

  changeStartDate(DateTime date){
    setState(() {
      startDate = date;
    });
  }

  changeEndDate(DateTime date){
    setState(() {
      endDate = date;
    });
  }

  @override
  void initState() {
    super.initState();
    //_loadMonthList();
    //_loadDetail();
  }
  
  DateTime startDate = DateTime.now().subtract(const Duration(days:7)); // 선택한 날짜를 입력받을 변수 선언
  DateTime endDate = DateTime.now(); // 선택한 날짜를 입력받을 변수 선언

  @override
  Widget bulid(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(brightness: Brightness.dark),
    );

  }

  @override
  Widget build(BuildContext context) {
    final formatCurrency = new NumberFormat.simpleCurrency(locale: "ko_KR", name: "", decimalDigits: 0);

    return Scaffold(
      /** 앱바 **/
      appBar: ComponentAppbar(
        text: '배송료 수익 내역',
        page: '/page_rider_main',
      ),
      body: SingleChildScrollView(
        /** 전체를 감싸는 컨테이너 **/
        child: Container(
          width: MediaQuery.of(context).size.width,
          height:MediaQuery.of(context).size.height,
          color: colorMainBg,
          child: Column(
            children: [
              /** 총 수익 부분**/
              Container(
                alignment: AlignmentDirectional.bottomStart,
                padding: EdgeInsets.fromLTRB(16, 5, 16, 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      child: Text(
                        '총 수익',
                        style: TextStyle(
                            fontFamily:'NotoSans_NotoSansKR-Regular',
                            fontSize: fontSizeMainText,
                            color: colorText,
                            letterSpacing: -0.5
                        ),
                      ),
                    ),
                    Container(
                      child: Text(
                        /** 1000단위 당 콤마(,) 찍기 **/
                        '${formatCurrency.format(_response?.totalFee ?? 0)} 원',
                        style: TextStyle(
                            fontFamily: 'NotoSans_Bold',
                            fontSize: fontSizeTitle,
                            color: colorTextBright,
                            letterSpacing: -0.5
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              /** 캘린더 입력 **/
              Container(
                margin: EdgeInsets.only(bottom: 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      child: ComponentCalender(date: startDate, changeDate: changeStartDate),
                    ),
                    /** 사이 물결 **/
                    Container(
                      margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child: Text(
                        '~',
                        style: TextStyle(
                          fontFamily: 'NotoSans_NotoSansKR-SemiBold',
                          color: colorTextBright,
                          fontSize: fontSizeTitle,
                        ),
                      ),
                    ),
                    Container(
                      child: ComponentCalender(date: endDate, changeDate: changeEndDate),
                    )
                  ],
                ),
              ),
              /** 조회 버튼 큰틀 **/
              Container(
                padding: EdgeInsets.fromLTRB(16, 0, 16, 11),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    /** 주별 조회 버튼 **/
                    Container(
                      margin: EdgeInsets.only(top: 8.0),
                      height: 50,
                      width: MediaQuery.of(context).size.width / 3.5,
                      child: ElevatedButton(
                        child: Container(
                          margin: EdgeInsets.only(left: 5),
                          child: Text(
                            '주별 조회',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: fontSizeMainText,
                              fontFamily: 'NotoSans_Bold',
                              letterSpacing: -0.5,
                            ),
                          ),
                        ),
                        style: ElevatedButton.styleFrom(
                            backgroundColor: colorButton,
                            foregroundColor: Colors.grey,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(6)
                            ),
                            padding: EdgeInsets.zero,
                            elevation: 0
                        ),
                        onPressed: () {
                          _loadDetail('W');
                          _loadList('W');
                        },
                      ),
                    ),
                    /** 월별 조회 버튼 **/
                    Container(
                      margin: EdgeInsets.only(top: 8.0),
                      height: 50,
                      width: MediaQuery.of(context).size.width / 3.5,
                      child: ElevatedButton(
                        child: Container(
                          margin: EdgeInsets.only(left: 5),
                          child: Text(
                            '월별 조회',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: fontSizeMainText,
                              fontFamily: 'NotoSans_Bold',
                              letterSpacing: -0.5,
                            ),
                          ),
                        ),
                        style: ElevatedButton.styleFrom(
                            backgroundColor: colorButton,
                            foregroundColor: Colors.grey,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(6)
                            ),
                            padding: EdgeInsets.zero,
                            elevation: 0
                        ),
                        onPressed: () {
                          _loadDetail('M');
                          _loadList('M');
                        },
                      ),
                    ),
                    /** 기간별 조회 버튼 **/
                    Container(
                      margin: EdgeInsets.only(top: 8.0),
                      height: 50,
                      width: MediaQuery.of(context).size.width / 3.5,
                      child: ElevatedButton(
                        child: Container(
                          margin: EdgeInsets.only(left: 5),
                          child: Text(
                            '기간별 조회',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: fontSizeMainText,
                              fontFamily: 'NotoSans_Bold',
                              letterSpacing: -0.5,
                            ),
                          ),
                        ),
                        style: ElevatedButton.styleFrom(
                            backgroundColor: colorButton,
                            foregroundColor: Colors.grey,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(6)
                            ),
                            padding: EdgeInsets.zero,
                            elevation: 0
                        ),
                        onPressed: () {
                          final param1 = new DateFormat("yyyy-MM-dd").format(startDate);
                          final param2 = new DateFormat("yyyy-MM-dd").format(endDate);

                          print('시작일 ${param1}');
                          print('종료일 ${param2}');

                        },
                      ),
                    ),
                  ],
                ),
              ),
              /** 일자별 수입 리스트 **/
              Container(
                padding: EdgeInsets.fromLTRB(16 , 5, 16, 8),
                width: MediaQuery.of(context).size.width,
                height: 40,
                decoration: BoxDecoration(
                  color: colorBg,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width / 2.25,
                      child: Text(
                        '날짜',
                        style: TextStyle(
                            fontFamily: 'NotoSans_NotoSansKR-Regular',
                            fontSize: fontSizeMainText,
                            color: colorTextBright,
                            letterSpacing: -0.5
                        ),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width / 3.82,
                      child: Text(
                        '금액',
                        style: TextStyle(
                            fontFamily: 'NotoSans_NotoSansKR-Regular',
                            fontSize: fontSizeMainText,
                            color: colorTextBright,
                            letterSpacing: -0.5
                        ),
                      ),
                    ),
                    Container(
                        child: Text(
                          '완료 / 취소',
                          style: TextStyle(
                              fontFamily: 'NotoSans_NotoSansKR-Regular',
                              fontSize: fontSizeMainText,
                              color: colorTextBright,
                              letterSpacing: -0.5
                          ),
                        )
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Ink(
                  color: colorMainBg,
                  child: new ListView.builder(
                    scrollDirection: Axis.vertical,
                    itemCount: _list?.length,
                    itemBuilder: (BuildContext ctx, int idx) {
                      return ComponentPayList(
                        incomeStatisticsItem: _list![idx],
                      );
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

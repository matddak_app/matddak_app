

import 'package:flutter/material.dart';
import 'package:matddak_v1/config/config_color.dart';
import 'package:matddak_v1/config/config_text_size.dart';
import 'package:matddak_v1/model/payment/rider_pay_response.dart';
import 'package:matddak_v1/repository/repo_payment.dart';

class PayTest extends StatefulWidget {
  const PayTest({super.key});

  @override
  State<PayTest> createState() => _PayTestState();
}

class _PayTestState extends State<PayTest> {

  RiderPayResponse? _response;


  Future<void> _loadDetail() async {
    await RepoPayment().getCurrentPay()
        .then((res) => {
      setState((){
        print("setState");
        print(res);

        _response = res.data;
      })
    })
        .catchError((err) => {
      debugPrint(err)
    });
  }

  @override
  void initState() {
    super.initState();
    _loadDetail();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          children: [
            Container(
              child: Text(
                '보유 페이',
                style: TextStyle(
                    fontFamily:'NotoSans_NotoSansKR-Regular',
                    fontSize: fontSizeMainText,
                    color: colorText,
                    letterSpacing: -0.5
                ),
              ),
            ),
            Container(
              child: Text(
                '${_response!.payNow}',
                style: TextStyle(
                    fontFamily: 'NotoSans_Bold',
                    fontSize: fontSizeTitle,
                    color: colorTextBright,
                    letterSpacing: -0.5
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

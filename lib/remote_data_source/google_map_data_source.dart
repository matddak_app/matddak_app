import 'package:dio/dio.dart';
import 'package:matddak_v1/config/config_api.dart';
import 'package:matddak_v1/functions/token_lib.dart';
import 'package:matddak_v1/model/order/request/order_get_response.dart';

class GoogleMapDataSource {
  Future<List<OrderGetResponse>> getList() async {
    String? token = await TokenLib.getRiderToken();
    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + token!;

    String _baseUrl = '$apiUrl/delivery/detail/pick';

    final response = await dio.get(
        _baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {

              return status == 200;

            }
        )
    );

    return (response.data as List)
        .map((e) => OrderGetResponse.fromJson(e))
        .toList();
  }
}
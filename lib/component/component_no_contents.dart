import 'package:flutter/material.dart';
import 'package:matddak_v1/config/config_color.dart';
import 'package:matddak_v1/config/config_text_size.dart';

class ComponentNoContents extends StatelessWidget {
  const ComponentNoContents({
    super.key,
    required this.icon,
    required this.msg
  });

  final IconData icon;
  final String msg;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          Container(
            height: 15,
            child: Icon(
              icon,
              size: 60,
            ),
          ),
          Text(
              msg,
            style: const TextStyle(
              fontFamily: 'NotoSans_NotoSansKR-SemiBold',
              fontSize: fontSizeMainText,
              color: colorText,
              letterSpacing: -0.5
            ),
          )
        ],
      ),
    );
  }
}

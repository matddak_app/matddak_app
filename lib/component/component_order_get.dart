import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:intl/intl.dart';
import 'package:matddak_v1/config/config_color.dart';
import 'package:matddak_v1/config/config_text_size.dart';
import 'package:matddak_v1/model/order/request/order_get_response.dart';
import 'package:matddak_v1/repository/repo_order.dart';

class ComponentOrderGet extends StatefulWidget {
  const ComponentOrderGet({
    super.key,
  });

  @override
  State<ComponentOrderGet> createState() => _ComponentOrderGetState();
}

class _ComponentOrderGetState extends State<ComponentOrderGet> {
  OrderGetResponse? _orderContents;

  Future<void> _loadOrderContents() async {
    await RepoOrder().getOrder()
        .then((res) => {
      setState(() {
        _orderContents = res.data;
      })
    });
  }

  void initState() {
    super.initState();
    _loadOrderContents();
  }

  @override
  Widget build(BuildContext context) {
    final formatCurrency = new NumberFormat.simpleCurrency(locale: "ko_KR", name: "", decimalDigits: 0);

    if (_orderContents == null) {
      return Expanded(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            /** 하단 영역 **/
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height / 2.6,
              decoration: BoxDecoration(
                  color: colorBox,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(15.0),
                    topRight: Radius.circular(15.0),
                  ),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black.withOpacity(0.5),
                        spreadRadius: 3,
                        blurRadius: 10,
                        offset: Offset(0,-2)
                    )
                  ]
              ),
              padding: EdgeInsets.fromLTRB(30, 30, 30, 31.9),
              /** 배차 정보 없을 때 **/
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                      Icons.info_outlined,
                    color: Colors.white,
                    size: 50,
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 8),
                    child: Text(
                      '배차 정보가 없습니다.',
                      style: TextStyle(
                          fontFamily: 'NotoSans_NotoSansKR-SemiBold',
                          fontSize: fontSizeSuper,
                          color: Colors.white,
                          letterSpacing: -0.5
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      );
    } else {
      return Expanded(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            /** 하단 영역 **/
            Container (
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    color: colorBox,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(15.0),
                      topRight: Radius.circular(15.0),
                    ),
                    /** 박스 그림자 **/
                    boxShadow: [
                      BoxShadow(
                          color: Colors.black.withOpacity(0.5),
                          spreadRadius: 3,
                          blurRadius: 10,
                          offset: Offset(0,-2)
                      )
                    ]
                ),
                padding: EdgeInsets.fromLTRB(30, 20, 30, 31.9),
                // height: 20,
                child: Column(
                  children: [
                    /** 픽업지 주소 **/
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            _orderContents!.storeName,
                            style: TextStyle(
                                fontFamily: 'Notosans_bold',
                                fontSize: fontSizeSuper,
                                color: colorText,
                                letterSpacing: -0.5
                            ),
                          ),
                          /** 매장 전화 연결 버튼 **/
                          TextButton(
                              style: TextButton.styleFrom(
                                minimumSize: Size.zero,
                                padding: EdgeInsets.fromLTRB(5, 5, 0, 5),
                                foregroundColor: Colors.green,
                              ),
                              onPressed: () {},
                              child: Row(
                                children: [
                                  Text(
                                    '매장 ',
                                    style: TextStyle(
                                        fontFamily: 'NotoSans_NotoSansKR-Regular',
                                        fontSize: fontSizeMainText,
                                        color: Color.fromRGBO(52, 199, 89, 1),
                                        letterSpacing: -0.5
                                    ),
                                  ),
                                  Icon(
                                    Icons.call,
                                    color: CupertinoColors.systemGreen,
                                    size: 15,
                                  )
                                ],
                              )
                          )
                        ],
                      ),
                    ),
                    /** 측정 거리, 산정 배달료 폼 **/
                    Container(
                      margin: EdgeInsets.only(top: 12.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          /** 측정 거리 타이틀 **/
                          Text(
                            '측정 거리',
                            style: TextStyle(
                                fontFamily: 'Notosans_bold',
                                fontSize: fontSizeMainText,
                                color: colorText,
                                letterSpacing: -0.5
                            ),
                          ),
                          /** 산정 배달료 타이틀 **/
                          Text(
                            '산정 배달료',
                            style: TextStyle(
                                fontFamily: 'Notosans_bold',
                                fontSize: fontSizeMainText,
                                color: colorText,
                                letterSpacing: -0.5
                            ),
                          ),
                        ],
                      ),
                    ),
                    /** 거리, 금액 폼 **/
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          /** 측정 거리 수치 **/
                          Container(
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Text(
                                  '${_orderContents!.distance}',
                                  style: TextStyle(
                                    fontSize: 45,
                                    fontFamily: 'NotoSans_Bold',
                                    color: Colors.white,
                                    letterSpacing: -0.5,
                                  ),
                                ),
                                /** 측정 거리 km **/
                                Container(
                                  margin: EdgeInsets.only(bottom: 8.0),
                                  child: Text(
                                    ' km',
                                    style: TextStyle(
                                        fontSize: fontSizeSuper,
                                        fontFamily: 'NotoSans_NotoSansKR-Regular',
                                        color: colorHintText,
                                        letterSpacing: -0.5
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          Container(
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[
                                /** 산정 배달료 금액 **/
                                Text(
                                  /** 1000단위 당 콤마(,) 찍기 **/
                                  formatCurrency.format(_orderContents!.feeTotal),
                                  style: TextStyle(
                                    fontSize: 45,
                                    fontFamily: 'NotoSans_Bold',
                                    color: Colors.white,
                                    letterSpacing: -0.5,
                                  ),
                                ),
                                /** 산정 배달료 원 **/
                                Container(
                                  margin: EdgeInsets.only(bottom: 8.0),
                                  child: Text(
                                    ' 원',
                                    style: TextStyle(
                                        fontSize: fontSizeSuper,
                                        fontFamily: 'NotoSans_NotoSansKR-Regular',
                                        color: colorHintText,
                                        letterSpacing: -0.5
                                    ),
                                  ),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    /** 요청 사항 폼 **/
                    Container(
                      margin: EdgeInsets.only(top: 10.0),
                      alignment: AlignmentDirectional.bottomStart,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          /** 요청 사항 타이틀 **/
                          Text(
                            '요청 사항',
                            style: TextStyle(
                                fontSize: fontSizeMainText,
                                fontFamily: 'NotoSans_Bold',
                                color: colorText,
                                letterSpacing: -0.5
                            ),
                          ),
                          /** 고객 라이더 요청 사항 **/
                          Container(
                            width: MediaQuery.of(context).size.width / 1.8,
                            margin: EdgeInsets.only(top: 5.0),
                            child: Text(
                              _orderContents!.requestRider,
                              style: TextStyle(
                                  fontSize: fontSizeMainText,
                                  fontFamily: 'NotoSans_NotoSansKR-Regular',
                                  color: colorHintText,
                                  letterSpacing: -0.5
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    /** 픽업, 배달 주소 폼 **/
                    Container(
                      margin: EdgeInsets.only(top: 20.0),
                      child: Column(
                        children: [
                          /** 픽업지 주소 **/
                          Container(
                            child: Row(
                              children: [
                                Container(
                                  child: Icon(
                                    Icons.location_on,
                                    color: colorText,
                                  ),
                                ),
                                /** 픽업 **/
                                Container(
                                  margin: EdgeInsets.only(left: 8.0),
                                  child: Text(
                                    '픽업',
                                    style: TextStyle(
                                        fontSize: fontSizeMainText,
                                        fontFamily: 'NotoSans_Bold',
                                        color: colorText,
                                        letterSpacing: -0.5
                                    ),
                                  ),
                                ),
                                /** 픽업지 주소 **/
                                Container(
                                  width: MediaQuery.of(context).size.width / 1.6,
                                  margin: EdgeInsets.only(left: 10.0),
                                  child: Text(
                                    _orderContents!.addressStoreDo,
                                    style: TextStyle(
                                        fontSize: fontSizeMainText,
                                        fontFamily: 'NotoSans_NotoSansKR-Regular',
                                        color: colorHintText,
                                        letterSpacing: -0.5
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 10.0),
                            alignment: AlignmentDirectional.bottomStart,
                            child: Text(
                              '·'
                                  '\n·',
                              style: TextStyle(
                                  fontSize: 6,
                                  fontFamily: 'NotoSans_Black',
                                  color: colorText,
                                  letterSpacing: -0.5,
                                  height: 0
                              ),
                            ),
                          ),
                          /** 배달지 주소 **/
                          Container(
                            margin: EdgeInsets.only(top: 1.0),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  margin: EdgeInsets.only(left: 1.0),
                                  decoration: BoxDecoration(
                                      border: Border.all(
                                          width: 1.5,
                                          color: colorText
                                      ),
                                      shape: BoxShape.circle
                                  ),
                                  child: Icon(
                                    Icons.keyboard_arrow_down,
                                    color: colorText,
                                    size: 18,
                                  ),
                                ),
                                /** 배달 **/
                                Container(
                                  margin: EdgeInsets.only(left: 10.0),
                                  child: Text(
                                    '배달',
                                    style: TextStyle(
                                        fontSize: fontSizeMainText,
                                        fontFamily: 'NotoSans_Bold',
                                        color: colorText,
                                        letterSpacing: -0.5
                                    ),
                                  ),
                                ),
                                /** 배달지 주소 **/
                                Container(
                                  width: MediaQuery.of(context).size.width / 1.6,
                                  margin: EdgeInsets.only(left: 10.0),
                                  child: Text(
                                    _orderContents!.addressClientDo,
                                    style: TextStyle(
                                        fontSize: fontSizeMainText,
                                        fontFamily: 'NotoSans_NotoSansKR-Regular',
                                        color: colorHintText,
                                        letterSpacing: -0.5
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                )
            ),
          ],
        ),
      );
    }
  }
}

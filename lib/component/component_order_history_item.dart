import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:matddak_v1/config/config_color.dart';
import 'package:matddak_v1/config/config_text_size.dart';
import 'package:matddak_v1/model/order/fin/order_fin_item.dart';


class ComponentOrderHistory extends StatelessWidget {
  const ComponentOrderHistory({
    super.key,
    required this.orderFinItem
  });

  final OrderFinItem orderFinItem;

  @override
  Widget build(BuildContext context) {
    final formatCurrency = new NumberFormat.simpleCurrency(locale: "ko_KR", name: "", decimalDigits: 0);

    return  Container(
      padding: EdgeInsets.fromLTRB(5, 5, 5, 0),
      child: Column(
        children: [
          /** 완료 박스 **/
          Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.fromLTRB(10, 5, 10, 8),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(6.0),
                color: colorBg,
              ),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      /** 시퀀스 **/
                      Text(
                        '${orderFinItem.id}',
                        style: TextStyle(
                            fontSize: fontSizeSuper,
                            fontFamily: 'Notosans_bold',
                            color: colorTextBright,
                            letterSpacing: -0.5
                        ),
                      ),
                      /** 완료 시간 **/
                      Text(
                        orderFinItem.dateDone,
                        style: TextStyle(
                            fontSize: fontSizeSuper,
                            fontFamily: 'Notosans_bold',
                            color: colorTextBright,
                            letterSpacing: -0.5
                        ),
                      ),
                    ],
                  ),
                  /** 구분선 **/
                  Container(
                    margin: EdgeInsets.fromLTRB(0, 5, 0, 5),
                    height: 1,
                    color: colorButton,
                  ),
                  /** 주소, 거리, 금액 큰틀 **/
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              /** 픽업지 주소 **/
                              Container(
                                  width: MediaQuery.of(context).size.width / 2,
                                  margin: EdgeInsets.fromLTRB(0, 0, 0, 5),
                                  child: Text(
                                    '[ ${orderFinItem.storeName} ]'
                                        '\n${orderFinItem.addressStoreDo}',
                                    style: TextStyle(
                                        fontSize: fontSizeMainText,
                                        fontFamily: 'NotoSans_NotoSansKR-light',
                                        color: colorText,
                                        letterSpacing: -0.5
                                    ),
                                  )
                              )
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(bottom: 5.0),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              /** 거리 **/
                              Container(
                                child: Text(
                                  '${orderFinItem.distance} km',
                                  style: TextStyle(
                                      fontFamily: 'NotoSans_Bold',
                                      fontSize: fontSizeMainText,
                                      color: colorText,
                                      letterSpacing: -0.5
                                  ),
                                ),
                              ),
                              /** 금액 **/
                              Container(
                                margin: EdgeInsets.only(left: 10.0),
                                child: Text(
                                  /** 1000단위 당 콤마(,) 찍기 **/
                                  '₩ ${formatCurrency.format(orderFinItem.feeTotal)}',
                                  style: TextStyle(
                                      fontFamily: 'NotoSans_Bold',
                                      fontSize: fontSizeMainText,
                                      color: colorText,
                                      letterSpacing: -0.5
                                  ),
                                ),
                              ),
                              /** 선,후불 **/
                              Container(
                                  margin: EdgeInsets.only(left: 10.0),
                                  width: 23,
                                  decoration: BoxDecoration(
                                    color: '선' == orderFinItem.isPay ? colorPoint : Colors.blue,
                                    shape: BoxShape.circle,
                                  ),
                                  child: Column(
                                    children: [
                                      Text(
                                        orderFinItem.isPay,
                                        style: TextStyle(
                                            fontSize: fontSizeMainText,
                                            fontFamily: 'Notosans_bold',
                                            color: colorTextBright,
                                            letterSpacing: -0.5
                                        ),
                                      ),
                                    ],
                                  )
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  /** 구분선 **/
                  Container(
                    margin: EdgeInsets.only(top: 5.0),
                    height: 1,
                    color: colorButton,
                  ),
                  /** 배달지 주소 **/
                  Container(
                    alignment: AlignmentDirectional.bottomStart,
                      margin: EdgeInsets.fromLTRB(0, 5, 0, 5),
                      child: Text(
                        orderFinItem.addressClientDo,
                        style: TextStyle(
                            fontSize: fontSizeMainText,
                            fontFamily: 'NotoSans_NotoSansKR-light',
                            color: colorText,
                            letterSpacing: -0.5
                        ),
                      )
                  ),
                  /** 구분선 **/
                  Container(
                    margin: EdgeInsets.only(top: 5.0),
                    height: 1,
                    color: colorButton,
                  ),
                  /** 배달 요청 사항 **/
                  Container(
                      alignment: AlignmentDirectional.bottomStart,
                      margin: EdgeInsets.only(top: 5),
                      child: Text(
                        orderFinItem.requestRider,
                        style: TextStyle(
                            fontSize: fontSizeMainText,
                            fontFamily: 'NotoSans_NotoSansKR-medium',
                            color: colorPoint,
                            letterSpacing: -0.5
                        ),
                      )
                  )
                ],
              )
          ),
        ],
      ),
    );
  }
}

import 'package:flutter/cupertino.dart';
import 'package:matddak_v1/config/config_color.dart';

import '../config/config_text_size.dart';

class ComponentPayDetails extends StatelessWidget {
  const ComponentPayDetails({super.key});

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child:  Container(
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.all(16),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  /** 산재 보험료 **/
                  Container(
                      width: MediaQuery.of(context).size.width / 2.15,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            '산재보험료 선 차감',
                            style: TextStyle(
                                fontFamily: 'NotoSans_Bold',
                                fontSize: fontSizeMainText,
                                color: colorTextBright,
                                letterSpacing: -0.5
                            ),
                          ),
                          /** 시간 **/
                          Text(
                            '2024-03-13 15:07:49',
                            style: TextStyle(
                                fontFamily: 'NotoSans_NotoSansKR-Regular',
                                fontSize: 15,
                                color: colorText,
                                letterSpacing: -0.5
                            ),
                          ),
                          /** 메모 **/
                          Text(
                            '라이더 수익 : 5600원',
                            style: TextStyle(
                                fontFamily: 'NotoSans_NotoSansKR-Regular',
                                fontSize: 15,
                                color: colorText,
                                letterSpacing: -0.5
                            ),
                          ),
                        ],
                      )
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width / 3.7,
                    child: Text(
                      '-24',
                      style: TextStyle(
                          fontFamily: 'NotoSans_Bold',
                          fontSize: fontSizeMainText,
                          color: colorTextBright,
                          letterSpacing: -0.5
                      ),
                    ),
                  ),
                  Text(
                    '85,600',
                    style: TextStyle(
                        fontFamily: 'NotoSans_Bold',
                        fontSize: fontSizeMainText,
                        color: colorTextBright,
                        letterSpacing: -0.5
                    ),
                    textAlign: TextAlign.right,
                  )
                ],
              ),
            ),
            /** 구분선 **/
            Container(
              height: 1,
              color: Color.fromRGBO(190, 190, 190, 0.2),
            ),
          ],
        ),
      )
    );
  }
}

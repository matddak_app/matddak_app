import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:intl/intl.dart';
import 'package:matddak_v1/config/config_color.dart';
import 'package:matddak_v1/config/config_text_size.dart';
import 'package:matddak_v1/model/order/request/order_get_response.dart';
import 'package:matddak_v1/repository/repo_order.dart';

class ComponentOrderStartResponse extends StatefulWidget {
  const ComponentOrderStartResponse({
    super.key,
  });

  @override
  State<ComponentOrderStartResponse> createState() => _ComponentOrderStartResponseState();
}

class _ComponentOrderStartResponseState extends State<ComponentOrderStartResponse> {
    OrderGetResponse? _orderContents;

    Future<void> _loadOrderContents() async {
      await RepoOrder().getStart()
          .then((res) => {
        setState(() {
          _orderContents = res.data;
        })
      });
    }

    void initState() {
      super.initState();
      _loadOrderContents();
    }

    @override
    Widget build(BuildContext context) {
      final formatCurrency = new NumberFormat.simpleCurrency(locale: "ko_KR", name: "", decimalDigits: 0);

      if (_orderContents == null) {
        return Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              /** 하단 영역 **/
              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height / 2.6,
                  decoration: BoxDecoration(
                      color: colorBox,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(15.0),
                        topRight: Radius.circular(15.0),
                      ),
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black.withOpacity(0.5),
                            spreadRadius: 3,
                            blurRadius: 10,
                            offset: Offset(0,-2)
                        )
                      ]
                  ),
                  padding: EdgeInsets.fromLTRB(30, 30, 30, 31.9),
                /** 출발 정보 없을 때 **/
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.info_outlined,
                      color: Colors.white,
                      size: 50,
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 8),
                      child: Text(
                        '출발 정보가 없습니다.',
                        style: TextStyle(
                            fontFamily: 'NotoSans_NotoSansKR-SemiBold',
                            fontSize: fontSizeSuper,
                            color: Colors.white,
                            letterSpacing: -0.5
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        );
      } else {
        return Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              /** 하단 영역 **/
              Container(
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                      color: colorBox,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(15.0),
                        topRight: Radius.circular(15.0),
                      ),
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black.withOpacity(0.5),
                            spreadRadius: 3,
                            blurRadius: 10,
                            offset: Offset(0,-2)
                        )
                      ]
                  ),
                  padding: EdgeInsets.fromLTRB(30, 30, 30, 31.9),
                  // height: 20,
                  child: Column(
                    children: [
                      Container(
                        child: Row(
                          children: [
                            Text(
                              ' 배달중',
                              style: TextStyle(
                                  fontFamily: 'Notosans_bold',
                                  fontSize: fontSizeMainText,
                                  color: colorPoint,
                                  letterSpacing: -0.5
                              ),
                            ),
                            Container(
                                margin: EdgeInsets.only(left: 5.0),
                                child: SpinKitThreeBounce(
                                  color: colorText,
                                  size: 10.0,
                                )
                            ),
                          ],
                        ),
                      ),
                      /** 배달지 주소 **/
                      Container(
                        margin: EdgeInsets.only(top: 5.0),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              width: MediaQuery.of(context).size.width / 1.5,
                              child: Text(
                                _orderContents!.addressClientDo,
                                style: TextStyle(
                                    fontFamily: 'Notosans_bold',
                                    fontSize: fontSizeSuper,
                                    color: colorText,
                                    letterSpacing: -0.5
                                ),
                              ),
                            ),
                            /** 선,후불 **/
                            Container(
                                margin: EdgeInsets.only(left: 10.0),
                                padding: EdgeInsets.only(bottom: 3.0),
                                width: 23,
                                decoration: BoxDecoration(
                                  color: '선' == _orderContents!.isPay ? colorPoint : Colors.green,
                                  shape: BoxShape.circle,
                                ),
                                child: Column(
                                  children: [
                                    Text(
                                      _orderContents!.isPay,
                                      style: TextStyle(
                                          fontSize: fontSizeMainText,
                                          fontFamily: 'Notosans_bold',
                                          color: colorTextBright,
                                          letterSpacing: -0.5
                                      ),
                                    ),
                                  ],
                                )
                            )
                          ],
                        ),
                      ),
                      /** 측정 거리, 산정 배달료 폼 **/
                      Container(
                        margin: EdgeInsets.only(top: 30.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            /** 측정 거리 타이틀 **/
                            Text(
                              '측정 거리',
                              style: TextStyle(
                                  fontFamily: 'Notosans_bold',
                                  fontSize: fontSizeMainText,
                                  color: colorText,
                                  letterSpacing: -0.5
                              ),
                            ),
                            /** 산정 배달료 타이틀 **/
                            Text(
                              '산정 배달료',
                              style: TextStyle(
                                  fontFamily: 'Notosans_bold',
                                  fontSize: fontSizeMainText,
                                  color: colorText,
                                  letterSpacing: -0.5
                              ),
                            ),
                          ],
                        ),
                      ),
                      /** 거리, 금액 폼 **/
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            /** 측정 거리 수치 **/
                            Container(
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  Text(
                                    '${_orderContents!.distance}',
                                    style: TextStyle(
                                      fontSize: 45,
                                      fontFamily: 'NotoSans_Bold',
                                      color: Colors.white,
                                      letterSpacing: -0.5,
                                    ),
                                  ),
                                  /** 측정 거리 km **/
                                  Container(
                                    margin: EdgeInsets.only(bottom: 8.0),
                                    child: Text(
                                      ' km',
                                      style: TextStyle(
                                          fontSize: fontSizeSuper,
                                          fontFamily: 'NotoSans_NotoSansKR-Regular',
                                          color: colorHintText,
                                          letterSpacing: -0.5
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Container(
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  /** 산정 배달료 금액 **/
                                  Text(
                                    /** 1000단위 당 콤마(,) 찍기 **/
                                    formatCurrency.format(_orderContents!.feeTotal),
                                    style: TextStyle(
                                      fontSize: 45,
                                      fontFamily: 'NotoSans_Bold',
                                      color: Colors.white,
                                      letterSpacing: -0.5,
                                    ),
                                  ),
                                  /** 산정 배달료 원 **/
                                  Container(
                                    margin: EdgeInsets.only(bottom: 8.0),
                                    child: Text(
                                      ' 원',
                                      style: TextStyle(
                                          fontSize: fontSizeSuper,
                                          fontFamily: 'NotoSans_NotoSansKR-Regular',
                                          color: colorHintText,
                                          letterSpacing: -0.5
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      /** 요청 사항 폼 **/
                      Container(
                        margin: EdgeInsets.only(top: 20.0),
                        alignment: AlignmentDirectional.bottomStart,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            /** 요청 사항 타이틀 **/
                            Text(
                              '요청 사항',
                              style: TextStyle(
                                  fontSize: fontSizeMainText,
                                  fontFamily: 'NotoSans_Bold',
                                  color: colorText,
                                  letterSpacing: -0.5
                              ),
                            ),
                            /** 고객 라이더 요청 사항 **/
                            Container(
                              width: MediaQuery.of(context).size.width / 1.8,
                              margin: EdgeInsets.only(top: 5.0),
                              child: Text(
                                _orderContents!.requestRider,
                                style: TextStyle(
                                    fontSize: fontSizeMainText,
                                    fontFamily: 'NotoSans_NotoSansKR-Regular',
                                    color: colorHintText,
                                    letterSpacing: -0.5
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  )
              ),
            ],
          ),
        );
      }
  }
}

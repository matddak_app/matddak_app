import 'package:flutter/material.dart';
import 'package:matddak_v1/config/config_color.dart';
import 'package:matddak_v1/config/config_text_size.dart';

class ComponentAppbar extends StatelessWidget implements PreferredSizeWidget {
  const ComponentAppbar({
    super.key,
    required this.text,
    required this.page
  });

  final String text;
  final String page;

  @override
  Widget build(BuildContext context) {
    return AppBar(

        leading: IconButton(
          onPressed:(){
            Navigator.of(context).pushNamed('${page}');
          },
          icon:Icon(Icons.keyboard_backspace,color: Colors.white,),
        ),
        backgroundColor: colorMainBg ,
        title: Align(
          alignment: Alignment.bottomRight,
          child: Text(
            text,
            style: TextStyle(
              fontFamily: 'NotoSans_Bold',
              fontSize: appbarText,
              color: Colors.white,
              letterSpacing: -0.5,
            ),
          ),
        )
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(45);
}
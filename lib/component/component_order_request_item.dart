import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:matddak_v1/config/config_color.dart';
import 'package:matddak_v1/config/config_text_size.dart';
import 'package:matddak_v1/model/order/request/order_request_item.dart';


class ComponentRequestItem extends StatelessWidget {
  const ComponentRequestItem({
    super.key,
    required this.orderRequestItem,
    required this.callback
  });

  final OrderRequestItem orderRequestItem;
  final VoidCallback callback;


  @override
  Widget build(BuildContext context) {
    final formatCurrency = new NumberFormat.simpleCurrency(locale: "ko_KR", name: "", decimalDigits: 0);

    return GestureDetector(
      onTap: callback,
      child: Container(
        color: colorMainBg,
        padding: EdgeInsets.fromLTRB(5, 5, 5, 0),
        child: Column(
          children: [
            /** 요청 박스 **/
            Container(
              width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.fromLTRB(10, 5, 10, 8),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(6.0),
                  color: colorBg,
                ),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        /** 거리 **/
                        Text(
                          '${orderRequestItem.distance}km',
                          style: TextStyle(
                              fontSize: fontSizeSuper,
                              fontFamily: 'Notosans_bold',
                              color: colorTextBright,
                              letterSpacing: -0.5
                          ),
                        ),
                        /** 가격 **/
                        Container(
                          child: Row(
                            children: [
                              Text(
                                '₩ ',
                                style: TextStyle(
                                    fontSize: fontSizeSuper,
                                    fontFamily: 'Notosans_bold',
                                    color: colorTextBright,
                                    letterSpacing: -0.5
                                ),
                              ),
                              Text(
                                formatCurrency.format(orderRequestItem.priceRide),
                                style: TextStyle(
                                    fontSize: fontSizeSuper,
                                    fontFamily: 'Notosans_bold',
                                    color: colorTextBright,
                                    letterSpacing: -0.5
                                ),
                              ),
                              /** 선,후불 **/
                              Container(
                                  margin: EdgeInsets.only(left: 10.0),
                                  padding: EdgeInsets.only(bottom: 3.0),
                                  width: 23,
                                  decoration: BoxDecoration(
                                    color: '선' == orderRequestItem.isPay ? colorPoint : Colors.green,
                                    shape: BoxShape.circle,
                                  ),
                                  child: Column(
                                    children: [
                                      Text(
                                        orderRequestItem.isPay,
                                        style: TextStyle(
                                            fontSize: fontSizeMainText,
                                            fontFamily: 'Notosans_bold',
                                            color: colorTextBright,
                                            letterSpacing: -0.5
                                        ),
                                      ),
                                    ],
                                  )
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                    /** 구분선 **/
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 5, 0, 5),
                      height: 1,
                      color: colorButton,
                    ),
                    /** 핀 아이콘 **/
                    Container(
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 4.0),
                              width: 28,
                              decoration: BoxDecoration(
                                color: colorText,
                                shape: BoxShape.circle,
                              ),
                              child: Column(
                                children: [
                                  Icon(
                                    Icons.location_on,
                                    color: colorBg,
                                  )
                                ],
                              )
                          ),
                          /** 픽업지 주소 **/
                          Container(
                            width: MediaQuery.of(context).size.width / 1.2,
                              margin: EdgeInsets.fromLTRB(5, 0, 0, 5),
                              child: Text(
                                '[ ${orderRequestItem.storeName} ]'
                                    '\n${orderRequestItem.addressStoreDo}',
                                style: TextStyle(
                                    fontSize: fontSizeMainText,
                                    fontFamily: 'NotoSans_NotoSansKR-light',
                                    color: colorTextBright,
                                    letterSpacing: -0.5
                                ),
                              )
                          )
                        ],
                      ),
                    ),
                    /** 구분선 **/
                    Container(
                      margin: EdgeInsets.only(top: 5.0),
                      height: 1,
                      color: colorButton,
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 2.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                              margin: EdgeInsets.only(top: 5.0),
                              padding: EdgeInsets.only(bottom: 3.0),
                              width: 23,
                              decoration: BoxDecoration(
                                color: Color.fromRGBO(237, 63, 9, 1),
                                shape: BoxShape.circle,
                              ),
                              child: Column(
                                children: [
                                  Text(
                                    '요',
                                    style: TextStyle(
                                        fontSize: fontSizeMainText,
                                        fontFamily: 'Notosans_bold',
                                        color: colorTextBright,
                                        letterSpacing: -0.5
                                    ),
                                  ),
                                ],
                              )
                          ),
                          Container(
                              width: MediaQuery.of(context).size.width / 1.2,
                              margin: EdgeInsets.fromLTRB(8, 5, 0, 2),
                              child: Text(
                                '${orderRequestItem.addressClientDo}',
                                style: TextStyle(
                                    fontSize: fontSizeMainText,
                                    fontFamily: 'NotoSans_NotoSansKR-light',
                                    color: colorTextBright,
                                    letterSpacing: -0.5
                                ),
                              )
                          )
                        ],
                      ),
                    ),
                    /** 구분선 **/
                    Container(
                      margin: EdgeInsets.only(top: 5.0),
                      height: 1,
                      color: colorButton,
                    ),
                    /** 라이더 요청 사항 **/
                    Container(
                        alignment: AlignmentDirectional.bottomStart,
                        margin: EdgeInsets.fromLTRB(5, 5, 0, 0),
                        child: Text(
                          orderRequestItem.requestRider,
                          style: TextStyle(
                              fontSize: fontSizeMainText,
                              fontFamily: 'NotoSans_NotoSansKR-medium',
                              color: colorPoint,
                              letterSpacing: -0.5
                          ),
                        )
                    )
                  ],
                )
            ),
          ],
        ),
      ),
    );
  }
}

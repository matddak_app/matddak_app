

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:matddak_v1/config/config_color.dart';
import 'package:matddak_v1/config/config_text_size.dart';
import 'package:matddak_v1/model/payment/income_statistics_item.dart';

class ComponentPayList extends StatelessWidget {
  const ComponentPayList({
    super.key,
    required this.incomeStatisticsItem,

  });
  final IncomeStatisticsItem incomeStatisticsItem;

  @override
  Widget build(BuildContext context) {
    final formatCurrency = new NumberFormat.simpleCurrency(locale: "ko_KR", name: "", decimalDigits: 0);

    return Column(
      children: [
        /** 컴포넌트 시작 **/
        Container(
          color: colorMainBg,
          alignment: AlignmentDirectional.bottomStart,
          padding: EdgeInsets.fromLTRB(16 ,16 , 16, 20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              /** 날짜 **/
              Text(
                '${incomeStatisticsItem.dateTotalIncome}',
                style: TextStyle(
                    fontFamily: 'NotoSans_Bold',
                    fontSize: fontSizeMainText,
                    color: colorTextBright,
                    letterSpacing: -0.5
                ),
              ),
              /** 금액 **/
              Text(
                /** 1000단위 당 콤마(,) 찍기 **/
                formatCurrency.format(incomeStatisticsItem.totalDayRider),
                style: TextStyle(
                    fontFamily: 'NotoSans_Bold',
                    fontSize: fontSizeMainText,
                    color: colorTextBright,
                    letterSpacing: -0.5
                ),
              ),
              /** 완료 / 취소 **/
              Text(
                '${incomeStatisticsItem.totalCount}'+'/'+'${incomeStatisticsItem.totalCancelCount}',
                style: TextStyle(
                    fontFamily: 'NotoSans_Bold',
                    fontSize: fontSizeMainText,
                    color: colorTextBright,
                    letterSpacing: -0.5
                ),
              ),
            ],
          ),
        ),
        /** 구분선 **/
        Container(
          height: 1,
          color: Color.fromRGBO(190, 190, 190, 0.2),
        ),

      ],
    );
  }
}

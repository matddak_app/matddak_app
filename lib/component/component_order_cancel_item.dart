import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:matddak_v1/config/config_color.dart';
import 'package:matddak_v1/config/config_text_size.dart';
import 'package:matddak_v1/model/order/cancel/order_cancel_item.dart';


class ComponentOrderCancelItem extends StatefulWidget {
  const ComponentOrderCancelItem({
    super.key,
    required this.orderCancelItem
  });

  final OrderCancelItem orderCancelItem;

  @override
  State<ComponentOrderCancelItem> createState() => _ComponentOrderCancelItemState();
}

class _ComponentOrderCancelItemState extends State<ComponentOrderCancelItem> {
  final formatCurrency = new NumberFormat.simpleCurrency(locale: "ko_KR", name: "", decimalDigits: 0);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: colorMainBg,
      padding: EdgeInsets.fromLTRB(5, 5, 5, 0),
      child: Column(
        children: [
          /** 취소 박스 **/
          Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.fromLTRB(10, 5, 10, 8),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(6.0),
                color: colorBg,
              ),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      /** 시퀀스 **/
                      Text(
                        '${widget.orderCancelItem.id}',
                        style: TextStyle(
                            fontSize: fontSizeSuper,
                            fontFamily: 'Notosans_bold',
                            color: colorTextBright,
                            letterSpacing: -0.5
                        ),
                      ),
                      /** 취소 시간 **/
                      Text(
                        // widget.orderCancelItem.timeAsk.substring(11, 16),
                        widget.orderCancelItem.timeAsk,
                        style: TextStyle(
                            fontSize: fontSizeSuper,
                            fontFamily: 'NotoSans_Bold',
                            color: colorTextBright,
                            letterSpacing: -0.5
                        ),
                      ),
                    ],
                  ),
                  /** 구분선 **/
                  Container(
                    margin: EdgeInsets.fromLTRB(0, 5, 0, 5),
                    height: 1,
                    color: colorButton,
                  ),
                  /** 핀 아이콘 **/
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                  margin: EdgeInsets.only(top: 5.0),
                                  width: 28,
                                  decoration: BoxDecoration(
                                    color: colorText,
                                    shape: BoxShape.circle,
                                  ),
                                  child: Column(
                                    children: [
                                      Icon(
                                        Icons.location_on,
                                        color: colorBg,
                                      )
                                    ],
                                  )
                              ),
                              /** 픽업지 주소 **/
                              Container(
                                  width: MediaQuery.of(context).size.width / 1.7,
                                  margin: EdgeInsets.fromLTRB(5, 0, 0, 5),
                                  child: Text(
                                    '[ ${widget.orderCancelItem.storeName} ]'
                                        '\n${widget.orderCancelItem.addressStoreDo}',
                                    style: TextStyle(
                                        fontSize: fontSizeMainText,
                                        fontFamily: 'NotoSans_NotoSansKR-light',
                                        color: colorText,
                                        letterSpacing: -0.5
                                    ),
                                  )
                              )
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(bottom: 5.0),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              /** 선,후불 **/
                              Container(
                                  margin: EdgeInsets.only(right: 2.0),
                                  padding: EdgeInsets.only(bottom: 3.0),
                                  width: 23,
                                  decoration: BoxDecoration(
                                    color: '선' == widget.orderCancelItem.isPay ? colorPoint : Colors.green,
                                    shape: BoxShape.circle,
                                  ),
                                  child: Column(
                                    children: [
                                      Text(
                                        widget.orderCancelItem.isPay,
                                        style: TextStyle(
                                            fontSize: fontSizeMainText,
                                            fontFamily: 'Notosans_bold',
                                            color: colorTextBright,
                                            letterSpacing: -0.5
                                        ),
                                      ),
                                    ],
                                  )
                              ),
                              Container(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    /** 거리 **/
                                    Container(
                                      child: Text(
                                        '${widget.orderCancelItem.distance} km',
                                        style: TextStyle(
                                            fontFamily: 'NotoSans_Bold',
                                            fontSize: fontSizeMainText,
                                            color: colorText,
                                            letterSpacing: -0.5
                                        ),
                                      ),
                                    ),
                                    /** 금액 **/
                                    Container(
                                      child: Text(
                                        formatCurrency.format(widget.orderCancelItem.priceRide),
                                        style: TextStyle(
                                            fontFamily: 'NotoSans_Bold',
                                            fontSize: fontSizeMainText,
                                            color: colorText,
                                            letterSpacing: -0.5
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  /** 구분선 **/
                  Container(
                    margin: EdgeInsets.only(top: 5.0),
                    height: 1,
                    color: colorButton,
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 2.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        /** 취소 아이콘 **/
                        Container(
                            margin: EdgeInsets.fromLTRB(0, 8, 0, 3),
                            padding: EdgeInsets.only(bottom: 3.0),
                            width: 23,
                            decoration: BoxDecoration(
                              color: Colors.grey,
                              shape: BoxShape.circle,
                            ),
                            child: Column(
                              children: [
                                Text(
                                  '취',
                                  style: TextStyle(
                                      fontSize: fontSizeMainText,
                                      fontFamily: 'Notosans_bold',
                                      color: colorTextBright,
                                      letterSpacing: -0.5
                                  ),
                                ),
                              ],
                            )
                        ),
                        /** 배달지 주소 **/
                        Container(
                            width: MediaQuery.of(context).size.width / 1.4,
                            margin: EdgeInsets.only(left: 8.0),
                            child: Text(
                              widget.orderCancelItem.addressClientDo,
                              style: TextStyle(
                                  fontSize: fontSizeMainText,
                                  fontFamily: 'NotoSans_NotoSansKR-light',
                                  color: colorText,
                                  letterSpacing: -0.5
                              ),
                            )
                        )
                      ],
                    ),
                  ),
                  /** 구분선 **/
                  Container(
                    margin: EdgeInsets.only(top: 5.0),
                    height: 1,
                    color: colorButton,
                  ),
                  /** 픽업지 주소 **/
                  Container(
                      alignment: AlignmentDirectional.bottomStart,
                      margin: EdgeInsets.fromLTRB(5, 5, 0, 0),
                      child: Text(
                        widget.orderCancelItem.requestRider,
                        style: TextStyle(
                            fontSize: fontSizeMainText,
                            fontFamily: 'NotoSans_NotoSansKR-medium',
                            color: colorPoint,
                            letterSpacing: -0.5
                        ),
                      )
                  )
                ],
              )
          ),
        ],
      ),
    );
  }
}

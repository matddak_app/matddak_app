

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:matddak_v1/config/config_color.dart';
import 'package:matddak_v1/config/config_text_size.dart';
import 'package:matddak_v1/model/payment/money_history_item.dart';

class ComponentOutMoney extends StatelessWidget {
  const ComponentOutMoney({
    super.key,
    required this.moneyHistoryItem
  });

  final MoneyHistoryItem moneyHistoryItem;

  @override
  Widget build(BuildContext context) {
    final formatCurrency = new NumberFormat.simpleCurrency(locale: "ko_KR", name: "", decimalDigits: 0);

    return SingleChildScrollView(
      child: Container(
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.fromLTRB(16 , 16, 16, 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  /** 날짜 **/
                  Text(
                    '${moneyHistoryItem.dateMoneyRenewal}',
                    style: TextStyle(
                        fontFamily: 'NotoSans_Bold',
                        fontSize: fontSizeMainText,
                        color: colorTextBright,
                        letterSpacing: -0.5
                    ),
                  ),
                  /** 금액 **/
                  Text(
                    /** 1000단위 당 콤마(,) 찍기 **/
                    '- ${formatCurrency.format(moneyHistoryItem.moneyAmount)} 원',
                    style: TextStyle(
                        fontFamily: 'NotoSans_Bold',
                        fontSize: fontSizeMainText,
                        color: colorTextBright,
                        letterSpacing: -0.5
                    ),
                  ),
                ],
              ),

            ),
            /** 구분선 **/
            Container(
              height: 1,
              color: Color.fromRGBO(190, 190, 190, 0.2),
            ),
          ],
        ),
      ),
    );

  }
}


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:matddak_v1/config/config_color.dart';
import 'package:matddak_v1/config/config_text_size.dart';

class ComponentCalender extends StatefulWidget {

  const ComponentCalender({super.key, required this.date, required this.changeDate});

   final date;
   final changeDate;

   @override
   State<ComponentCalender> createState() => _ComponentCalenderState();

 }

 class _ComponentCalenderState extends State<ComponentCalender> {

   @override
   Widget bulid(BuildContext context) {
     return MaterialApp(
       // localizationsDelegates: const [
       //   GlobalMaterialLocalizations.delegate,
       //   GlobalWidgetsLocalizations.delegate,
       //   GlobalCupertinoLocalizations.delegate,
       //
       // ],
       theme: ThemeData(brightness: Brightness.dark),
     );

   }

   @override
   Widget build(BuildContext context) {
     return GestureDetector(
           child: Column(
             children: [
               Container(
                 width: MediaQuery.of(context).size.width / 2.38,
                 height: 55,
                 decoration: BoxDecoration(
                     color: colorButton,
                     borderRadius: BorderRadius.circular(8)
                 ),
                 child: Container(
                   alignment: FractionalOffset.center,
                   margin: EdgeInsets.fromLTRB(0, 5, 0, 10),
                   child: TextButton(
                     onPressed: () async{
                       final selectedDate = await showDatePicker(
                         builder: (BuildContext context, Widget ?child) {
                           return Theme(data: ThemeData.dark(), child: child ??Text(''));
                         },
                         context: context, // 팝업으로 띄우기 때문에 context 전달
                         initialDate: widget.date, // 달력을 띄웠을 때 선택된 날짜. 위에서 date 변수에 오늘 날짜를 넣었으므로 오늘 날짜가 선택돼서 나옴
                         firstDate: DateTime(1980), // 시작 년도
                         lastDate: DateTime.now(), // 마지막 년도. 오늘로 지정하면 미래의 날짜 선택불가
                       );
                       if (selectedDate != null) {
                         setState(() {
                           widget.changeDate(selectedDate);
                         });
                       }
                     },
                     child: Text(
                       '${widget.date}'.substring(0, 10), //출력하면 시간도 같이 출력이 되는데 날짜만 표시하고 싶어서 substring으로 잘라냄
                       style: TextStyle(
                         fontFamily: 'NotoSans_Bold',
                         color: colorTextBright,
                         fontSize: fontSizeSuper,
                       ),
                     ),
                   ),
                 ),
               ),
             ],
           ),

         );

   }
 }

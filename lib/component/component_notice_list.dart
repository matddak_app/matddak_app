import 'package:flutter/material.dart';
import 'package:matddak_v1/config/config_color.dart';
import 'package:matddak_v1/config/config_text_size.dart';
import 'package:matddak_v1/model/notice/notice_item.dart';

class ComponentNoticeList extends StatelessWidget {
  const ComponentNoticeList({
    super.key,
    required this.noticeItem,
    required this. callback,

  });

  final NoticeItem noticeItem;
  final VoidCallback callback;




  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Column(
        children: [
          Container(
            color: colorMainBg,
            alignment: AlignmentDirectional.bottomStart,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                /** 컴포넌트 시작 **/
                Container(
                  alignment: AlignmentDirectional.bottomStart,
                  padding: EdgeInsets.all(16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        noticeItem.title,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            fontFamily: 'NotoSans_Bold',
                            fontSize: fontSizeMainText,
                            color: colorTextBright,
                            letterSpacing: -0.5
                        ),
                      ),
                      Text(
                        noticeItem.dateCreate,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            fontFamily:'NotoSans_NotoSansKR-Light',
                            fontSize: fontSizeMainText,
                            color: colorHintText
                        ),
                      ),
                    ],
                  ),
                ),
                /** 구분선 **/
                Container(
                  height: 1,
                  color: Color.fromRGBO(190, 190, 190, 0.2),
                ),
              ],
            ),
          ),
        ],
      ),
      onTap: () {
        // 공지사항 목록에서 itemBuilder 로 해당 클래스 호출 했을 시 실행되는 함수를 callback으로 넘겨줬음
        // 근데 네비게이터를 써버려서 다른데로 보내버림. 콜백함수를 생성했으면 써야할거아니냐...
        callback();
      },
    );
  }
}

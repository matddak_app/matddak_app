const double fontSizeTitle = 25;
const double appbarText = 20;
const double fontSizeSuper = 20;
const double fontSizeMainText = 17;
const double fontSizeMicro = 12;
const double fontSizeCareful = 13;
const double tabBarText = 16;
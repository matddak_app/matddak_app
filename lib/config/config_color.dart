import 'package:flutter/material.dart';


const Color colorSecondary = Color.fromARGB(255, 251, 106, 28);
const Color colorRed = Color.fromARGB(255, 216, 34, 34);
const Color colorLightGray = Color.fromRGBO(0, 0, 0, 0.1);

const Color colorBg=Color.fromRGBO(26,26,26,1);
const Color colorLogin = Color.fromRGBO(40, 40, 40, 1);
const Color colorJoin = Color.fromRGBO(39, 39, 39, 1);
const Color colorPrimary = Color.fromRGBO(243, 155, 30, 1);
const Color colorPoint = Color.fromRGBO(235, 133, 18, 1);
const Color colorText = Color.fromRGBO(190, 190, 190, 1);
const Color colorBox = Color.fromRGBO(33, 33, 33, 1);
const Color colorWarningText = Color.fromRGBO(94, 94, 94, 1);
const Color colorButton = Color.fromRGBO(64, 64, 64, 1);
const Color colorTextBright = Color.fromRGBO(224, 224, 224, 1);
const Color colorHintText = Color.fromRGBO(127, 127, 127, 1);
const Color colorDrawerListColor = Color.fromRGBO(51, 51, 51, 1);
const Color colorMainBg = Color.fromRGBO(48, 48, 48, 1);
const Color colorDivider = Color.fromRGBO(190, 190, 190, 0.2);



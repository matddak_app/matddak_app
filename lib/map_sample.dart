import 'dart:async';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:matddak_v1/remote_data_source/google_map_data_source.dart';

class MapSample extends StatefulWidget {
  const MapSample({
    super.key,
  });


  @override
  State<MapSample> createState() => MapSampleState();
}

class MapSampleState extends State<MapSample> {

  Future<Position> getCurrentLocation() async {
    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);

    return position;
  }

  Position? position;

  /**
   * 기기 현재 위치 확인
   *
   * 위치 서비스가 활성화 되지 않았거나 사용 권한이 있는 경우
   * '미래'에 오류가 반환되는 것을 거부한다.
   */
  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    // 위치 서비스 활성화 테스트
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      // 위치서비스 비활성화 => 계속 하지 않음
      // 위체이 액세스 하고 사용자에게 요청
      // 앱 위치 서비스 활성화
      return Future.error('위치 서비스가 거부되었습니다.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        // 권한이 거부되었습니다 => 다음에 시도할 수 있습니다.
        // 권한 재요청 ( this is also where )
        // Android's는 RequestPermissionRationale를 표시해야 합니다
        // 사실로 돌아갔습니다. 안드로이드 가이드라인에 따르면
        // 지금 앱에 설명 UI가 표시되어야 합니다.
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // 권한은 영원히 거부되며, 적절히 처리됩니다.
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    // 우리가 여기에 도착하면, 허가가 주어지고 우리는 할 수 있습니다
    // 장치의 위치에 계속 액세스합니다.
    return await Geolocator.getCurrentPosition();
  }

  /// 현재 위치
  Future<Position> currentLocation() async {
    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);

    return position;
  }

  List<Marker> _markers = [];

  final Completer<GoogleMapController> _controller =
  Completer<GoogleMapController>();

  final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(37.5665734, 126.978179),
    zoom: 14.4746,
  );

  @override
  void initState() {
    super.initState();
    _determinePosition();
    _getMapList();

    _determinePosition().then((value) => {
      setState((){
        position = value;
      })
    });
  }

  Future<void> _getMapList() async {
    await GoogleMapDataSource().getList().then((res) => {
      res.forEach((e) {
        setState(() {
          _markers.add(
              Marker(
                  markerId: MarkerId(e.addressStoreDo),
                  position: LatLng(e.storeX.toDouble(), e.storeY.toDouble()),
                  infoWindow: InfoWindow(title: e.priceFood.toString()))
          );
        });
      })
    });
  }

  static const CameraPosition _kLake = CameraPosition(
      bearing: 192.8334901395799,
      target: LatLng(37.43296265331129, -122.08832357078792),
      tilt: 59.440717697143555,
      zoom: 19.151926040649414);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GoogleMap(
        mapType: MapType.normal,
        markers: Set.from(_markers),
        initialCameraPosition: _kGooglePlex,
        onMapCreated: (GoogleMapController controller) {
          _controller.complete(controller);
        },
        myLocationEnabled: true,
        myLocationButtonEnabled: false,
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: _goToTheLake,
        label: const Text('현재위치 가져와라'),
      ),
    );
  }

  Future<void> _goToTheLake() async {
    final GoogleMapController controller = await _controller.future;
    await controller.animateCamera(CameraUpdate.newCameraPosition(_kLake));
  }
}
import 'package:geolocator/geolocator.dart';

/// 기기 현재 위치 확인
///
/// 위치 서비스가 활성화 되지 않았거나 사용 권한이 있는 경우
/// '미래'에 오류가 반환되는 것을 거부한다.
Future<Position> _determinePosition() async {
  bool serviceEnabled;
  LocationPermission permission;

  // 위치 서비스 활성화 테스트
  serviceEnabled = await Geolocator.isLocationServiceEnabled();
  if (!serviceEnabled) {
    // 위치서비스 비활성화 => 계속 하지 않음
    // 위체이 액세스 하고 사용자에게 요청
    // 앱 위치 서비스 활성화
    return Future.error('위치 서비스가 거부되었습니다.');
  }

  permission = await Geolocator.checkPermission();
  if (permission == LocationPermission.denied) {
    permission = await Geolocator.requestPermission();
    if (permission == LocationPermission.denied) {
      // 권한이 거부되었습니다 => 다음에 시도할 수 있습니다.
      // 권한 재요청 ( this is also where )
      // Android's는 RequestPermissionRationale를 표시해야 합니다
      // 사실로 돌아갔습니다. 안드로이드 가이드라인에 따르면
      // 지금 앱에 설명 UI가 표시되어야 합니다.
      return Future.error('Location permissions are denied');
    }
  }

  if (permission == LocationPermission.deniedForever) {
    // 권한은 영원히 거부되며, 적절히 처리됩니다.
    return Future.error(
        'Location permissions are permanently denied, we cannot request permissions.');
  }

    // 우리가 여기에 도착하면, 허가가 주어지고 우리는 할 수 있습니다
    // 장치의 위치에 계속 액세스합니다.
  return await Geolocator.getCurrentPosition();
}
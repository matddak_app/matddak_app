

import 'package:matddak_v1/model/payment/income_statistics_item.dart';

class ListResultIncomeStatisticsItem{

  String msg;
  num code;
  bool isSuccess;
  num totalCount;
  num totalPage;
  num currentPage;
  List<IncomeStatisticsItem> list;

  ListResultIncomeStatisticsItem(
      this.msg,
      this.code,
      this.isSuccess,
      this.totalCount,
      this.totalPage,
      this.currentPage,
      this.list,

      );

  factory ListResultIncomeStatisticsItem.fromJson(Map<String,dynamic>json) {

    return ListResultIncomeStatisticsItem(

      json['msg'],
      json['code'],
      json['isSuccess'],
      json['totalCount'],
      json['totalPage'],
      json['currentPage'],
      json['list'] !=null
        ? (json['list'] as List ).map((e) => IncomeStatisticsItem.fromJson(e)).toList()
          :[],
    );

  }


}
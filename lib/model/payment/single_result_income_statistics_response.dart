

import 'package:matddak_v1/model/payment/income_statistics_response.dart';

class SingleResultIncomeStatisticsResponse {

  String msg;
  num code;
  bool isSuccess;
  IncomeStatisticsResponse data;

  SingleResultIncomeStatisticsResponse(
      this.msg,
      this.code,
      this.isSuccess,
      this.data
      );

  factory SingleResultIncomeStatisticsResponse.fromJson(Map<String,dynamic>json){
    return SingleResultIncomeStatisticsResponse(
        json['msg'],
        json['code'],
        json['isSuccess'],
        IncomeStatisticsResponse.fromJson(json['data'])
    );
  }

}
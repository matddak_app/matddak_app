


import 'package:matddak_v1/model/payment/money_history_item.dart';

class ListResultMoneyHistoryItem {

   String msg;
   num code;
   bool isSuccess;
   num totalCount;
   num totalPage;
   num currentPage;
   List<MoneyHistoryItem> list;

   ListResultMoneyHistoryItem(
       this.msg,
       this.code,
       this.isSuccess,
       this.totalCount,
       this.totalPage,
       this.currentPage,
       this.list
       );


   factory ListResultMoneyHistoryItem.fromJson(Map<String,dynamic>json){

      return ListResultMoneyHistoryItem(
          json['msg'],
          json['code'],
          json['isSuccess'],
          json['totalCount'],
          json['totalPage'],
          json['currentPage'],
          (json['list'] != null
          ? (json['list'] as List ).map((e) => MoneyHistoryItem.fromJson(e)).toList()
              :[]),
      );
   }
}


class MoneyHistoryItem {

  num id;
  num riderId;
  String riderName;
  String moneyType;
  num moneyAmount;
  String dateMoneyRenewal;
  String? etcMemo;

  MoneyHistoryItem(
      this.id,
      this.riderId,
      this.riderName,
      this.moneyType,
      this.moneyAmount,
      this.dateMoneyRenewal,
      this.etcMemo
      );

  factory MoneyHistoryItem.fromJson(Map<String,dynamic>json) {

    return MoneyHistoryItem(
        json['id'],
        json['riderId'],
        json['riderName'],
        json['moneyType'],
        json['moneyAmount'],
        json['dateMoneyRenewal'],
        json['etcMemo']);
  }

}
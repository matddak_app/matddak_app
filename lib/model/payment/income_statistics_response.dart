

class 	IncomeStatisticsResponse{

  String dateTotalIncome;
  num totalCount;
  num totalFee;
  num totalAdmin;
  num totalRider;
  num totalDistance;
  num perDistance;

  IncomeStatisticsResponse(
      this.dateTotalIncome,
      this.totalCount,
      this.totalFee,
      this.totalAdmin,
      this.totalRider,
      this.totalDistance,
      this.perDistance,
      );

  factory	IncomeStatisticsResponse.fromJson(Map<String,dynamic>json) {

    return IncomeStatisticsResponse(
        json['dateTotalIncome'],
        json['totalCount'],
        json['totalFee'],
        json['totalAdmin'],
        json['totalRider'],
        json['totalDistance'],
        json['perDistance']
    );

  }

 }

class RiderPayResponse{

  String riderName;
  num payNow;
  String bankName;
  String bankNumber;
  String bankOwner;


  RiderPayResponse(
      this.riderName,
      this.payNow,
      this.bankName,
      this.bankNumber,
      this.bankOwner
      );

  factory RiderPayResponse.fromJson(Map<String,dynamic>json) {

    return RiderPayResponse(
      json['riderName'],
      json['payNow'],
      json['bankName'],
      json['bankNumber'],
      json['bankOwner'],
    );
  }

}
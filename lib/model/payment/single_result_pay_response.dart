
import 'package:matddak_v1/model/payment/rider_pay_response.dart';

class SingleResultPayResponse {

  String msg;
  num code;
  bool isSuccess;
  RiderPayResponse data;

  SingleResultPayResponse(
      this.msg,
      this.code,
      this.isSuccess,
      this.data
      );

  factory SingleResultPayResponse.fromJson(Map<String,dynamic>json){


    // print(json['data']);
    // print(IncomeStatisticsResponse.fromJson(json['data']));
    return SingleResultPayResponse(
        json['msg'],
        json['code'],
        json['isSuccess'],
        RiderPayResponse.fromJson(json['data']));
  }

}

class MoneyHistoryCreateRequest {

  num moneyAmount;

  String? etcMemo;

  MoneyHistoryCreateRequest(this.moneyAmount, [this.etcMemo]);

  Map<String, dynamic> toJson() {

    final Map<String,dynamic> data = Map<String,dynamic>();
    data['moneyAmount'] = this.moneyAmount;
    data['etcMemo'] = this.etcMemo;
    return data;

  }
}
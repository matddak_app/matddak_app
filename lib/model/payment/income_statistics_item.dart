
class IncomeStatisticsItem {

  String dateTotalIncome;
  num totalRider;
  num totalDayRider;
  num totalCount;
  num totalCancelCount;

  IncomeStatisticsItem(
      this.dateTotalIncome,
      this.totalRider,
      this.totalDayRider,
      this.totalCount,
      this.totalCancelCount
      );

  factory	IncomeStatisticsItem.fromJson(Map<String,dynamic>json) {

    return IncomeStatisticsItem(
        json['dateTotalIncome'],
        json['totalRider'],
        json['totalDayRider'],
        json['totalCount'],
        json['totalCancelCount']
    );
  }

}

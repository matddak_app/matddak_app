import 'package:matddak_v1/model/login/login_response.dart';

class LoginResult {
  LoginResponse data;

  LoginResult(this.data);

  factory LoginResult.fromJson(Map<String, dynamic> json) {
    return LoginResult(
      LoginResponse.fromJson(json['data'])
    );
  }
}
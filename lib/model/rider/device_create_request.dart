
class DeviceCreateRequest{

  num id;
  String phoneNumber;
  String phoneType;

  DeviceCreateRequest(
      this.id,
      this.phoneNumber,
      this.phoneType
      );
  Map<String,dynamic> toJson() => {

    'id':id,
    'phoneNumber':phoneNumber,
    'phoneType':phoneType

  };

}

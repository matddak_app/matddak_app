import 'package:matddak_v1/model/rider/rider_info_response.dart';

class SingleResultRiderInfoResponse {
  String msg;
  num code;
  bool isSuccess;
  RiderInfoResponse data;

  SingleResultRiderInfoResponse(
      this.msg,
      this.code,
      this.isSuccess,
      this.data
      );

  factory SingleResultRiderInfoResponse.fromJson(Map<String, dynamic> json){
    return SingleResultRiderInfoResponse(
        json['msg'],
        json['code'],
        json['isSuccess'],
        RiderInfoResponse.fromJson(json['data'])
    );
  }
}
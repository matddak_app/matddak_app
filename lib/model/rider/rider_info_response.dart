class RiderInfoResponse{
  String riderName;
  String isWork;
  num payNow;
  String addressWish;

  RiderInfoResponse(
      this.riderName,
      this.isWork,
      this.payNow,
      this.addressWish
      );

  factory RiderInfoResponse.fromJson(Map<String, dynamic> json) {
    return RiderInfoResponse(
        json['riderName'],
        json['isWork'],
        json['payNow'],
        json['addressWish']
    );
  }
}
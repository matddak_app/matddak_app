class Step1Request {

  String name;
  String idNum;
  String? confirmParents;
  String email;
  String password;
  String passwordRe;
  String phoneNumber;

  Step1Request(
      this.name,
      this.idNum,
      this.email,
      this.password,
      this.passwordRe,
      this.phoneNumber,
      [this.confirmParents,]
  );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['name'] = name;
    data['idNum'] = idNum;
    data['confirmParents'] = confirmParents;
    data['email'] = email;
    data['password'] = password;
    data['passwordRe'] = passwordRe;
    data['phoneNumber'] = phoneNumber;
    return data;
  }

}

class Step2Request {

  String? bankOwner;
  String? bankName;
  String? bankIdNum;
  String? bankNumber;

  Step2Request(
    this.bankOwner,
    this.bankName,
    this.bankIdNum,
    this.bankNumber
  );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['bankOwner'] = bankOwner;
    data['bankName'] = bankName;
    data['bankIdNum'] = bankIdNum;
    data['bankNumber'] = bankNumber;
    return data;
  }

}

class Step3Request {

  String? addressWish;
  String? driveType;
  String? driveNumber;

  Step3Request(
    this.addressWish,
    this.driveType,
    this.driveNumber
  );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['addressWish'] = addressWish;
    data['driveType'] = driveType;
    data['driveNumber'] = driveNumber;
    return data;
  }

}


class JoinRequest {

  Step1Request? step1;
  Step2Request? step2;
  Step3Request? step3;

  JoinRequest();

  set setStep1(Step1Request step1){
    this.step1 = step1;
  }

  set setStep2(Step2Request step2){
    this.step2 = step2;
  }

  set setStep3(Step3Request step3){
    this.step3 = step3;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data.addAll(step1!.toJson());
    data.addAll(step2!.toJson());
    data.addAll(step3!.toJson());
    return data;
  }

}
class JoinCreateRequest{

  String name;
  String idNum;
  String? confirmParents;
  String email;
  String password;
  String phoneNumber;
  String phoneType;
  String bankOwner;
  String bankName;
  String bankIdNum;
  String bankNumber;
  String addressWish;
  String driveType;
  String driveNumber;

  JoinCreateRequest(
      this.name,
      this.idNum,
      this.confirmParents,
      this.email,
      this.password,
      this.phoneNumber,
      this.phoneType,
      this.bankOwner,
      this.bankName,
      this.bankNumber,
      this.bankIdNum,
      this.addressWish,
      this.driveType,
      this.driveNumber
      );



  Map<String,dynamic>toJson() =>{ //제이슨을 받아서 스트링으로
   'name':name,
   'idNum':idNum,
   'confirmParents':confirmParents,
   'email':email,
   'password':password,
   'phoneNumber':phoneNumber,
   'phoneType':phoneType,
   'bankOwner':bankOwner,
   'bankName':bankName,
   'bankNumber':bankNumber,
   'bankIdNum':bankIdNum,
   'addressWish':addressWish,
   'driveType':driveType,
   'driveNumber':driveNumber


  };





}
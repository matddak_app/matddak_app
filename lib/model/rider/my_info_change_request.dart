
 class MyInfoChangeRequest {
   String name;
   String addressWish;
   String driveType;
   String driveNumber;
   String bankOwner;
   String bankName;
   String bankNumber;

   MyInfoChangeRequest(
       this.name,
       this.addressWish,
       this.driveType,
       this.driveNumber,
       this.bankOwner,
       this.bankName,
       this.bankNumber
       );



   factory MyInfoChangeRequest.fromJson(Map<String,dynamic> json) {

     return MyInfoChangeRequest(
         json['name'],
         json['addressWish'],
         json['driveType'],
         json['driveNumber'],
         json['bankOwner'],
         json['bankName'],
         json['bankNumber']
     );


   }


   Map<String,dynamic> toJson() {

     final Map<String,dynamic> data = Map<String,dynamic>();

     data['name'] = name;
     data['addressWish'] = addressWish;
     data['driveType'] = driveType;
     data['driveNumber'] = driveNumber;
     data['bankOwner'] = bankOwner;
     data['bankName'] = bankName;
     data['bankNumber'] = bankNumber;

     return data;
   }



 }

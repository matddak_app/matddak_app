
class RiderResponse{
  num id;
  String name;
  String password;
  String addressWish;
  String addressWishValue;
  String driveType;
  String driveTypeValue;
  String driveNumber;
  String phoneType;
  String bankName;
  String bankValue;
  String bankOwner;

  RiderResponse(
      this.id,
      this.name,
      this.password,
      this.addressWish,
      this.addressWishValue,
      this.driveType,
      this.driveTypeValue,
      this.driveNumber,
      this.phoneType,
      this.bankName,
      this.bankValue,
      this.bankOwner,


      );

  factory RiderResponse.fromJson(Map<String,dynamic>json) {
    return RiderResponse(
        json['id'],
        json['name'],
        json['password'],
        json['addressWish'],
        json['addressWishValue'],
        json['driveType'],
        json['driveTypeValue'],
        json['driveNumber'],
        json['phoneType'],
        json['bankName'],
        json['bankValue'],
        json['bankOwner']
    );
  }

}

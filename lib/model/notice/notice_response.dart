
 class NoticeResponse {
  num id;
  String title;
  String? img;
  String text;
  String dateCreate;

  NoticeResponse(
      this.id,
      this.title,
      this.text,
      this.dateCreate
      );

  factory NoticeResponse.fromJson(Map<String,dynamic>json){

   return NoticeResponse(
       json['id'],
       json['title'],
       json['text'],
       json['dateCreate']

   );

  }



 }
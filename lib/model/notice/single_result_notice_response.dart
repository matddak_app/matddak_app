
import 'package:matddak_v1/model/notice/notice_response.dart';

class SingleResultNoticeResponse{

  String msg;
  num code;
  bool isSuccess;
  NoticeResponse data;

  SingleResultNoticeResponse(
      this.msg,
      this.code,
      this.isSuccess,
      this.data
      );

  factory SingleResultNoticeResponse.fromJson(Map<String,dynamic>json){

    return SingleResultNoticeResponse(
        json['msg'],
        json['code'],
        json['isSuccess'],
        NoticeResponse.fromJson(json['data']));
  }
}
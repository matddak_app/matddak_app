class OrderStartChangeRequest {
  num askId;

  OrderStartChangeRequest(this.askId);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['askId'];
    return data;
  }
}
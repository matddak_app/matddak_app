class TabCountResponse {
  num requestCount;
  num pickCount;
  num goCount;
  num doneCount;
  num cancelCount;

  TabCountResponse(
      this.requestCount,
      this.pickCount,
      this.goCount,
      this.doneCount,
      this.cancelCount
      );

  factory TabCountResponse.fromJson(Map<String, dynamic> json) {
    return TabCountResponse(
        json['requestCount'],
        json['pickCount'],
        json['goCount'],
        json['doneCount'],
        json['cancelCount']
    );
  }
}
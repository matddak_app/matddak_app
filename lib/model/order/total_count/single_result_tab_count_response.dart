import 'package:matddak_v1/model/order/total_count/tab_count_response.dart';

class SingleResultTabCountResponse {
  String msg;
  num code;
  bool isSuccess;
  TabCountResponse data;

  SingleResultTabCountResponse(
      this.msg,
      this.code,
      this.isSuccess,
      this.data
      );

  factory SingleResultTabCountResponse.fromJson(Map<String, dynamic> json){
    return SingleResultTabCountResponse(
        json['msg'],
        json['code'],
        json['isSuccess'],
        TabCountResponse.fromJson(json['data'])
    );
  }
}
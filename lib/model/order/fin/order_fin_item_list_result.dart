import 'package:matddak_v1/model/order/fin/order_fin_item.dart';

class OrderFinItemListResult {
  num totalCount;
  num totalPage;
  num currentPage;
  List<OrderFinItem> list;

  OrderFinItemListResult(
      this.totalCount,
      this.totalPage,
      this.currentPage,
      this.list
      );

  factory OrderFinItemListResult.fromJson(Map<String, dynamic> json) {
    return OrderFinItemListResult(
        json['totalCount'],
        json['totalPage'],
        json['currentPage'],
        json['list'] !=null
            ? (json['list'] as List).map((e) => OrderFinItem.fromJson(e)).toList()
            : [],
    );
  }
}
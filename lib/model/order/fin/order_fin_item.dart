/** 완료 리스트 복수 R 작은 틀 **/

class OrderFinItem {
  num id;
  num riderId;
  String riderName;
  num askId;
  String dateDone;
  String storeName;
  String addressStoreDo;
  String addressClientDo;
  String stateDelivery;
  num distance;
  num feeTotal;
  String requestRider;
  String isPay;

  OrderFinItem(
      this.id,
      this.riderId,
      this.riderName,
      this.askId,
      this.dateDone,
      this.storeName,
      this.addressStoreDo,
      this.addressClientDo,
      this.stateDelivery,
      this.distance,
      this.feeTotal,
      this.requestRider,
      this.isPay
      );

  factory OrderFinItem.fromJson(Map<String, dynamic> json) {
    return OrderFinItem(
        json['id'],
        json['riderId'],
        json['riderName'],
        json['askId'],
        json['dateDone'],
        json['storeName'],
        json['addressStoreDo'],
        json['addressClientDo'],
        json['stateDelivery'],
        json['distance'],
        json['feeTotal'],
        json['requestRider'],
        json['isPay']
    );
  }
}
/** 요청 리스트 복수 R 작은 틀 **/

class OrderRequestItem {
  num id;
  num priceRide;
  String isPay;
  num storeId;
  String storeName;
  String addressStoreDo;
  String addressClientDo;
  String? addressClientG;
  String requestRider;
  num distance;

  OrderRequestItem(
      this.id,
      this.priceRide,
      this.isPay,
      this.storeId,
      this.storeName,
      this.addressStoreDo,
      this.addressClientDo,
      this.addressClientG,
      this.requestRider,
      this.distance
      );

  factory OrderRequestItem.fromJson(Map<String, dynamic> json) {
    return OrderRequestItem(
        json['id'],
        json['priceRide'],
        json['isPay'],
        json['storeId'],
        json['storeName'],
        json['addressStoreDo'],
        json['addressClientDo'],
        json['addressClientG'],
        json['requestRider'],
        json['distance']
    );
  }
}
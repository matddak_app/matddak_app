/** 배차 단수 R 작은 틀 **/

class OrderGetResponse {
  num id;
  num riderId;
  String riderName;
  String storeName;
  num distance;
  num feeTotal;
  String requestRider;
  String addressStoreDo;
  String addressClientDo;
  String isPay;
  num priceFood;
  String? dateDone;
  num clientX;
  num clientY;
  num storeX;
  num storeY;

  OrderGetResponse(
      this.id,
      this.riderId,
      this.riderName,
      this.storeName,
      this.distance,
      this.feeTotal,
      this.requestRider,
      this.addressStoreDo,
      this.addressClientDo,
      this.isPay,
      this.priceFood,
      this.clientX,
      this.clientY,
      this.storeX,
      this.storeY
      );

  factory OrderGetResponse.fromJson(Map<String, dynamic> json) {
    return OrderGetResponse(
        json['id'],
        json['riderId'],
        json['riderName'],
        json['storeName'],
        json['distance'],
        json['feeTotal'],
        json['requestRider'],
        json['addressStoreDo'],
        json['addressClientDo'],
        json['isPay'],
        json['priceFood'],
        json['clientX'],
        json['clientY'],
        json['storeX'],
        json['storeY'],
    );
  }
}
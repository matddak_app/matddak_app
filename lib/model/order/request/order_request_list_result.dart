import 'package:matddak_v1/model/order/request/order_request_item.dart';

/** 요청 리스트 복수 R 큰 틀 **/

class OrderRequestListResult {
  num totalCount;
  num totalPage;
  num currentPage;
  List<OrderRequestItem> list;

  OrderRequestListResult(
      this.totalCount,
      this.currentPage,
      this.totalPage,
      this.list
      );

  factory OrderRequestListResult.fromJson(Map<String, dynamic> json) {
    return OrderRequestListResult(
        json['totalCount'],
        json['currentPage'],
        json['totalPage'],
        json['list'] != null
            ? (json['list'] as List).map((e) => OrderRequestItem.fromJson(e)).toList()
            : [],
    );
  }
}
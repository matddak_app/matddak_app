import 'package:matddak_v1/model/order/request/order_get_response.dart';

/** 배차 단수 R용 큰 틀 **/

class OrderGetResult {
  String msg;
  num code;
  OrderGetResponse data;

  OrderGetResult(
      this.msg,
      this.code,
      this.data
      );

  factory OrderGetResult.fromJson(Map<String, dynamic> json) {
    return OrderGetResult(
        json['msg'],
        json['code'],
        OrderGetResponse.fromJson(json['data'])
    );
  }
}
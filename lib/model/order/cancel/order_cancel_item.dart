/** 취소 리스트 복수 R 작은 틀 **/
class OrderCancelItem {
  num id;
  String storeName;
  String timeAsk;
  num priceRide;
  String isPay;
  String requestRider;
  String addressStoreDo;
  String addressClientDo;
  String addressClientG;
  num distance;

  OrderCancelItem(
      this.id,
      this.storeName,
      this.timeAsk,
      this.priceRide,
      this.isPay,
      this.requestRider,
      this.addressStoreDo,
      this.addressClientDo,
      this.addressClientG,
      this.distance
      );

  factory OrderCancelItem.fromJson(Map<String, dynamic> json) {
    return OrderCancelItem(
        json['id'],
        json['storeName'],
        json['timeAsk'],
        json['priceRide'],
        json['isPay'],
        json['requestRider'],
        json['addressStoreDo'],
        json['addressClientDo'],
        json['addressClientG'],
        json['distance']
    );
  }
}
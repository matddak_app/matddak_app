import 'package:matddak_v1/model/order/cancel/order_cancel_item.dart';

class OrderCancelListResult {
  num totalCount;
  num totalPage;
  num currentPage;
  List<OrderCancelItem> list;

  OrderCancelListResult (
      this.totalCount,
      this.currentPage,
      this.totalPage,
      this.list
      );

  factory OrderCancelListResult.fromJson(Map<String, dynamic> json) {
    return OrderCancelListResult(
        json['totalCount'],
        json['currentPage'],
        json['totalPage'],
        json['list'] !=null
            ? (json['list'] as List).map((e) => OrderCancelItem.fromJson(e)).toList()
            : []
    );
  }
}
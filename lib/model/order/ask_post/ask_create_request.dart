class AskCreateRequest {
  num askId;

  AskCreateRequest(this.askId);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['askId'] = askId;
    return data;
  }
}